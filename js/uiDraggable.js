/*
 *  Document   : uiDraggable.js
 *  Author     : pixelcave
 */
var UiDraggable = function() {
    return{init: function() {
            $(".draggable-blocks").sortable({
                onnectWith: ".block", 
                items: ".block", 
                cancel: ".block-fullscreen",
                opacity: .75, 
                handle: ".block-title", 
                placeholder: "draggable-placeholder", 
                tolerance: "pointer", 
                start: function(e, t) {
                    t.placeholder.css("height", t.item.outerHeight())
                }})
        }}
}();