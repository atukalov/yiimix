<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CPagesAction extends CAction {

    public function render($view, $params) {
        $res = Config::get('pages');

        $v = $res['titlefield'][0];
        $this->controller->pageTitle = $params['model']->$v;
        if ($res['showkeyword'][0])
            Yii::app()->clientScript->registerMetaTag($params['model']->keywords, 'keywords');
        Yii::app()->clientScript->registerMetaTag($params['model']->description, 'description');

        if (isset($_GET["id"]) && is_numeric($_GET["id"]) && $_GET["id"] > 0)
            $model = Pages::model()->updateCounters(array('views' => 1), array('condition' => "id = " . intval($_GET["id"]),
            ));
        $this->controller->render($view, $params);
    }

}
