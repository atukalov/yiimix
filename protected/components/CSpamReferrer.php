<?php

class CSpamReferrer {

    public static function install() {

        $sql = <<<EOD
CREATE TABLE `{{blacklist}}` (
`id` mediumint(9) NOT NULL AUTO_INCREMENT,
`item_hash` varchar(42) NOT NULL DEFAULT '',
`item` varchar(255) NOT NULL DEFAULT '',
`source` tinyint(1) DEFAULT NULL,
`works` tinyint(1) DEFAULT NULL,
UNIQUE KEY `id` (`id`),
UNIQUE KEY `item_hash` (`item_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD;

        Yii::app()->db->createCommand($sql)->execute();
    }

    public static function update() {


        /*  $url = 'http://www.didcode.com/srb-blacklist-delta.php?version=2.22&updated_at=';

          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects

          $result = curl_exec($ch); // run the whole process */
        $result = <<<EOD
{"ilovevitaly.com":1,"semalt.semalt.com":1,".darodar.com":2,"priceg.com":0,"buttons-for-website.com":1,"make-money-online.7makemoneyonline.com":1,"bestsub.com":1,"civilwartheater.com":1,"econom.co":1,"entourank.com":1,"webstatsdomain.org":1,"similarpages.com":1,"seokicks.de":1,"cukwiki.com":1,"ilovevitaly.co":1,"casinobonustips.com":1,"co.lumb.co":1,"seoairport.com":1,"ymlp.com":1,"blackhatworth.com":0,"serw.clicksor.com":1,"hulfingtonpost.com":1,"cenoval.ru":0,"bestwebsitesawards.com":0,"co.lumb":0,"kambasoft.com":0,"lumb.co":0,"ranksonic.info":0,"savetubevideo.info":0,"see-your-website-here.com":0,"descargar-musica-gratis.net":0,"lomb.co":0,"medispainstitute":0,"sq01":0,"alienpayday":0,"artobox":0,"axisalternativementalhealth":0,"sharebutton.net":0,"torontoplumbinggroup.com":0,"tasteidea.com":0,"paparazzistudios.com.au":0,"76brighton.co.uk":0,"powitania.pl":0,"ilovevitaly.ru":0,"o-o-6-o-o.com":0,"humanorightswatch.org":0,"forum20.smailik.org":0,"social-buttons.com":0,"s.click.aliexpress.com":0,"simple-share-buttons.com":0,"best-seo-solution.com":0,"buttons-for-your-website.com":0,"get-free-traffic-now.com":0,"best-seo-offer.com":0}
EOD;
        //curl_close($ch);
        $items = json_decode($result, true);

        if (!empty($items)) {

            foreach ($items as $item => $status) {
                $esc_item = CHtml::encode($item);
                $esc_status = CHtml::encode($status);
                $esc_item_hash = md5($item);

                $already = Yii::app()->db->createCommand("SELECT * from {{blacklist}} where item = '$esc_item'")->queryRow();
                if (!$already) {
                    $sql = "INSERT IGNORE INTO {{blacklist}} (item_hash, item, works, source) VALUE ('$esc_item_hash', '$esc_item', '$esc_status', '1')";
                } else {
                    $sql = "UPDATE {{blacklist}} SET item_hash = '$esc_item_hash', item = '$esc_item', works = '$esc_status', source='1' WHERE item = '$esc_item' LIMIT 1";
                }
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }

    public static function filter() {

        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
            $ref = $_SERVER['HTTP_REFERER'];
            $pu = parse_url(self::getTargetHost(), PHP_URL_PATH);
            if ($pu['host'] == self::getTargetHost())
                return true;

            $bl = self::blacklist();

            foreach ($bl as $spammer) {
                if ($spammer['item'] != "" && (strpos($ref, $spammer['item']) !== false)) {
                    header("HTTP/1.0 405 Method Not Allowed");
                    die();
                }
            }
        }
    }

    public static function blacklist() {
        return Yii::app()->db->createCommand("SELECT * FROM {{blacklist}} ORDER BY item")->queryAll();
    }

    /**
     * Determines the target host name from request headers
     * @return string|null
     */
    public static function getTargetHost() {
        $host = null;
        // Check if the value is set
        if (!empty($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        }
        // If HOST header is missing, we're possibly dealing with Apache's mod_proxy
        elseif (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_HOST'], ',') !== FALSE) {
                $hostNames = explode(',', $_SERVER['HTTP_X_FORWARDED_HOST']);
                $host = array_shift($hostNames);
            } else {
                $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
            }
        }
        return $host;
    }

}
