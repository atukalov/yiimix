<?php

/*
 * Custom Fields for Models
 *
 * Create function getUID() for geted Item ID
 *
 * Insert config rule
 * -------------------
 * module  = "admin"
 * section = "fields.target"
 * param   = "{table name}"
 * value   = "{item title}"
 * type    = "string"
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/package/base/custom_fields
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class FieldsBehavior extends CActiveRecordBehavior {

    public function beforeValidate($event) {
        foreach ($this->getFields() as $field) {
            if ($field->multi == 1 && $field->input == 'select') {
                $n = $field->name;  //название поля
                $o = $event->sender;  //берем model       
                $v = $o->{$n};

                if (is_array($v))
                    $v = join(",", $v);

                $o->{$n} = $v;
            }
        }
    }

    public function afterFind($event) {
        $this->owner->rules = array_merge($this->owner->rules(), $this->getFieldsRules());
        $this->owner->createValidators();
    }

    public function getFields() {
        $target = get_class($this->owner); //trim(trim($this->owner->tableName(), "}"), "{");
        $uid = $this->owner->getUID();
        $id = "fields." . $target . "." . intval($uid);

        $value = Yii::app()->cache->get($id);

        $t = array();

        if ($value === false) {
            if ($uid !== false)
                $ids = ActionFields::model()->findAllByAttributes(array('target' => $target, 'action' => $uid));
            else {
                $ids = Fields::model()->findAllByAttributes(array('target' => $target));

                Yii::app()->cache->set($id, $ids, 60 * 60);
                return $ids;
            }
            if (!empty($ids)) {
                foreach ($ids as $v) {
                    $t[] = $v->field;
                }
            }

            $f = Fields::model()->findAllByPk($t);

            $f1 = Fields::model()->findAllByAttributes(array('target' => $target, 'all' => 1));
            $f = array_merge($f, $f1);
            $this->owner->labels = array_merge($this->owner->attributeLabels(), $this->getFieldsLabels($f));

            Yii::app()->cache->set($id, $f, 60 * 60);

            return $f;
        } else
            return $value;
    }

    public function getFieldsRules($fields = null) {
        if ($fields === null)
            $fields = $this->getFields();

        $temp = array();
        if (!empty($fields))
            foreach ($fields as $f) {
                $temp[] = $f->getRule();
            }
        return $temp;
    }

    public function getFieldsLabels($fields = null) {
        if ($fields === null)
            $fields = $this->getFields();

        $temp = array();
        foreach ($fields as $f) {
            $temp[$f->name] = $f->title;
        }

        return $temp;
    }

}
