<?php

class Image {

    static private $cachePath = '/assets/imageCache';

    static private function init() {

        if(!file_exists('.'.self::$cachePath))
            mkdir('.'.self::$cachePath, 0777);
    }

    private static function inCache($file, $size) {

        self::init();

        $f = 'resize_'.$size.'_'.pathinfo($file, PATHINFO_BASENAME);

        if(file_exists('.'.self::$cachePath.'/'.$f))
            return self::$cachePath.'/'.$f;
        else
            return false;
    }

    public static function get($file, $size, $crop = 0) {
        
        if($file==="")
            return false;

        if(self::inCache($file, $size) != false) {
            return self::inCache($file, $size);
        }
        else
            return self::resize($file, $size, $crop);
    }

    private static function resize($src, $size, $crop) {

        list($width, $height) = explode('x', $size);
        $dst = '.'.self::$cachePath.'/resize_'.$size.'_'.pathinfo($src, PATHINFO_BASENAME);

        if(!list($w, $h) = getimagesize('.'.$src))
            return "Unsupported picture type!";

        $type = strtolower(substr(strrchr($src, "."), 1));
        if($type == 'jpeg')
            $type = 'jpg';
        switch ($type) {
            case 'bmp': $img = imagecreatefromwbmp($src);
                break;
            case 'gif': $img = imagecreatefromgif($src);
                break;
            case 'jpg': $img = imagecreatefromjpeg('.'.$src);
                break;
            case 'png': $img = imagecreatefrompng($src);
                break;
            default : return "Unsupported picture type!";
        }

        // resize
        if($crop) {
            if($w < $width or $h < $height)
                return "Picture is too small!";
            $ratio = max($width / $w, $height / $h);
            $h = $height / $ratio;
            $x = ($w - $width / $ratio) / 2;
            $w = $width / $ratio;
        }
        else {
            if($w < $width and $h < $height)
                return "Picture is too small!";
            $ratio = min($width / $w, $height / $h);
            $width = $w * $ratio;
            $height = $h * $ratio;
            $x = 0;
        }

        $new = imagecreatetruecolor($width, $height);

        // preserve transparency
        if($type == "gif" or $type == "png") {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
        }

        imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

        switch ($type) {
            case 'bmp': imagewbmp($new, $dst);
                break;
            case 'gif': imagegif($new, $dst);
                break;
            case 'jpg': imagejpeg($new, $dst);
                break;
            case 'png': imagepng($new, $dst);
                break;
        }
        return $dst;
    }

    public static function isImage($src) {
        $type = strtolower(substr(strrchr($src, "."), 1));
        switch ($type) {
            case 'jpeg': return true;
                break;
            case 'bmp': return true;
                break;
            case 'gif': return true;
                break;
            case 'jpg': return true;
                break;
            case 'png': return true;
                break;
            default : return false;
        }
    }
    
    
    
    public static function getImageByAttributes($param=array()) {
        if(empty($param))
            return false;
        
        
        $res= Filelib::model()->findByAttributes($param);
        if($res){
            return "/uploads/".$res->target."/".$res->item."/".$res->filename;
        }
        
        return false;
        
    }

}
