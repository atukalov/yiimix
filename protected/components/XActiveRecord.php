<?php

class XActiveRecord extends CActiveRecord {

    public $entity;
    public $rules = array();
    public $labels = array();

    public function __construct() {
        if ($this->entity === null)
            $this->entity = strtolower(get_class($this));
        // parent::__construct();
    }

    public function beforeSave() {

        $temp = array();
        $name = array();
        $tmpname = array();
        $empty = array();
        if (!empty($_FILES)) {
            foreach ($_FILES[get_class($this)]['name'] as $k => $v) {
                if (empty($v)) {
                    $empty[] = $k;
                } else {
                    $temp[] = $k;

                    $name[$k] = $_FILES[get_class($this)]['name'][$k];
                    $tmpname[$k] = $_FILES[get_class($this)]['tmp_name'][$k];
                }
            }
        }

        $rr = $this::model()->findByPk($this->id);
        if (!empty($empty))
            foreach ($empty as $k => $v) {
                $this->$v = $rr->$v;
            }
        // var_dump($rr->$empty[0]);
        // die;

        foreach ($this->attributes as $key => $attribute) {
            if (in_array($key, $temp)) {
                if (!file_exists(realpath('./uploads/' . $this->entity))) {
                    mkdir(realpath('./uploads/' . $this->entity), 0777);
                }
                if (!file_exists(realpath('./uploads/' . $this->entity . '/' . $this->id))) {
                    mkdir('./uploads/' . $this->entity . '/' . $this->id, 0777);
                }

                $strSource = $tmpname[$key];
                copy($tmpname[$key], './uploads/' . $this->entity . '/' . $this->id . '/' . $name[$key]);
                $this->$key = '/uploads/' . $this->entity . '/' . $this->id . '/' . $name[$key];
            }
        }
        return parent::beforeSave();
    }

    public function registerModules($action = '') {

        $b = $this->behaviors();

        if (!empty($b))
            foreach ($b as $k => $v) {
                if (method_exists($this->$k, "registerModules"))
                    $this->$k->registerModules($action);
            }
    }

    public function behaviors() {

        Yii::trace('behavior ' . $this->entity, 'system.XActiveRecord');

        $entity = $this->entity;
        $id = "behaviors" . $entity;

        if ($entity === null)
            return array();

        $value = Yii::app()->cache->get($id);

        if ($value === false) {

            $res = Config::get("behaviors.model." . $entity);

            $temp = array();

            if (!empty($res) && isset($res[$entity]))
                if (is_array($res[$entity])) {

                    foreach ($res[$entity] as $v) {
                        $c = explode(".", $v);
                        $c = end($c);
                        if (!empty($c))
                            $temp[$c] = array("class" => $v);
                    }
                } else {
                    $c = explode(".", $res[$entity]);
                    $c = end($c);
                    if (!empty($c))
                        $temp[$c] = array("class" => $res[$entity]);
                }

            Yii::app()->cache->set($id, $temp, 60 * 60);


            return $temp;
        }
        return $value;
    }

    /**
     * Creates validator objects based on the specification in {@link rules}.
     * This method is mainly used internally.
     * @throws CException if current class has an invalid validation rule
     * @return CList validators built based on {@link rules()}.
     */
    public function createValidators() {
        $validators = new CList;
        $this->rules = array_merge($this->rules, $this->rules());

        foreach ($this->rules as $rule) {
            if (isset($rule[0], $rule[1]))  // attributes, validator name
                $validators->add(CValidator::createValidator($rule[1], $this, $rule[0], array_slice($rule, 2)));
            else
                throw new CException(Yii::t('yii', '{class} has an invalid validation rule. The rule must specify attributes to be validated and the validator name.', array('{class}' => get_class($this))));
        }
        return $validators;
    }

    /**
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see generateAttributeLabel
     * @see attributeLabels
     */
    public function getAttributeLabel($attribute) {
        $this->labels = $this->attributeLabels();
        if (isset($this->labels[$attribute]))
            return $this->labels[$attribute];
        else
            return $this->generateAttributeLabel($attribute);
    }

}
