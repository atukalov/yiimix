<?php

class XWebUser extends CWebUser
{

    static $_admin;
    static $_admins;
    
     /**
     * @var boolean
     * @desc use email for activation user account
     */
    public $sendActivationMail=false;
        /**
     * @var boolean
     * @desc allow auth for is not active user
     */
    public $loginNotActiv = false;

    /**
     * @var boolean
     * @desc activate user on registration (only $sendActivationMail = false)
     */
    public $activeAfterRegister = false;

    /**
     * @var boolean
     * @desc login after registration (need loginNotActiv or activeAfterRegister = true)
     */
    public $autoLogin = true;
    
    /**
     * @var boolean
     */
    public $captcha = array('registration' => true);
    
    public $hash='md5';
    public $registrationUrl = array("/user/registration");
    public $recoveryUrl = array("/user/recovery/recovery");
    public $loginUrl = array("/user/login");
    public $logoutUrl = array("/user/logout");
    public $profileUrl = array("/user/profile");
    public $returnUrl = array("/user/profile");
    public $returnLogoutUrl = array("/user/login");
    
    public function __construct()
    {
       $c=Config::get('config.user');           
       
       if(!empty($c))
           foreach($c as $k=>$v)
               $this->$k=$v;       
    }
    
    public function getRole()
    {
        return $this->getState('__role');
    }

    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

//    protected function beforeLogin($id, $states, $fromCookie)
//    {
//        parent::beforeLogin($id, $states, $fromCookie);
//
//        $model = new UserLoginStats();
//        $model->attributes = array(
//            'user_id' => $id,
//            'ip' => ip2long(Yii::app()->request->getUserHostAddress())
//        );
//        $model->save();
//
//        return true;
//    }

    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->updateSession();
    }

    public function updateSession()
    {
        $user = $this->user($this->id);
        $userAttributes = array(
            'email' => $user->email,
            'username' => $user->username,
            'create_at' => $user->create_at,
            'lastvisit_at' => $user->lastvisit_at,
        );
        foreach($userAttributes as $attrName => $attrValue)
        {
            $this->setState($attrName, $attrValue);
        }
    }

    public function model($id = 0)
    {
        return User::model()->findByPk($id);
    }

    public function user($id = 0)
    {
        return User::model()->findByPk($id == 0 ? Yii::app()->user->getId() : $id);
    }

    public function getUserByName($username)
    {
        return User::model()->findByattributes(array('username' => $username));
    }

    public function getAdmins()
    {        
        if(!self::$_admins)
        {
            $admins = User::model()->active()->superuser()->findAll();
            $return_name = array();
            foreach($admins as $admin)
                array_push($return_name, $admin->username);
            self::$_admins = ($return_name) ? $return_name : array('');
        }
        return self::$_admins;
        
    }

    public function getAvatar()
    {        
        $user = User::model()->findByPk($this->id);
        $au = Yii::app()->getModule("admin")->getAssetsUrl();
        
        return ($user->profiles->avatar==''?$au.'/images/nouser.jpg':'/'.$user->profiles->avatar);
    }

    public function isAdmin()
    {        
        $u=User::model()->findByPk(Yii::app()->user->getId());
        return $u->superuser;
    }

     /**
     * @return hash string.
     */
    public static function encrypting($string = "")
    {
        $hash = $this->hash;
        if($hash == "md5")
            return md5($string);
        if($hash == "sha1")
            return sha1($string);
        else
            return hash($hash, $string);
    }
    
    /**
     * @param $place
     * @return boolean 
     */
    public static function doCaptcha($place = '')
    {
        if(!extension_loaded('gd'))
            return false;
        if(in_array($place, Yii::app()->user->captcha))
            return Yii::app()->user->captcha[$place];
        return false;
    }
}
