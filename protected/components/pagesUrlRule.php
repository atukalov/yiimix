<?php

/*
 * Yiimix parse urls for Pages base system
 * 
 * @category   URL RULES => Pages and SEF(Urls)
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class PagesUrlRule extends CBaseUrlRule {

    public $connectionID = 'db';

    public function createUrl($manager, $route, $params, $ampersand) {
        //  return parent::createUrl($manager, $route, $params, $ampersand);

        $rp = explode('/', $route);
        if ($rp[0] == 'pages') {
            if (isset($params['id'])) {
                $r = Pages::model()->findByPk($params['id']);
                $temp = '';
                foreach ($params as $k => $v) {
                    if ($k != 'id')
                        $temp.=$ampersand . $k . '=' . $v;
                }
                return $r->url . '?' . ltrim($temp, '&');
            }
        }
    }

    /**
     * @params Default for base class
     * @return Controller/Action or False
     */
    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {


        $path = Language::check($pathInfo);
        if ($pathInfo == '')
            return "pages/index";
        else {

            $sef = Urls::model()->findByAttributes(array("url" => array($pathInfo, '/' . $pathInfo), "active" => 1));

            if ($sef && $sef->target == "pages") {
                $res = Pages::model()->findByPk($sef->item);
            } else {
                $res = Pages::model()->findByAttributes(array("url" => $pathInfo));

                if ($res) {
                    $_GET['id'] = $res->id;
                    return 'pages/' . $res->action;
                }
            }

            if ($res) {
                Yii::app()->request->redirect('/' . $res->url);
            }

            return false;
        }
    }

}
