<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CInstaller
{

    public $path;

    public function cmd($cmd, $filename = "")
    {
        $this->filename = $filename;

        switch($cmd)
        {
            case "install":               
                //echo $this->getSources();
                $this->init();
                unset(Yii::app()->session['progress']);
                break;

            case "update":
                $this->update();
                break;

            case "remove":
                $this->remove();
                break;
            default:
                echo $this->status();
                break;
        }
    }

    public function init()
    {
        
    }

    public function update()
    {
        
    }

    public function remove()
    {
        
    }

    public function status()
    {
        return "Status";
    }

    private function getSources()
    {
        $rt = Yii::getPathOfAlias("application.runtime");

        if(!file_exists($rt."/temp"))
        {
            mkdir($rt."/temp", 0777);
        }

        $pp = pathinfo($this->filename);
        $ext = $pp["extension"];
        $bn = $pp["basename"];
        $name = $pp["filename"];

        if(!file_exists($rt."/temp/".$name))
        {
            mkdir($rt."/temp/".$name, 0777);
        }

        if(copy($this->filename, $rt."/temp/".$name."/".$bn))
        {
            $this->archive = $rt."/temp/".$name."/".$bn;
            $this->path = $rt."/temp/".$name;

            if($ext == 'gz')
            {
                $execute = "gunzip -$rt/temp/$name $bn";
                `$execute`;
            }
            if($ext == 'zip')
            {
                $execute = "unzip -u $bn -d $rt/temp/$name";
                `$execute`;
            }
        }
        else
            return "Error Intall: Copy temp file";
    }

    function progress($percent)
    {
       Yii::app()->session['progress'] = $percent;  
        Yii::app()->session->close();
       //session_write_close();
    }

}
