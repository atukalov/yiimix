<?php
/*
 * Контроллер параметров системы.
 * Параметры храняться в таблице {{config}}
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base/config
 * @see        Model.Params
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class Config {

    const INTERFACE_ALL = 0;
    const INTERFACE_MENU = 1;
    const INTERFACE_MAIN = 2;
    const INTERFACE_LEFT = 3;
    const INTERFACE_SOURCELIST = 4;

    /**
     * Фукция получения значения параметра.
     *
     * @param string $module - Имя модуля.
     * @param string $section - Имя секции.
     * @param string $param - Имя параметра. Если не задан, будут выбранны
     * все параметры в секции, сделано чтобы иметь возможность создавать
     * массивы параметров.
     * @return mixed, false -  если ошибка.
     */
    public static function getInterface($current, $get = CONFIG::INTERFACE_ALL) {

        $path = Yii::app()->controller->module->id . ".interface." . Yii::app()->controller->id . "." . Yii::app()->controller->action->id;

        $value = Yii::app()->cache->get(Yii::app()->user->id . $path);
        if ($value === false) {
            $config = array();
            //Берем все возможные виджеты
            $all = Params::model()->findAllByAttributes(array("path" => $path, "active" => 1));

            //Проверяем есть ли настройки пользователя
            $conf = Params::model()->findByAttributes(array("path" => $path . ".sort", "user" => Yii::app()->user->id));
            if ($conf) {
                $conf = self::decode($conf->value, $conf->type);

                //Ищем скрытые виджеты для отображения в меню
                $temp = array();
                foreach ($all as $v)
                    $temp[] = $v->id;

                $all_visible = array_merge($conf->main, $conf->left);
                $menu = array();
                foreach ($temp as $v) {
                    if (!in_array($v, $all_visible))
                        $menu[] = $v;
                }
                // Config
                $config = array("main" => array(), "left" => array(), "menu" => array());
                foreach ($all as $v) {

                    if (!empty($menu) && in_array(intval($v->id), $menu))
                        $config["menu"][] = $v;
                    elseif (($i = array_search(intval($v->id), $conf->main)) !== FALSE) {
                        $config["main"][$i] = $v;
                    } elseif (($i = array_search(intval($v->id), $conf->left)) !== FALSE)
                        $config["left"][$i] = $v;
                }
                $i = array_search(0, $conf->main);
                $config["main"][$i] = 0;
                $config["source"] = $all;


                Yii::app()->cache->set(Yii::app()->user->id . $path, $config);
                $value = $config;
            }
            else {
                $config = array("main" => array(0), "left" => array(), "menu" => array());

                foreach ($all as $v) {
                    $config["left"][] = $v;
                }

                Yii::app()->cache->set(Yii::app()->user->id . $path, $config);
                $value = $config;
            }
        } else {
            $config = array("main" => array(), "left" => array(), "menu" => array());
        }

        switch ($get) {
            case self::INTERFACE_LEFT:
                return $value["left"];
                break;
            case self::INTERFACE_MAIN:
                return $value["main"];
                break;
            case self::INTERFACE_MENU:
                return $value["menu"];
                break;
            case self::INTERFACE_SOURCELIST:
                return $value["source"];
                break;
            default :
                return $value;
        }
    }

    /**
     * Фукция получения значения параметра.
     *
     * @param string $module - Имя модуля.
     * @param string $section - Имя секции.
     * @param string $param - Имя параметра. Если не задан, будут выбранны
     * все параметры в секции, сделано чтобы иметь возможность создавать
     * массивы параметров.
     * @return mixed, false -  если ошибка.
     */
    public static function lockInterface($user, $path, $value) {
        $conf = Params::model()->findByAttributes(array("path" => $path . ".sort", "user" => $user));
        if (!$conf) {
            $model = new Params();
            $model->user = $user;
            $model->path = $path . ".sort";
            $model->value = json_encode($value);
            $model->type = "json";
            $model->active = 1;
            $model->save();
        } else {
            $conf->value = json_encode($value);
            $conf->save("value");
        }


        $value = Yii::app()->cache->delete(Yii::app()->user->id . $path);
    }

    /**
     * Фукция получения значения параметра.
     *
     * @param string $path - Alias поиска.
     * @return mixed, false -  если ошибка.
     */
    public static function get($path, $useCache = false) {

        $value = $useCache ? Yii::app()->cache->get($path) : false;
        if ($value === false) {
            $sql = "SELECT `path`, `value`,`type` FROM `{{config}}` WHERE `path` LIKE \"" . $path . "%\" AND `active`=1 ORDER BY `id` ASC";
            $res = Yii::app()->db->createCommand($sql)->queryAll();

            $temp = array();

            if (!empty($res))
                if (count($res) == 1) {
                    $a = explode(".", $res[0]['path']);
                    $a = end($a);

                    $temp[$a] = self::decode($res[0]["value"], $res[0]["type"]);
                } else
                    foreach ($res as $v) {
                        $a = explode(".", $v['path']);
                        $a = end($a);

                        $temp[$a][] = self::decode($v["value"], $v["type"]);
                    }

            if ($useCache)
                Yii::app()->cache->set($path, $temp, 60 * 60);
            return $temp;
        }

        return $value;
    }

   

    /**
     * Фукция получения значения параметра с парметрами.
     *
     * @param string $module - Имя модуля.
     * @param string $section - Имя секции.
     * @param string $param - Имя параметра. Если не задан, будут выбранны
     * все параметры в секции, сделано чтобы иметь возможность создавать
     * массивы параметров.
     * @return mixed, false -  если ошибка.
     */
    public static function getByValue($module, $section, $param = false) {
        if (!$param) {
            return false;
        } else {
            $res = Params::model()->findAllByAttributes(array("path" => $module, "value" => $param));

            if (!$res)
                return false;
            else {
                $t = array();
                foreach ($res as $p)
                    $t[$p->param] = self::decode($p->value, $p->type);

                return $t;
            }
        }
    }

    /**
     * Фукция задания параметра. Создает или обновляет параметр
     *
     * @param string $module - Имя модуля.
     * @param string $section - Имя секции.
     * @param string $param - Имя параметра.
     * @param mixed $value - Значение параметра.
     * @param string $type - Обработчик значения параметра.
     * 		Возможные значения: 'array','string','int','real','bool','json'.
     * 		Елси не задан, то обрабатывается как строка
     * @return BOOL
     */
    public static function set($module, $section, $param, $value, $type = "string") {

        $p = Params::model()->findAllByAttributes(array("module" => $module, "section" => $section, "param" => $param));

        if (!$p)
            $p = new Params();

        $p->module = $module;
        $p->section = $section;
        $p->param = $param;
        $p->type = $type;
        $p->type = self::encode($value, $type);

        if ($p->save())
            return true;

        return false;
    }

    /**
     * Функция декодирует значение параметра
     *
     * @param mixed $field - параметр.
     * @return mixed
     */
    public static function decode($value, $type) {

        switch ($type) {
            case "array":
                return explode(",", $value);
                break;
            case "int":
                return intval($value);
                break;
            case "real":
                return floatval($value);
                break;
            case "bool":
                return (bool) $value;
                break;
            case "json":
                return json_decode($value);
                break;
            default:
                return $value;
        }
    }

    /**
     * Функция кодирует значение параметра
     *
     * @param mixed $value - Значение параметра.
     * @param string $type - Обработчик значения параметра.
     * 		Возможные значения: 'array','string','int','real','bool','json'.
     * 		Елси не задан, то обрабатывается как строка
     * @return mixed
     */
    public static function encode($value, $type) {

        switch ($type) {
            case "array":
                return join(",", $value);
                break;
            case "json":
                return json_encode($value);
                break;
            default:
                return $value . "";
        }
    }

}
