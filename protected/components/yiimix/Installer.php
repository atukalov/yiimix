<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class Installer
{

    public static function findPackage($pname)
    {


        $dir = Yii::getPathOfAlias('application');
        $dirs = array();

        $yourStartingPath = $dir;
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($yourStartingPath), RecursiveIteratorIterator::SELF_FIRST);

        foreach($iterator as $name => $file)
        {
            if(!$file->isDir() && $file->getFilename() != '.' && $file->getFilename() != '..' && substr($file->getFilename(), 0, 1) != '.')
            {

                $path = $file->getRealpath();
                $dirs[] = $path;
                // echo "<br />".$path;
            }
        }

        $used = array();
        foreach($dirs as $f)
        {
            $c = file_get_contents($f);
            if(preg_match('/@package (\s)+'.$pname.'/isu', $c))
            {
                $used[] = str_replace($dir, '', $f);
            }
            unset($c);
        }

        unset($dirs);

        $filesumm = array();
        //создаем структуру пакета
        //директория куда складывать
        $base = '/Volumes/2TB/01_WEBSITES/remont.ru';
        $dest = $base.'/installer/'.$pname;

        if(!file_exists($base.'/installer/'.$pname))
            mkdir($base.'/installer/'.$pname, 0777, true);

        foreach($used as $v)
        {

            $du = dirname($v);

            $filename = $dest.'/source/'.$du;
            if(!file_exists($filename))
                mkdir($filename, 0777, true);

            $c = file_get_contents($dir.$v);

            file_put_contents($dest.'/source/'.$v, $c);
            chmod($dest.'/source/'.$v, 0777);
            $filesumm[$v] = md5_file($dest.'/source/'.$v);
        }
        $ss["package"] = $pname;
        $ss["files"] = $filesumm;
        file_put_contents($dest.'/package.text', json_encode($ss));
    }

}
