<?php


class Language {

    private static $names = array();
    public static $langs = array();
    public static $default = false;
    public static $current = false;
    public static $admin = false;

    public static function init() {

        if (!self::$langs) {
            $res = Config::get("site.lang");
            if ($res) {
                if (isset($res['language'])) {

                    foreach ($res['language'] as $v) {
                        $l = explode(':', $v);
                        self::$langs[] = $l[0];
                        self::$names[$l[0]] = $l[1];
                    }
                }
                if (isset($res['default']))
                    self::$default = $res['default'][0];
                if (isset($res['admin']))
                    self::$admin = $res['admin'][0];
                self::$current = isset(Yii::app()->request->cookies["lang"]) ? Yii::app()->request->cookies["lang"] : self::$default;

                Yii::app()->language = self::$current;
            }
        }
    }

    public static function check($url) {
        self::init();

        $res = preg_match("/^(" . join('|', self::$langs) . ")/ui", $url, $m);
        if ($res == 1 && !empty($url)) {
            if ($m[1] == self::$default)
                throw new CHttpException(404, "Page not found");
            else {
                self::current($m[1]);
                return self::clearLang($url);
            }
        } else {
            self::current(self::$default);             
            return $url;
        }
    }

    public static function current($lang) {
        self::init();
        self::$current = $lang;
        Yii::app()->language = $lang;
    }
     public static function admin() {
        self::init();
        return self::$admin;
      
    }

    public static function lang() {
        self::init();
        return (self::$current == self::$default ? '' : self::$current . '/');
    }

    public static function alternative() {
        self::init();
        return '';
    }

    public static function is($lang) {
        self::init();
        return self::$current == $lang ? true : false;
    }

    public static function url($lang) {
        self::init();
        $u = self::clearLang(trim(Yii::app()->request->requestUri, '/'));
        if ($lang == self::$default) {
            return $u;
        } else
            return $lang . '/' . $u;
    }

    public static function clearLang($url) {
        self::init();
        return preg_replace("/^(" . join('\/|', self::$langs) . "\/)/ui", "", $url);
    }

}
