<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class YiiMix {

    private $error = false;

    /**
     * Функция регистрации экшена для модуля Admin. Вызывается один раз.
     * 
     * @param string $controller - Имя контроллера. Пример: pages
     * @param string $action - Имя экшена. Пример: menu_insert (url: /admin/pages/menu/insert)
     * @param string $path - Путь к файлу класса в формате alias: 'admin.controllers.pages.menu.insertAction'
     * @return BOOL, true - если успешно создан
     */
    public static function registerAdminAction($controller, $action, $path) {
        if (!Params::model()->findByAttributes(array('module' => 'admin', 'section' => $controller . '.action', 'param' => $action))) {
            return Config::set('admin', $controller . '.action', $action, $path);
        } else {
            $this->error = 'Экшен ' . $action . ' уже зарегистрирован';
            return false;
        }
    }

    public static function getErrors() {
        return $this->error;
    }

    /**
     * Функция сохранения Cookie.     * 
     * @param string $par - Имя переменной
     * @param string $val - Mixed 
     */
    public static function setCookie($par, $val) {

        $cookie = new CHttpCookie($par, $val);
        $cookie->expire = time() + 60 * 60 * 24 * 365;
        $cookie->path = "/";
        Yii::app()->request->cookies[$par] = $cookie;
    }

    /**
     * Старт системы. 
     */
    public static function start() {
        $el = Config::get("system.autoload");
        if ($el)
            foreach ($el as $v)
                call_user_func($v . "::run");
        //Yii::import('ext.refspam.*');
        //$r=new refSpam();
       // CSpamReferrer::filter();
        //CSpamReferrer::update();
        //$r->actionIndex();
    }

    /**
     * Send mail method
     */
    public static function sendMail($email, $subject, $message) {
        $adminEmail = Yii::app()->params['adminEmail'];
        $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        return mail($email, '=?UTF-8?B?' . base64_encode($subject) . '?=', $message, $headers);
    }

}
