<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class PageFieldsBehavior extends CBehavior
{

    public function events()
    {
        return array(
            'onBeforeSave' => 'beforeSave',
            'onAfterFind' => 'afterFind',
        );
    }

    public function beforeSave()
    {

        foreach($this->getOwner()->getFields() as $field)
        {

            //Multiple CheckboxList
            if($field->multi == 1 && $field->input == 'check')
            {
                $n = $field->name;  //название поля
                $o = $this->getOwner();  //берем model
                $o->$n = ($o->$n != '' ? join(',', $o->$n) : '');
            }
        }
    }

    public function afterFind()
    {

        /*   foreach($this->getOwner()->getFields() as $field) {

          //Multiple CheckboxList
          // if($field->multi == 1 && $field->input == 'check') {
          $n = $field->name;  //название поля
          $o = $this->getOwner();  //берем model
          $o->$n = explode(',', $o->$n);
          //}
          } */
        //var_dump($this->getOwner());die;
    }

}
