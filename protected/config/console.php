<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',
    // preloading 'log' component
    'preload' => array('log'),
    // application components
    'components' => array(
         'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
            'defaultRoles'=>array('guest'),
        ),
        
        'user' => array(
            'class' => 'application.components.XWebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('/user/login'),
        ),
        
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=diapac_new;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'rjynhjkm',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'modules' => array(

    ),
);