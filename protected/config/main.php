<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Мегалогистика',
    'id' => 'http://loc.sasha.ru',
    'language' => 'ru',
    'sourceLanguage' => 'en',
    // preloading 'log' component
    'preload' => array('log', 'debug'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.widgets.*',
        'application.components.*',
        'application.components.yiimix.*',
        'application.components.yiimix.compass.*',
        'ext.*',
    ),
    'onBeginRequest' => function($event) {
YiiMix::start();
},
    'modules' => array(
// uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '111',
        // If removed, Gii defaults to localhost only. Edit carefully to taste.
        ///'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'admin',
    //  'compass',
    //  'projects',
    //  'Shop',
    ),
    // application components
    'components' => array(
        'cache' => array(
            'class' => 'system.caching.CFileCache'
        ),
        'user' => array(
            'class' => 'application.components.XWebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('/users/login'),
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('guest'),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'soap' => 'soap/quote',
                //'gii' => 'gii/default/index',
                'analyse' => 'analyse/index',
                'compass/image/info.png' => 'compass/image',
                'site/captcha' => 'site/captcha',
                array('class' => 'application.modules.admin.components.AdminUrlRule'),
                array('class' => 'application.components.pagesUrlRule'),
                'shop' => 'Shop/default/index',
                'shop/<controller:\w+>' => 'Shop/<controller>/index',
                'shop/<controller:\w+>/<action:\w+>' => 'Shop/<controller>/<action>',
                'shop/<controller:\w+>/<action:\w+>/<id:\w+>' => 'Shop/<controller>/<action>',
            ),
        ),
// uncomment the following to use a MySQL database

        /*  'dba' => array(
          'class'=>'CDbConnection',
          'connectionString' => 'mysql:host=localhost;dbname=sy;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => 'rjynhjkm',
          'charset' => 'utf8',
          'tablePrefix' => '',
          'enableProfiling' => true,
          'enableParamLogging' => true,
          'schemaCachingDuration' => 3600,
          ), */
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=yiimix_last;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'rjynhjkm',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
            'enableProfiling' => true,
            'enableParamLogging' => true,
            'schemaCachingDuration' => 3600,
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'debug' => array(
            'class' => 'ext.yii2-debug.Yii2Debug', // composer installation
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, trace',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'adminEmail' => 'anatoly.tukalov@gmail.com',
        'uploadFolder' => './images/',
        'editAria_fontSize' => 10,
        'editAria_fontFamily' => 'Monospace',
    ),
);
