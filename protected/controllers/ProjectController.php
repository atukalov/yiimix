<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix Project
 * @package    yiimix.project
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/project
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */


class ProjectController extends Controller
{

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
    public function actions()
    {
        $value = Yii::app()->cache->get('ProjectControllerActions');
        if($value === false)
        {
            $internal = array('index');
            $temp = array();
            $r = Config::get("project.action", false);
            if(!empty($r))
                foreach($r as $k => $v)
                {
                    if($k == "action")
                        $temp["index"] = 'application.'.$v;
                    else
                        $temp[$k] = 'application.'.$v;
                }

            $value = self::mergeActions($internal, $temp);

            Yii::app()->cache->set('ProjectControllerActions', $temp, 60 * 60 * 24 * 30);            
        }
        return $value;
    }

    private static function mergeActions($ar, $ar1)
    {
        $temp = array();

        foreach($ar as $k => $v)
        {
            if(is_numeric($k))
            {
                if(array_key_exists($v, $ar1))                
                    $temp[$v] = $ar1[$v];                
                else                
                    $temp[] = $v;                
            }
            else
            {
                if(array_key_exists($k, $ar1))                
                    $temp[$k] = $ar1[$k];                
                else                
                    $temp[$k] = $v;               
            }
        }
        foreach($ar1 as $k => $v)
        {
            if(is_numeric($k)&& !array_key_exists($v, $temp))                                  
                    $temp[] = $v;            
            else            
                $temp[$k] = $v;           
        }
        return $temp;
    }

    public function actionIndex()
    {
        $this->render('index');
    }
}