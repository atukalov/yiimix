<?php

class ServiceController extends CController
{
    public function actions()
    {
        return array(
            'quote'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }
 
    /**
     * @param string индекс предприятия
     * @return float цена
     * @soap
     */
    public function getNew()
    {
        return Orders::model()->findAll(array('order'=>'date DESC','limit'=>'10'));
    }
}