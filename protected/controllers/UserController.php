<?php

class UserController extends Controller {

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
    public function actions() {
        $value = Yii::app()->cache->get('UserControllerActions');
        if ($value === false) {
            $internal = array('index', 'login',);
            $temp = array();
            $r = Config::get("user.action", false);
            if (!empty($r))
                foreach ($r as $k => $v) {
                    if ($k == "action")
                        $temp["index"] = 'application.' . $v;
                    else
                        $temp[$k] = 'application.' . $v;
                }

            $value = self::mergeActions($internal, $temp);

            Yii::app()->cache->set('UserControllerActions', $temp, 60 * 60 * 24 * 30);
        }
        return $value;
    }

    private static function mergeActions($ar, $ar1) {
        $temp = array();

        foreach ($ar as $k => $v) {
            if (is_numeric($k)) {
                if (array_key_exists($v, $ar1))
                    $temp[$v] = $ar1[$v];
                else
                    $temp[] = $v;
            }
            else {
                if (array_key_exists($k, $ar1))
                    $temp[$k] = $ar1[$k];
                else
                    $temp[$k] = $v;
            }
        }
        foreach ($ar1 as $k => $v) {
            if (is_numeric($k) && !array_key_exists($v, $temp))
                $temp[] = $v;
            else
                $temp[$k] = $v;
        }
        return $temp;
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionLogout() {

        $burl = Yii::app()->request->getUrlReferrer();
        $rb = '';
        if ($burl != '') {
            $rb = parse_url($burl);
            if ($rb['host'] == $_SERVER['SERVER_NAME'])
                $rb = true;
            else
                $rb = false;
        }

        Yii::app()->user->logout();
        if ($rb)
            $this->redirect($burl);
        else
            $this->redirect('/');
    }

    private function lastViset() {
        /* $lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
          $lastVisit->lastvisit = time();
          $lastVisit->save(); */
        $sql = "UPDATE {{user}} SET lastvisit=NOW() WHERE id=" . Yii::app()->user->id;
        Yii::app()->db->createCommand($sql)->execute();
    }

    public function actionLogin() {
        if (Yii::app()->user->isGuest) {
            $model = new UserLogin;
            if (isset($_POST['UserLogin'])) {
                $model->attributes = $_POST['UserLogin'];
                if ($model->validate()) {
                    $this->lastViset();
                    if (isset($_POST['rurl']) && $_POST['rurl'] != '')
                        $this->redirect($_POST['rurl']);
                    else
                        $this->redirect('/user');
                }
            }
            $this->render('login', array('model' => $model));
        } else
            $this->redirect('/user');
    }

    /**
     * Registration user
     */
    public function actionRegistration() {
        $model = new RegistrationForm;
        // $profile=new Profile;
        // $profile->regMode = true;
        // ajax validator
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form') {
            //echo UActiveForm::validate(array($model,$profile));
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->redirect('/user');
        } else {
            if (isset($_POST['RegistrationForm'])) {
                $model->attributes = $_POST['RegistrationForm'];
                //$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
                if ($model->validate()) {
                    $soucePassword = $model->password;
                    $model->activkey = UserModule::encrypting(microtime() . $model->password);
                    $model->password = UserModule::encrypting($model->password);
                    $model->verifyPassword = UserModule::encrypting($model->verifyPassword);
                    $model->superuser = 0;
                    $model->status = ((Yii::app()->controller->module->activeAfterRegister) ? User::STATUS_ACTIVE : User::STATUS_NOACTIVE);

                    if ($model->save()) {
                        //$profile->user_id=$model->id;
                        //$profile->save();
                        if (Yii::app()->user->sendActivationMail) {
                            $activation_url = $this->createAbsoluteUrl('/user/activation', array("activkey" => $model->activkey, "email" => $model->email));
                            Yiimix::sendMail(
                                    $model->email, Yii::t('user', "You registered from {site_name}", array('{site_name}' => Yii::app()->name)), Yii::t('user', "Please activate you account go to {activation_url}", array('{activation_url}' => $activation_url)
                            ));
                        }

                        if ((Yii::app()->user->loginNotActiv || (Yii::app()->user->activeAfterRegister && Yii::app()->user->sendActivationMail == false)) && Yii::app()->user->autoLogin) {
                            $identity = new UserIdentity($model->username, $soucePassword);
                            $identity->authenticate();
                            Yii::app()->user->login($identity, 0);
                            $this->redirect(Yii::app()->user->returnUrl);
                        } else {
                            if (!Yii::app()->user->activeAfterRegister && !Yii::app()->user->sendActivationMail) {
                                Yii::app()->user->setFlash('registration', Yii::t('user', "Thank you for your registration. Contact Admin to activate your account."));
                            } elseif (Yii::app()->user->activeAfterRegister && Yii::app()->user->sendActivationMail == false) {
                                Yii::app()->user->setFlash('registration', Yii::t('user', "Thank you for your registration. Please {{login}}.", array('{{login}}' => CHtml::link(Yii::t('user', 'Login'), Yii::app()->user->loginUrl))));
                            } elseif (Yii::app()->user->loginNotActiv) {
                                Yii::app()->user->setFlash('registration', Yii::t('user', "Thank you for your registration. Please check your email or login."));
                            } else {
                                Yii::app()->user->setFlash('registration', Yii::t('user', "Thank you for your registration. Please check your email."));
                            }
                            $this->refresh();
                        }
                    }
                } //else $profile->validate();
            }
            $this->render('/user/registration', array('model' => $model));
        }
    }

}
