<?php
class indexAction extends CPagesAction {

    public function run() {
        $model = Pages::model()->findByAttributes(array('action' => 'index'));      
        $this->render('index/index', array(
            'model' => $model,
        ));
    }

}

?>