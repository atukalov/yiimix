<?php
/*
 * Контроллер шаблона {name}
 * YiiMix 0.1
 */

class {name}Action extends CPagesAction {

    public function run($id) {

        if (Yii::app()->request->isPostRequest) {

            /* Если шаблон содержит форму и её нужно обработать,
             * то в этом блоке можно это сделать,
             * если не содержит форм, то блок можно удалить.
             */            
            
        } else {

            $model = Pages::model()->findByPk($id);            
            
          

            // Отображаем страницу
            $this->render('{name}/index', array(
                'model' => $model,
            ));
        }
    }

}

?>
