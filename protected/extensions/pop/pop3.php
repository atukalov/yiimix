<?php

class pop3 {

  protected $fp = null;

  // коннект и логин на сервере
  public function __construct($host, $port, $user, $pwd) {
    if ( !$this->connect($host, $port) )
      throw new Exception('Connect to pop3 server failed');
    if ( !$this->login($user, $pwd) )
      throw new Exception('Login on pop3 server failed');
  }

  // возвращает количество писем в ящике и их размер
  public function count() {
    fwrite($this->fp, "STAT\r\n");
    $stat = fread($this->fp, 1024);
    if ( $stat[0] == '+' ) {
      list(, $num, $size) = explode(' ', $stat);
      return array($num, $size);
    } else {
      return false;
    }
  }

  // удаляем письма
  public function dele($id) {
    fwrite($this->fp, "DELE ".$id."\r\n");
    fread($this->fp, 1024);
  }

  // список писем
  /*public function mlist($mess_id = null) {
    if ( $mess_id > 0 ) {
      $mess = ' '.$mess_id;
      $concrette = true;
    } else {
      $mess = '';
      $concrette = false;
    }
    fwrite($this->fp, "LIST".$mess."\r\n");
    $data = fread($this->fp, 1024);
    if ( $data[0] == '+' ) {
      if ( $concrette ) {
        list(,, $size) = explode(' ', $data);
        return $size;
      } else {
        preg_match_all('!\d+!', $data, $m);
        return array($m[0][0], $m[0][1]);
      }
    } else {
      return false;
    }
  }*/

  // получение письма
  public function retr($id) {
    fwrite($this->fp, "RETR ".$id."\r\n");

    $data = '';
    while ( true )  {
      $tmp = fgets($this->fp, 1024);
      if ($tmp == ".\r\n") break;
      $data .= $tmp;
    }
    return explode("\r\n\r\n", $data, 2);
  }

  // соединение с сервером
  protected function connect($host, $port) {
    if ( function_exists('fsockopen') ) {
      $this->fp = fsockopen($host, $port, $errno, $errstr, 5);
      if ( !$this->fp || $errno > 0 ) {
        return false;
      }
      $banner = fread($this->fp, 1024); // баннер
    } else {
      return false;
    }
    return true;
  }

  // авторизация на сервере
  protected function login($user, $pwd) {
    fwrite($this->fp, "USER ".$user."\r\n");
    $answ = fread($this->fp, 1024);
    if ( $answ[0] == '+' ) {
      fwrite($this->fp, "PASS ".$pwd."\r\n");
      $answ = fread($this->fp, 1024);
      return $answ[0] == '+';
    } else {
      return false;
    }
    return $answ[0] == '+';
  }

  // прощаемся
  public function __destruct() {
    fwrite($this->fp, "QUIT\r\n");
    $quit = fread($this->fp, 1024);
    fclose($this->fp);
  }

}