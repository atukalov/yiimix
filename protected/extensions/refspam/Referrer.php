<?php

/*
 * Referrer Spam Detected Tools Model
 *
 * @category   yiimix.extentions
 * @package    yiimix.refspam
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2015 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/refspam
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class Referrer extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{referrer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clicks, first, last, IP', 'required'),
            array('clicks, IP, blocked', 'numerical', 'integerOnly' => true),
            array('url, link_anchor', 'length', 'max' => 255),
            array('domain', 'length', 'max' => 150),
            array('status', 'length', 'max' => 7),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, url, domain, clicks, first, last, link_anchor, IP, status, blocked', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'url' => 'Url',
            'domain' => 'Domain',
            'clicks' => 'Clicks',
            'first' => 'First',
            'last' => 'Last',
            'link_anchor' => 'Link Anchor',
            'IP' => 'Ip',
            'status' => 'Status',
            'blocked' => 'Blocked',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('domain', $this->domain, true);
        $criteria->compare('clicks', $this->clicks);
        $criteria->compare('first', $this->first, true);
        $criteria->compare('last', $this->last, true);
        $criteria->compare('link_anchor', $this->link_anchor, true);
        $criteria->compare('IP', $this->IP);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('blocked', $this->blocked);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Referrer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
