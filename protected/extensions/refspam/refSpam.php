<?php

/*
 * Referrer Spam Detected Tools
 *
 * @category   yiimix.extentions
 * @package    yiimix.refspam
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2015 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/refspam
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class refSpam
{

    const REFSPAM = 0;
    const LINK = 1;
    const NOLINK = 2;

    private static $host;

    public function __construct()
    {
        self::$host ='loc.yiimix.ru'; //Config::get("main.domain");
        
    }

    public function actionIndex()
    {
        //error_reporting(E_ALL);
        $ip = 0;
        if(isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        elseif(isset($_SERVER['REMOTE_ADDR']))
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        /* if($_SERVER['HTTP_REFERER']=="")
          return "NO REFFERER";

          if (!$url = Yii::app()->getBaseUrl())
          $url = self::$host;

          $parse = parse_url($_SERVER['HTTP_REFERER']);
          if ($url == $parse['host'])
          return "INNER JUMP";
          else
          $link = $_SERVER['HTTP_REFERER']; */

        $ads = array("googleads.g.doubleclick.net",
            "yandex.ru",
            "www.google.ru",
            "google.ru",
            "www.google.com",
            "google.com",
        );


        $link = "http://site3.free-share-buttons.com";

        $parse = parse_url($link);

        if(in_array($parse['host'], $ads))
            return "ADV OR SEARCH SYSTEMS";
        $referr = false;
        $referr = Referrer::model()->findByAttributes(array("domain" => $parse['host']));

        if(!$referr)
        {
           // var_dump(self::parse($link));die;

            list($status, $rurl, $ranchor) = self::parse($link);
            switch($status)
            {
                case self::REFSPAM:
                    //Рефспам
                    break;
                case self::LINK:
                    echo "Found Link in Page".PHP_EOL;
                    $model = new Referrer();
                    $model->IP =  ip2long($ip);
                    $model->domain = $parse["host"];
                    $model->url = $link;
                    $model->first = new CDbExpression("NOW()");
                    $model->last = new CDbExpression("NOW()");
                    $model->clicks = 1;
                    $model->link_anchor = $ranchor;
                    $model->status = "LINK";
                    $model->blocked = 0;
                    if(!$model->save()){ print_r($model->getErrors());}
                    break;
                case self::NOLINK:
                    //Возможен рефспам
                    break;
            }
        }
        else
        {

            $ref = Referrer::model()->findByAttributes(array("domain" => $parse["host"], "url" => $link));
            if($ref)
            {
                $ref->clicks = $ref->clicks + 1;
                $ref->last = new CDbExpression("NOW()");
                $ref->save("clicks,last");
            }
            else
            {
                
            }
        }
    }

    private static function parse($link)
    {


        /*  $curlInit = curl_init("http://".$link);
          curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
          curl_setopt($curlInit, CURLOPT_HEADER, true);
          curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
          $text = curl_exec($curlInit);
          curl_close($curlInit);


          $text=  mb_strtolower($response); */

        $text = "<html><head><meta http-equiv='content-type' content='text/html; charset=utf-8'>"
            ."<meta http-equiv=\"refresh\" content=\"0;url=http://sharebutton.to/\"></head></html>";
//var_dump($text);
        $text = '<a class="nn" href="http://loc.yiimix.ru">Ремонт</a> <a tg="dfgd" href="https://www.alltabs.org/dfgdg" tg="dfgd" class="nn">Alltabs</a> sddf<br> <a href="http://remont.ru">remont</a>';
        //echo $text;die;
        
        $reg_exUrl = "/http-equiv\s*=\s*\"\s*refresh\s*\"/sim";

        if(preg_match($reg_exUrl, $text))
        {          
            return array(self::REFSPAM,false,false);
        }
        else
        {
            echo "None refresh";
            //Получаем все ссылки на странице
            $reg_exUrl = "/href=\"(http|https)\:\/\/([a-zA-Z0-9-\.]+\.[a-zA-Z]{2,3})(\/\S*)?/";
            
            $nums=array();
               
            if(preg_match_all($reg_exUrl, $text, $url))
            {               

                $ch = str_replace("www.", "", self::$host);
                
             
                //проверяем есть ли наш домен в ссылках
                foreach($url[2] as $k => $v)
                {                     
                    $ph = str_replace("www.", "", $v);
                      
                    if($ch == $ph)
                    {        
                        $nums[]=$k;
                    }
                }
                
                var_dump($nums);
                
                if(!empty($nums)){
                        //если есть то берем адрес ссылки и текст ссылки
                        $regex = '#href\s*=\s*["\']?([^\s>]+)?["\']?([.\s="\'a-zA-Z-_]+)?>(.*?)</a>#i';
                        if(preg_match_all($regex, $text, $u))
                        {                         
                            foreach($nums as $k => $v)
                            {                              
                                //var_dump($u[1][$v]);die;
                                   return array(self::LINK, substr($u[1][$v], 0, strlen($u[1][$v]) - 1), $u[3][$v]);
                                
                            }
                        }
                }
                
                //   Если нет ссылки на наш сайт, то в исключение
                return array(self::NOLINK);
            }
            else
            {
                //   Если ссылок не найдено, то в исключение
                return array(self::NOLINK);
            }
        }
    }

}
