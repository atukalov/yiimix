<?php

return array(
  'Home page'=>'Главная страница',  
    'First Name'=>'Имя',
    'Last name'=>'Фамилия',
    'Avatar'=>'Аватар',
    'Options'=>'Опции',
    'File Manager'=>'Менеджер файлов',
    'Pages'=>'Страницы',
    'Service'=>'Сервис',
    'Users'=>'Пользователи',
    'Lists'=>'Списки',
);

