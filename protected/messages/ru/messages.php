<?php 
return array(
"Inbox"=>"Входящие",
"Sended"=>"Отправленные",
"Drafts"=>"Черновики",
"Archive"=>"В архиве",
"Span"=>"Спам",
"Trash"=>"Корзина",
"Refresh"=>"Обновить",
"Your Message Center"=>"Центр сообщений",
"Write"=>"Написать",
"Work"=>"По работе",
"Friend"=>"Друзья",
"Project"=>"Проекты",
"For Late"=>"Не срочно",
"Important"=>"Важно",
);