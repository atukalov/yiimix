<?php

class m160710_084419_install extends CDbMigration {

    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8';

    public function up() {

        $this->createTable(Yii::app()->db->tablePrefix . 'config', array(
            "id" => "pk",
            "path" => "VARCHAR(255)",
            "value" => "VARCHAR(255)",
            "type" => "enum('none','array','string','int','real','bool','json') NOT NULL DEFAULT 'string'",
            "active" => "tinyint(1) NOT NULL DEFAULT '1'"
                ), $this->MySqlOptions);

        $this->createIndex("active", Yii::app()->db->tablePrefix . "config", "active", false);
        $this->createIndex("path", Yii::app()->db->tablePrefix . "config", "path", false);

        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "config` (`path`, `value`, `type`, `active`)
    VALUES
    ('admin.config.action.fields', 'admin.controllers.config.modules.FieldsAction', 'string', 1),
	('admin.config.action.filelib', 'admin.controllers.config.modules.FilelibAction', 'string', 1),
	('admin.config.action.index', 'admin.controllers.config.IndexAction', 'string', 1),
	('admin.config.action.install', 'admin.controllers.config.install.IndexAction', 'string', 1),
	('admin.config.action.language', 'admin.controllers.config.modules.LanguageAction', 'string', 1),
	('admin.config.action.shop', 'admin.controllers.config.ShopAction', 'string', 1),
	('admin.config.action.users', 'admin.controllers.config.modules.UsersAction', 'string', 1),
	('admin.interface.pages.articles_update', 'admin.extensions.filelib.FilesWidget', 'string', 1),
	('admin.interface.pages.articles_update', 'admin.extensions.filelib.FileLibWidget', 'string', 1),
	('admin.interface.pages.articles_update.sort', '{\"main\":[0],\"left\":[78,77]}', 'json', 1),
	('admin.interface.pages.logic_update', 'admin.widgets.fields.fieldsForItem', 'string', 1),
	('admin.modules.config', '" . Yii::t('install', 'Options') . "', 'string', 1),
	('admin.modules.filelib', '" . Yii::t('install', 'File Manager') . "', 'none', 1),
	('admin.modules.pages', '" . Yii::t('install', 'Pages') . "', 'string', 1),
	('admin.modules.sef', 'SEF', 'string', 1),
	('admin.modules.service', '" . Yii::t('install', 'Service') . "', 'string', 1),
	('admin.modules.users', '" . Yii::t('install', 'Users') . "', 'string', 1),
	('admin.pages.action.ajax_onoff', 'admin.controllers.pages.ajax.onoffAction', 'string', 1),
	('admin.pages.action.articles', 'admin.controllers.pages.articles.IndexAction', 'string', 1),
	('admin.pages.action.articles_ajax', 'admin.controllers.pages.articles.AjaxAction', 'string', 1),
	('admin.pages.action.articles_create', 'admin.controllers.pages.articles.CreateAction', 'string', 1),
	('admin.pages.action.articles_data', 'admin.controllers.pages.articles.DataAction', 'string', 1),
	('admin.pages.action.articles_delete', 'admin.controllers.pages.articles.DeleteAction', 'string', 1),
	('admin.pages.action.articles_sort', 'admin.controllers.pages.articles.SortAction', 'string', 1),
	('admin.pages.action.articles_update', 'admin.controllers.pages.articles.UpdateAction', 'string', 1),
	('admin.pages.action.fields', 'admin.controllers.pages.fields.IndexAction', 'string', 1),
	('admin.pages.action.fields_ajax', 'admin.controllers.pages.fields.AjaxAction', 'string', 1),
	('admin.pages.action.fields_create', 'admin.controllers.pages.fields.CreateAction', 'string', 1),
	('admin.pages.action.fields_delete', 'admin.controllers.pages.fields.DeleteAction', 'string', 1),
	('admin.pages.action.fields_update', 'admin.controllers.pages.fields.UpdateAction', 'string', 1),
	('admin.pages.action.index', 'admin.controllers.pages.IndexAction', 'string', 1),
	('admin.pages.action.lists', 'admin.controllers.pages.lists.IndexAction', 'string', 1),
	('admin.pages.action.lists_ajax', 'admin.controllers.pages.lists.AjaxAction', 'string', 1),
	('admin.pages.action.lists_create', 'admin.controllers.pages.lists.CreateAction', 'string', 1),
	('admin.pages.action.lists_delete', 'admin.controllers.pages.lists.DeleteAction', 'string', 1),
	('admin.pages.action.lists_update', 'admin.controllers.pages.lists.UpdateAction', 'string', 1),
	('admin.pages.action.logic', 'admin.controllers.pages.logic.IndexAction', 'string', 1),
	('admin.pages.action.logic_create', 'admin.controllers.pages.logic.CreateAction', 'string', 1),
	('admin.pages.action.logic_delete', 'admin.controllers.pages.logic.DeleteAction', 'string', 1),
	('admin.pages.action.logic_edit', 'admin.controllers.pages.logic.EditAction', 'string', 1),
	('admin.pages.action.logic_new', 'admin.controllers.pages.logic.NewAction', 'string', 1),
	('admin.pages.action.logic_save', 'admin.controllers.pages.logic.SaveAction', 'string', 1),
	('admin.pages.action.logic_update', 'admin.controllers.pages.logic.UpdateAction', 'string', 1),
	('admin.pages.action.menu', 'admin.controllers.pages.menu.IndexAction', 'string', 1),
	('admin.pages.action.menu_create', 'admin.controllers.pages.menu.CreateAction', 'string', 1),
	('admin.pages.action.menu_delete', 'admin.controllers.pages.menu.DeleteAction', 'string', 1),
	('admin.pages.action.menu_update', 'admin.controllers.pages.menu.UpdateAction', 'string', 1),
	('admin.pages.action.price', 'admin.controllers.pages.price.IndexAction', 'string', 1),
	('admin.pages.action.price_delete', 'admin.controllers.pages.price.DeleteAction', 'string', 1),
	('admin.pages.action.price_qsave', 'admin.controllers.pages.price.QSaveAction', 'string', 1),
	('admin.pages.action.price_update', 'admin.controllers.pages.price.UpdateAction', 'string', 1),
	('admin.pages.behavior.test', 'application.modules.admin.test', 'none', 1),
	('admin.profile.action.index', 'admin.controllers.profile.IndexAction', 'string', 1),
	('admin.profile.action.planner', 'admin.controllers.profile.planner.IndexAction', 'string', 1),
	('admin.sef.action.ajax_create', 'admin.controllers.sef.ajax.CreateAction', 'string', 1),
	('admin.sef.action.delete', 'admin.controllers.sef.DeleteAction', 'string', 1),
	('admin.sef.action.index', 'admin.controllers.sef.IndexAction', 'string', 1),
	('admin.service.action.files', 'admin.controllers.service.files.IndexAction', 'string', 1),
	('admin.service.action.index', 'admin.controllers.service.IndexAction', 'string', 1),
	('admin.service.action.updater', 'admin.controllers.service.updater.IndexAction', 'string', 1),
	('admin.service.action.updater_check', 'admin.controllers.service.updater.CheckAction', 'string', 1),
	('admin.service.action.updater_update', 'admin.controllers.service.updater.UpdateAction', 'string', 1),
	('admin.users.action.ajax', 'admin.controllers.users.ajax.IndexAction', 'string', 1),
	('admin.users.action.ajax_position', 'admin.controllers.users.ajax.PosAction', 'string', 1),
	('admin.users.action.create', 'admin.controllers.users.CreateAction', 'string', 1),
	('admin.users.action.index', 'admin.controllers.users.IndexAction', 'string', 1),
	('admin.users.action.messages', 'admin.controllers.users.messages.IndexAction', 'string', 1),
	('admin.users.action.profile', 'admin.controllers.users.ProfileAction', 'string', 1),
	('admin.users.action.rbac', 'admin.controllers.users.rbac.IndexAction', 'string', 1),
	('admin.users.action.update', 'admin.controllers.users.UpdateAction', 'string', 1),
	('behaviors.model.lists', 'application.modules.admin.extensions.behaviors.FileLibBehavior', 'string', 1),
	('behaviors.model.lists', 'application.components.FieldsBehavior', 'string', 1),
	('behaviors.model.pages', 'application.modules.admin.extensions.behaviors.FileLibBehavior', 'string', 1),
	('behaviors.model.pages', 'application.components.FieldsBehavior', 'string', 1),
	('behaviors.model.user', 'application.components.FieldsBehavior', 'string', 1),
	('behaviors.model.user', 'application.modules.admin.extensions.behaviors.FileLibBehavior', 'string', 1),
	('config.quality.png', '6', 'int', 1),
	('config.user.activeAfterRegister', '0', 'bool', 1),
	('config.user.autoLogin', '1', 'bool', 1),
	('config.user.loginNotActiv', '0', 'bool', 1),
	('config.user.sendActivationMail', '1', 'bool', 1),
	('fields.cerber', '[\"lists\",\"pages\"]', 'json', 1),
	('fields.entity.lists', '" . Yii::t('install', 'Lists') . "', 'string', 1),
	('fields.entity.pages', '" . Yii::t('install', 'Pages') . "', 'string', 1),
	('fields.entity.profiles', '" . Yii::t('install', 'Users') . "', 'string', 1),
	('filelib.max.landscape', '1045x696', 'string', 1),
	('filelib.max.portland', '1045x696', 'string', 1),
	('filelib.paths', '/uploads', 'string', 1),
	('filelib.paths', '/images', 'string', 1),
	('filelib.quality.jpeg', '6', 'int', 1),
	('filelib.quality.png', '6', 'int', 1),
	('filelib.thumbs.size', '273x182', 'string', 1),
	('filelib.thumbs.size', '130x100', 'string', 1),
	('pages.showkeyword', '0', 'bool', 1),
	('pages.titlefield', 'title', 'string', 1),
	('site.lang.admin', '".Yii::app()->language."', 'string', 1),
	('site.lang.default', '".Yii::app()->language."', 'string', 1),
	('site.lang.language', 'ru:Русский', 'string', 1),
	('site.lang.language', 'en:English', 'string', 1),
	('user.panel.widgets.messages', 'admin.widgets.user.UWMessages', 'string', 1),
	('user.panel.widgets.profile', 'admin.widgets.user.UWProfile', 'string', 1),
	('user.panel.widgets.project', 'admin.widgets.user.UWProject', 'string', 1);";
        $this->execute($sql);



        $this->createTable(Yii::app()->db->tablePrefix . "fields", array(
            "id" => "pk",
            "title" => "VARCHAR(50)  DEFAULT NULL",
            "name" => "VARCHAR(50)  DEFAULT NULL",
            "type" => "enum('DIGIT','STR','TEXT','DATE','BOOL') NOT NULL DEFAULT 'STR'",
            "multi" => " tinyint(1) NOT NULL DEFAULT '0'",
            "all" => "tinyint(1) NOT NULL DEFAULT '0'",
            "req" => "tinyint(1) NOT NULL DEFAULT '0'",
            "input" => "enum('text','area','editor','file','date','check','select','custom') NOT NULL DEFAULT 'text'",
            "data" => "text",
            "custom" => "varchar(255) DEFAULT NULL",
            "target" => "varchar(50) DEFAULT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("name", Yii::app()->db->tablePrefix . "fields", array("name", "target"), true);
        $sql = "INSERT INTO `tbl_fields` (`id`, `title`, `name`, `type`, `multi`, `all`, `req`, `input`, `data`, `custom`, `target`)
VALUES
	(1, '" . Yii::t('install', 'First Name') . "', 'first_name', 'STR', 0, 0, 0, 'text', NULL, NULL, 'profiles'),
	(2, '" . Yii::t('install', 'Last name') . "', 'last_name', 'STR', 0, 0, 0, 'text', NULL, NULL, 'profiles'),
	(3, '" . Yii::t('install', 'Avatar') . "', 'avatar', 'STR', 0, 0, 0, 'text', NULL, NULL, 'profiles');
";
        $this->execute($sql);


        $this->createTable(Yii::app()->db->tablePrefix . "action_fields", array(
            "id" => "pk",
            "action" => "int(11) NOT NULL",
            "field" => "int(11) NOT NULL",
            "target" => "varchar(50) NOT NULL DEFAULT ''",
            "name" => "varchar(100) DEFAULT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("action", Yii::app()->db->tablePrefix . "action_fields", "action", false);
        $this->createIndex("field", Yii::app()->db->tablePrefix . "action_fields", "field", false);
        $this->createIndex("entity", Yii::app()->db->tablePrefix . "action_fields", "target", false);


        $this->createTable(Yii::app()->db->tablePrefix . "actions", array(
            "id" => "pk",
            "cat" => "int(11) NOT NULL",
            "title" => "varchar(100) DEFAULT NULL",
            "name" => "varchar(50) DEFAULT NULL",
            "fields" => "text",
                ), $this->MySqlOptions);

        $this->createIndex("cat", Yii::app()->db->tablePrefix . "actions", "cat", false);
        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "actions` (`id`, `cat`, `title`, `name`, `fields`) VALUES (1, 1, '" . Yii::t('install', 'Home page') . "', 'index', 'text')";
        $this->execute($sql);

        $this->createTable(Yii::app()->db->tablePrefix . "filelib", array(
            "id" => "pk",
            "target" => "enum('pages','lists','profiles') NOT NULL DEFAULT 'pages'",
            "item" => "int(11) NOT NULL DEFAULT '0'",
            "filename" => "varchar(255) NOT NULL",
            "alt" => "varchar(255) NOT NULL DEFAULT ''",
            "title" => "varchar(255) NOT NULL DEFAULT ''",
            "pos" => "int(11) NOT NULL DEFAULT '0'",
            "kind" => "enum('image','sound','pdf','arch') NOT NULL DEFAULT 'image'",
            "size" => "varchar(12) DEFAULT NULL",
            "primary" => "tinyint(1) NOT NULL DEFAULT '0'",
            "errors" => "varchar(255) DEFAULT NULL",
            "original" => "tinyint(1) DEFAULT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("entity", Yii::app()->db->tablePrefix . "filelib", array("target", "item"), false);

        $this->createTable(Yii::app()->db->tablePrefix . "lists", array(
            "id" => "pk",
            "parent" => "int(11) NOT NULL DEFAULT '0'",
            "name" => "varchar(255) DEFAULT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("parent", Yii::app()->db->tablePrefix . "lists", "parent", false);

        $this->createTable(Yii::app()->db->tablePrefix . "menus", array(
            "id" => "pk",
            "parent" => "int(11) NOT NULL",
            "name" => "varchar(255) NOT NULL DEFAULT ''",
            "url" => "varchar(255) NOT NULL DEFAULT ''",
            "pos" => "int(11) NOT NULL DEFAULT '0'",
            "options" => "varchar(255) NOT NULL DEFAULT ''",
            "icon" => "varchar(255) NOT NULL DEFAULT ''",
            "query" => "varchar(255) NOT NULL DEFAULT ''",
            "op" => "tinyint(3) NOT NULL DEFAULT '0'",
            "main_id" => "int(11) NOT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("parent", Yii::app()->db->tablePrefix . "menus", "parent", false);
        $this->createIndex("pos", Yii::app()->db->tablePrefix . "menus", "pos", false);
        $this->createIndex("main_id", Yii::app()->db->tablePrefix . "menus", "main_id", false);


        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "menus` (`id`, `parent`, `name`, `url`, `pos`, `options`, `icon`, `query`, `op`, `main_id`)
    VALUES
	(1, 0, 'main', '', 0, '', '', '', 0, 1),
	(2, 0, 'admin', '', 0, 'id=\"cssmenu\"', '', '', 2, 2),
	(3, 2, 'Dashboard', 'admin', 0, '', 'gi gi-home sidebar-nav-icon', '', 0, 2),
	(4, 2, 'Pages', 'admin/pages', 1, '', 'gi gi-book_open', '', 1, 2),
	(5, 4, 'Navigation', 'admin/pages/menu', 0, '', 'gi gi-shoe_steps sidebar-nav-icon', '', 0, 2),
	(6, 4, 'Articles', 'admin/pages/articles', 1, '', 'gi gi-book sidebar-nav-icon', '', 0, 2),
	(7, 4, 'Lists', 'admin/pages/lists', 2, '', 'fa fa-list sidebar-nav-icon', '', 0, 2),
	(8, 4, 'Templates', 'admin/pages/logic', 2, '', 'gi gi-display sidebar-nav-icon', '', 0, 2),
	(9, 2, 'Modules', '', 2, '', 'gi gi-cogwheel', '', 1, 2),
	(10, 9, 'Users', 'admin/users', 0, '', 'gi gi-user sidebar-nav-icon', '', 0, 2),
	(11, 9, 'Config', 'admin/config', 1, '', 'fa fa-wrench sidebar-nav-icon', '', 0, 2);";

        $this->execute($sql);

        $this->createTable(Yii::app()->db->tablePrefix . "pages", array(
            "id" => "pk",
            "parent" => "int(11) NOT NULL",
            "url" => "varchar(255) NOT NULL DEFAULT ''",
            "action" => "varchar(100) NOT NULL DEFAULT ''",
            "title" => "varchar(150) NOT NULL DEFAULT ''",
            "text" => "longtext NOT NULL",
            "cdate" => "datetime NOT NULL",
            "udate" => "datetime NOT NULL",
            "active" => "tinyint(1) NOT NULL DEFAULT '1'",
            "keywords" => "varchar(255) NOT NULL DEFAULT ''",
            "description" => "varchar(255) NOT NULL DEFAULT ''",
            "views" => "int(11) NOT NULL DEFAULT '0'",
            "position" => "int(11) DEFAULT NULL",
                ), $this->MySqlOptions);

        $this->createIndex("url", Yii::app()->db->tablePrefix . "pages", "url", false);
        $this->createIndex("parent", Yii::app()->db->tablePrefix . "pages", "parent", false);
        $this->createIndex("action", Yii::app()->db->tablePrefix . "pages", "action", false);
        $this->createIndex("views", Yii::app()->db->tablePrefix . "pages", "views", false);
        $this->createIndex("position", Yii::app()->db->tablePrefix . "pages", "position", false);

        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "pages` (`id`, `parent`, `url`, `action`, `title`, `text`, `cdate`, `udate`, `active`, `keywords`, `description`, `views`, `position`) VALUES "
                . "(1, 0, '', 'index', '" . Yii::t('install', 'Home page') . "', '', NOW(), NOW(), 1, '', '', 0, 0);";
        $this->execute($sql);

        $this->createTable(Yii::app()->db->tablePrefix . "user", array(
            "id" => "pk",
            "username" => "varchar(20) NOT NULL DEFAULT ''",
            "password" => "varchar(128) NOT NULL DEFAULT ''",
            "email" => "varchar(128) NOT NULL DEFAULT ''",
            "activkey" => "varchar(128) NOT NULL DEFAULT ''",
            "superuser" => "int(1) NOT NULL DEFAULT '0'",
            "status" => "int(1) NOT NULL DEFAULT '0'",
            "create_at" => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
            "lastvisit_at" => "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
                ), $this->MySqlOptions);
        $this->createIndex("username", Yii::app()->db->tablePrefix . "user", "username", true);
        $this->createIndex("user_email", Yii::app()->db->tablePrefix . "user", "email", true);
        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "user` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`) VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '853667a784412b2588cb655e98a1ce6d', 1, 1, NOW(), NOW());";
        $this->execute($sql);

        $this->createTable(Yii::app()->db->tablePrefix . "profiles", array(
            "id" => "pk",
            "first_name" => "varchar(255) NOT NULL DEFAULT ''",
            "last_name" => "varchar(255) NOT NULL DEFAULT ''",
            "avatar" => "varchar(255) NOT NULL DEFAULT ''",
                ), $this->MySqlOptions);

        $sql = "INSERT INTO `" . Yii::app()->db->tablePrefix . "profiles` (`id`) VALUES (1);";
        $this->execute($sql);

        $this->createTable(Yii::app()->db->tablePrefix . "urls", array(
            "id" => "pk",
            "url" => "varchar(255) NOT NULL DEFAULT ''",
            "target" => "varchar(100) NOT NULL DEFAULT ''",
            "item" => "int(11) NOT NULL DEFAULT '0'",
            "active" => "tinyint(1) NOT NULL DEFAULT '0'",
                ), $this->MySqlOptions);
        $this->createIndex("item", Yii::app()->db->tablePrefix . "urls", array("target", "item"), false);
        $this->createIndex("active", Yii::app()->db->tablePrefix . "urls", "active", false);
    }

    public function down() {
        echo "m160710_084419_install does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
