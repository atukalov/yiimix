<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/**
 * This is the model class for table "{{action_fields}}".
 *
 * The followings are the available columns in table '{{action_fields}}':
 * @property string $id
 * @property integer $action
 * @property integer $field
 * @property integer $target
 * @property string $name
 */
class ActionFields extends XActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{action_fields}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('action, field', 'numerical', 'integerOnly' => true),
            array('target', 'length', 'max' => 50),
            array('name', 'length', 'max' => 100),
           
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, action, field', 'safe', 'on' => 'search'),
        );
    }

    public function behaviors()
    {
        return array();
        
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rField' => array(self::BELONGS_TO, 'Fields', 'field')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'action' => 'Action',
            'field' => 'Field',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('action', $this->action);
        $criteria->compare('field', $this->field);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ActionFields the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function label($form, $model)
    {
        return $form->labelEx($model, $this->rField->name);
    }

    public function input($form, $model)
    {

        switch($this->rField->input)
        {
            case 'select':
                if(!empty($this->rField->data) && substr($this->rField->data, 0, 4) == 'sql:')
                {
                    $res = Yii::app()->db->
                            createCommand(substr($this->rField->data, 5, strlen($this->rField->data)))->
                            setFetchMode(PDO::FETCH_NUM)->queryAll();
                    if($res)
                    {
                        $t = array();
                        foreach($res as $v)
                            $t[$v[0]] = $v[1];
                    }
                    else
                    {
                        $t = array();
                    }
                }
                return $form->dropDownList($model, $this->rField->name, $t);
                break;
            case 'area':
                return $form->textarea($model, $this->rField->name);
                break;
                break;
            case 'editor':
                return $form->textarea($model, $this->rField->name, array('class' => 'ckeditor'));
                break;
            case 'check':
                $data = self::getData($this->rField->data);
                if($this->rField->multi == 0)
                {
                    return $form->radioButtonList($model, $this->rField->name, $data);
                }
                else
                {
                    $s = $this->rField->name;
                    //$model->$s=explode(',', $model->$s);					
                    return $form->checkboxList($model, $this->rField->name, $data);
                }
                break;
            default:
                return $form->textField($model, $this->rField->name);
                break;
        }
    }

    private static function getType($str)
    {

        if(empty($str))
            $d = false;
        if(substr($str, 0, 4) == 'sql:')
            $d = 'sql';
        elseif(substr($str, 0, 5) == 'json:')
            $d = 'json';

        return $d;
    }

    private static function getData($str)
    {
        $dt = self::getType($str);
        //	if(!$dt)
        //	return false;

        if($dt == 'sql')
        {
            $res = Yii::app()->db->
                    createCommand(substr($str, 5, strlen($str)))->
                    setFetchMode(PDO::FETCH_NUM)->queryAll();
            if($res)
            {
                $t = array();
                foreach($res as $v)
                    $t[$v[0]] = $v[1];
            }
            else
            {
                $t = array();
            }
        }
        elseif($dt == 'json')
        {
            $data = json_decode(substr($str, 6, strlen($str)));
            return $data;
        }
    }

}
