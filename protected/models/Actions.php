<?php

/**
 * This is the model class for table "{{actions}}".
 *
 * The followings are the available columns in table '{{actions}}':
 * @property string $id
 * @property integer $cat
 * @property string $title
 * @property string $name
 * @property string $fields
 */
class Actions extends XActiveRecord {

    public $oldname;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{actions}}';
    }

    public function behaviors() {
        return array();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cat', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 100),
            array('name', 'length', 'max' => 50),
            array('title, name', 'filter', 'filter' => 'trim'),
            array('fields', 'unsafe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, cat, title, name, fields', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rFields' => array(self::HAS_MANY, 'ActionFields', 'action'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'cat' => 'Cat',
            'title' => 'Title',
            'name' => 'Name',
            'fields' => 'Fields',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('cat', $this->cat);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('fields', $this->fields, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Actions the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function adminMenu() {
        $l = self::model()->findAll();
        $temp = array();
        if ($l)
            foreach ($l as $k => $v) {
                $temp["a" . $v->id] = array(
                    "id" => "a" . $v->id,
                    "parent" => 0,
                    "name" => $v->title,
                    "url" => Yii::app()->createUrl("admin/pages/logic", array("action" => $v->name)),
                    "op" => 0,
                    "query" => "",
                    "icon" => "",
                    "options" => "",
                    "pos" => $k,
                    "sub" => array(),
                );
            }
        return $temp;
    }

}
