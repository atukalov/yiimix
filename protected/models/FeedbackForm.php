<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FeedbackForm extends CFormModel {

    public $uname;
    public $umail;
    public $utel;
    public $umessage;
    public $uwork;
    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('uname, utel,umail,uwork, umessage', 'required'),
            // email has to be a valid email address
            array('umail', 'email'),
                // verifyCode needs to be entered correctly
                //array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'uname' => 'Имя',
            'umail' => 'E-mail',
            'utel' => 'Телефон',
            'umessage' => 'Сообщение',
        );
    }

}