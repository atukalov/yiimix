<?php
/*
 * Модель Fields - дополнительные поля для таблиц
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 *
 * This is the model class for table "{{fields}}".
 *
 * The followings are the available columns in table '{{fields}}':
 * @property string $id
 * @property string $title
 * @property string $name
 * @property string $type
 * @property integer $multi
 * @property integer $all
 * @property integer $req
 * @property string $input
 * @property string $data
 * @property string $custom
 * @property string $target
 */

class Fields extends XActiveRecord {

    public $oldname;
    public $oldtarget;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{fields}}';
    }

    public function behaviors() {
        return array();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('multi, all, req', 'numerical', 'integerOnly' => true),
            array('title, name, target', 'length', 'max' => 50),
            array('type', 'length', 'max' => 5),
            array('input', 'length', 'max' => 6),
            array('custom', 'length', 'max' => 255),
            array('data', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, name, type, multi, all, req, input, data, custom', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => Yii::t('admin', 'Title'),
            'name' => Yii::t('admin', 'Var'),
            'type' => Yii::t('admin', 'Type'),
            'multi' => Yii::t('admin', 'Multi'),
            'all' => Yii::t('admin', 'All'),
            'req' => Yii::t('admin', 'Required'),
            'input' => Yii::t('admin', 'Input'),
            'data' => Yii::t('admin', 'Datas'),
            'custom' => Yii::t('admin', 'Custom input'),
            'target' => Yii::t('admin', 'Table')
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('multi', $this->multi);
        $criteria->compare('all', $this->all);
        $criteria->compare('req', $this->req);
        $criteria->compare('input', $this->input, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('custom', $this->custom, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getValues() {
        $res = Config::get('fields.entity');
        $temp = array();
        if (!empty($res))
            foreach ($res as $k => $v) {
                $temp[$k] = $v[0];
            }
        //var_dump($res);
        return $temp;
    }

    public static function isChecked($action, $target, $id) {
        return ActionFields::model()->findByAttributes(array('action' => $action, 'target' => $target, 'field' => $id)) ? 1 : 0;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Fields the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function label($form, $model) {
        
    }

    /**
     * Получает начальные данные для поля если они есть
     * @return array
     */
    public function getData() {
        $t = array();
        if (empty($this->data)) {
            return $t;
        }
        $m = substr($this->data, 0, 4);

        if ($m == 'sql:') {
            $t = $this->getSqlData();
        } elseif ($m == 'json') {
            $t = json_decode(substr($this->data, 6, strlen($this->data)));
        }
        return $t;
    }

    public function getSqlData() {
        $t = array();
        $res = Yii::app()->db->
                        createCommand(substr($this->data, 5, strlen($this->data)))->
                        setFetchMode(PDO::FETCH_NUM)->queryAll();
        if ($res) {
            foreach ($res as $v) {
                $t[$v[0]] = $v[1];
            }
        }
        return $t;
    }

    /**
     * Вывод поле ввода для оп поля     *
     * @param CActiveForm $form текущая форма.
     * @param Pages or Lists $model текущая модель.
     * @return Код поля ввода
     */
    public function input($form, $model) {
        if ($this->input != "hidden") {
            ?>
            <div class="form-group">
            <?= $form->labelEx($model, $this->title, array("class" => "col-md-2 control-label")); ?>   	

                <?php
            }
            switch ($this->input) {
                case "file":
                    ?><div class="col-md-10"><?php
                    $cs = Yii::app()->getClientScript();
                    $cs->registerScriptFile('/js/ant/fileinput.min.js', CClientScript::POS_END);
                    $cs->registerCssFile('/css/ant/fileinput.min.css');
                    echo $form->fileField($model, $this->name);
                    ?></div><?php
                    break;
                case "select":
                    ?><div class="col-md-10"><?php
                    if ($this->multi == 1) {
                        $c = $this->name;
                        $model->$c = explode(",", rtrim($model->$c, ","));
                        echo $form->dropDownList($model, $this->name, $this->getData(), array("class" => "form-control select-chosen", "multiple" => "multiple"));
                    } else {
                        echo $form->dropDownList($model, $this->name, $this->getData(), array("class" => "form-control select-chosen"));
                    }
                    echo "</div>";
                    break;

                case "area":
                case "text":
                    ?><div class="col-md-10"><?php
                        if ($this->type == "TEXT")
                            echo $form->textarea($model, $this->name, array("class" => "form-control"));
                        else
                            echo $form->textField($model, $this->name, array("class" => "form-control"));
                        ?></div><?php
                        break;

                    case "editor":
                        ?><div class="col-md-10"><?php
                        echo $form->textarea($model, $this->name, array('class' => 'ckeditor'));
                        ?></div><?php
                            break;
                        case "check":
                            if ($this->multi == 0) {
                                echo $form->radioButtonList($model, $this->name, $this->getData());
                            } else {
                                echo $form->checkboxList($model, $this->name, $this->getData());
                            }
                            break;
                        default:
                            ?><div class="col-md-10"><?php
                        echo $form->textField($model, $this->name, array("style" => "width:100%", "class" => "form-control"));
                        ?></div><?php
                            break;
                    }
                    if ($this->input != "hidden") {
                        ?>

                </div>
            <?php
        }
    }

    /**
     * Проверяет и если нет то добавляет c_ к имени переменной
     */
    public function cerber() {
        $r=Config::get('fields.cerber');
       
        $r=$r?$r['cerber']:false;
        if(!$r) Yii::trace('Error: Fields cerber options not found');   
        if(!in_array($this->target,$r)) return;
        
        if (substr($this->name, 0, 2) !== 'c_') {
            $this->name = 'c_' . $this->name;
        }
    }

    /**
     * Returns the AR rule for Field
     * @return array
     */
    public function getRule() {
        $rule = false;
        switch ($this->type) {
            case 'BOOL':
            case 'DIGIT':
                $rule = array($this->name, 'numerical', 'integerOnly' => true);
                break;
            case 'STR':
                if ($this->input == 'file')
                    $rule = array($this->name, 'file', 'types' => 'jpg, gif, png, doc, pdf, zip', 'allowEmpty' => true);
                else
                    $rule = array($this->name, 'length', 'max' => 255);
                break;
            default:
                $rule = array($this->name, 'safe');
        }

        return $rule;
    }

    public static function realType($_type) {
        switch ($_type) {
            case "DIGIT":
                $type = "int(11)";
                break;
            case "TEXT":
                $type = "text";
                break;
            case "DATE":
                $type = "datetime";
                break;
            case "BOOL":
                $type = "tinyint(1)";
                break;
            default:
                $type = "varchar(255)";
        }
        return $type;
    }

    public function fieldUpdate() {

        //Переименовываем поле
        if (strcasecmp($this->oldname, $this->name) !== 0) {
            if (strcasecmp($this->oldtarget, $this->target) === 0)
                Yii::app()->db->createCommand()->renameColumn("{{" . $this->target . "}}", $this->oldname, $this->name);
            else
                Yii::app()->db->createCommand()->dropColumn("{{" . $this->oldtarget . "}}", $this->oldname);
        }

        //перенос поля в другую таблицу
        if (strcasecmp($this->oldtarget, $this->target) !== 0) {
            $this->fieldInsert();
            $this->fieldDelete($this->oldtarget);
        }


        //Проверяем изменился ли тип поля
        $m = ucfirst($this->target);
        if ($m::model()->hasAttribute($this->name) &&
                $m::model()->getMetaData()->columns[$this->name]->dbType !== self::realType($this->type))
            Yii::app()->db->createCommand()->alterColumn("{{" . $this->target . "}}", $this->name, self::realType($this->type));
    }

    public function fieldInsert() {

        $m = ucfirst($this->target);
        if (!$m::model()->hasAttribute($this->name)) {
            Yii::app()->db->createCommand()->addColumn($m::model()->tableName(), $this->name, self::realType($this->type));
        } else
            return false;
    }

    public function fieldDelete($target = null) {

        if ($target === null)
            $target = $this->target;
        $m = ucfirst($target);

        ActionFields::model()->deleteAllByAttributes(array('field' => $this->id, 'target' => $target));

        if ($m::model()->hasAttribute($this->name)) {
            $res = Yii::app()->db->createCommand()->dropColumn($m::model()->tableName(), $this->name);
        }
        return $res;
    }

    public static function parse($params, &$model) {

        $ids = $model->getFields();

        $model->attributes = $params;

        if (!empty($ids))
            foreach ($ids as $k => $v) {
                if ($v->multi == 1) {
                    $c = $v->name;
                    if (is_array($model->$c)) {
                        $model->$c = "," . join(",", $model->$c) . ",";
                    }
                }
            }

        // return $params;
    }

}
