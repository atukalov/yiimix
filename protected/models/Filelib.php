<?php

/**
 * This is the model class for table "{{filelib}}".
 *
 * The followings are the available columns in table '{{filelib}}':
 * @property string $id
 * @property string $entity
 * @property integer $item
 * @property string $filename
 * @property string $alt
 * @property string $title
 * @property integer $pos
 */
class Filelib extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{filelib}}';
    }

  
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('item,pos', 'numerical', 'integerOnly' => true),
            array('target', 'length', 'max' => 100),
            array('filename, alt, title', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, target, item, filename, alt, title', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'target' => 'Таблица',
            'item' => 'ID элемента',
            'filename' => 'Имя файла',
            'alt' => 'Alt',
            'title' => 'Title',
            'pos' => 'Порядок',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('target', $this->target, true);
        $criteria->compare('item', $this->item);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('alt', $this->alt, true);
        $criteria->compare('title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Filelib the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function imageSrc()
    {

        return "/uploads/".$this->target."/".$this->item."/".$this->filename;
    }

}
