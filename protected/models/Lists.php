<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1

 * This is the model class for table "{{cats_items}}".
 *
 * The followings are the available columns in table '{{cats_items}}':
 * @property string $id
 * @property integer $parent
 * @property string $name
 */

class Lists extends XActiveRecord {

    public $entity = "lists";

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{lists}}';
    }

    public function behaviors() {
        return array(
            'FieldsBehavior' => array('class' => 'application.components.FieldsBehavior'),
            'FileLib' => array('class' => 'application.modules.admin.extensions.behaviors.FileLibBehavior'),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('parent', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('id, parent, name', 'safe', 'on' => 'search')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent' => 'ID предка',
            'name' => 'Название',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('cat', $this->parent);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CatsItems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function __call($name, $parameters) {

        parent::__call($name, $parameters);
    }

    public static function getParent($id) {

        return self::model()->findByPk($id);
    }

    public static function getParents($id) {
        $ids = array();
        $i = $id;
        while ($b = self::getParent($i)) {
            $ids[] = $b->id;
            if ($b->parent == 0)
                break;
            $i = $b->parent;
        }
        //$ids=array_reverse($ids);
        return self::model()->findAllByPk($ids);
    }

    public function getElements($nak = true) {

        $res = Lists::model()->findAllByAttributes(array("parent" => $this->id));

        if ($nak) {
            $temp = array();

            foreach ($res as $v) {
                $temp[$v->name] = $v;
            }
            return $temp;
        } else
            return $res;
    }

    public function getUID() {
        if ($this->parent == 0)
            return $this->id;
        else
            return $this->parent;
    }

}
