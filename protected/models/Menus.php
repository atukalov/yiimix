<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/**
 * This is the model class for table "{{menus}}".
 *
 * The followings are the available columns in table '{{menus}}':
 * @property string $id
 * @property integer $parent
 * @property string $name
 * @property string $url
 * @property integer $pos
 * @property string $options
 * @property string $icon
 * @property string $query
 * @property integer $op
 * @property integer $main_id
 */
class Menus extends XActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{menus}}';
    }

      public function behaviors()
    {
        return array();
    }

    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent,pos,op,main_id', 'numerical', 'integerOnly' => true),
            array('name, url, options,icon,query', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent, name, url', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rParent' => array(self::BELONGS_TO, 'Menus', array('parent' => 'id')),
        );
    }

    public static function getParent($id) {

        return Menus::model()->findByPk($id);
    }

    public static function getParents($id) {
        $ids = array();
        $i = $id;
        while ($b = self::getParent($i)) {
            $ids[] = $b->id;
            if ($b->parent == 0)
                break;
            $i = $b->parent;
        }
        //$ids=array_reverse($ids);
        return Menus::model()->findAllByPk($ids);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent' => 'Parent',
            'name' => 'Name',
            'url' => 'Url',
            'pos' => 'Позиция',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent', $this->parent);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Menus the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function adminBreadcrumbs($id, $op = false) {
        $temp = array();

        if ($id !=0) {
            $i = self::model()->findByAttributes(array("id" => intval($id)));
            if ($i) {
                if (!$op)
                    $temp[] = $i->name;
                else{
                    $temp[$i->name] = "/admin/pages/menu/" . $i->id;
                    $op=false;
                }
                if ($i->parent != 0)
                    $temp = array_merge(self::adminBreadcrumbs($i->parent, true), $temp);
            }
        }
        if (!$op) {
            if(empty($temp))
                $temp=array_merge(array("Навигация"),$temp);
            else
               $temp=array_merge(array("Навигация"=>"/admin/pages/menu/"),$temp);
        }

        //die;
        return $temp;
    }

}
