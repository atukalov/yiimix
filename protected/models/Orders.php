<?php

/**
 * This is the model class for table "{{orders}}".
 *
 * The followings are the available columns in table '{{orders}}':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $from
 * @property string $to
 * @property string $weight
 * @property string $volume
 * @property string $date
 * @property string $comment
 * @property string $file
 * @property integer $page_id
 * @property string $grus_number
 */
class Orders extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{orders}}';
    }

    public function behaviors() {
        return array();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('kind, page_id', 'required'),
            array('name, phone, email, from, to, volume, date, comment, weight', 'required', 'on' => 'full_order'),
            array('name, phone,  page_id, from, to,volume, weight', 'required', 'on' => 'order'),
            array('phone', 'required', 'on' => 'call'),
            array('grus_number', 'required', 'on' => 'gruz'),
            array('page_id', 'numerical', 'integerOnly' => true),
            array('name, phone, email, from, to, volume, file, weight,grus_number', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, phone, email, from, to, volume, date, comment, file, page_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'from' => 'Откуда',
            'to' => 'Куда',
            'volume' => 'Объем',
            'weight' => 'Вес',
            'date' => 'Дата',
            'comment' => 'Прримечание',
            'file' => 'Файл',
            'page_id' => 'Страница',
            'kind' => 'Тип запроса',
            'grus_number' => 'Номер груза',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('from', $this->from, true);
        $criteria->compare('to', $this->to, true);
        $criteria->compare('volume', $this->volume, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('file', $this->file, true);
        $criteria->compare('page_id', $this->page_id);
        $criteria->order = 'date DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'countCriteria' =>
            array(
                'condition' => $criteria->condition,
                'params' => $criteria->params
            ),
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page',
            ),
        ));
    }

    public static function getKind($id) {

        $model = self::model()->findByPk($id);
        switch ($model->kind) {
            case 1:
                return "Обратный звонок";
                break;
            case 2:
                return "Расчет доставки";
                break;
            case 3:
                return "Заказ";
                break;
            case 4:
                return "Пребывание груза";
                break;
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Orders the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
