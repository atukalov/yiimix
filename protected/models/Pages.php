<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1

 * This is the model class for table "{{pages}}".
 *
 * The followings are the available columns in table '{{pages}}':
 * @property string $id
 * @property integer $parent
 * @property string $action
 * @property string $title
 * @property string $url
 * @property string $text
 * @property string $cdate
 * @property string $udate
 * @property integer $active
 * @property string $keywords
 * @property string $description
 * @property integer $views
 * @property integer $position
 */

class Pages extends XActiveRecord {

    public $entity = "pages";
    public $oldUrl = false;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{pages}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array(
            array('parent, active, views, position', 'numerical', 'integerOnly' => true),
            array('action', 'length', 'max' => 100),
            array('title', 'length', 'max' => 150),
            array('url', 'length', 'max' => 200),
            array('keywords, description', 'length', 'max' => 255),
            array('title,url', 'filter', 'filter' => 'trim'),
            array('text, cdate, udate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('title, url, description, parent', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rAction' => array(self::HAS_ONE, 'Actions', array('name' => 'action')),
            'rSef' => array(self::HAS_MANY, 'Urls', array('item' => 'id'), 'condition' => '`rSef`.`target` = "pages"'),
        );
    }

    public function behaviors() {
        return array(
            'FieldsBehavior' => array('class' => 'application.components.FieldsBehavior'),
            'FileLib' => array('class' => 'application.modules.admin.extensions.behaviors.FileLibBehavior'),
        );
    }

    public static function getParent($id) {

        return self::model()->findByPk($id);
    }

    public static function getParents($id) {
        $ids = array();
        $i = $id;
        while ($b = self::getParent($i)) {
            $ids[] = $b->id;
            if ($b->parent == 0) {
                break;
            }
            $i = $b->parent;
        }
        return self::model()->findAllByPk($ids);
    }

    public function getBreadcrumb() {
        $ids = array();
        $i = $this->id;

        while ($b = self::getParent($i)) {
            $ids[] = $b->id;
            if ($b->parent == 0) {
                break;
            }
            $i = $b->parent;
        }

        $el = self::model()->findAllByPk($ids);

        $temp = array();
        foreach ($el as $v) {
            if ($v->id !== $this->id && $v->id > 1)
                $temp[$v->title] = $v->url;
        }

        $temp[] = $this->title;
        return $temp;
    }

    private static function toMenu($t, $item, $ac = false) {

        $action = ($ac !== false) ? $ac : "pages/" . $item->action;

        if (empty($t))
            $t[] = array("label" => $item->title, "url" => array($action, "id" => $item->id), "id" => $item->id);
        else {
            foreach ($t as $k => $v)
                if ($v["id"] == $item->parent)
                    $t[$k]["items"][] = array("label" => $item->title, "url" => array($action, "id" => $item->id), "id" => $item->id);
        }
        return $t;
    }

    public function getParentsAsMenu($action = false) {

        $p = self::getParents($this->id);

        $temp = array();
        if ($p)
            foreach ($p as $k => $v) {
                $temp = self::toMenu($temp, $v, $action);
            }

        return $temp;
    }

    public static function getMenu($id) {

        $p = self::model()->findAllByAttributes(array('parent' => $id));

        $temp = array();
        if ($p)
            foreach ($p as $k => $v) {
                $temp[] = array('name' => $v->title, 'url' => '/' . $v->url, 'id' => $v->parent);
            }

        return $temp;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {

        return array(
            'id' => 'ID',
            'parent' => Yii::t('admin', 'Parent ID'),
            'action' => Yii::t('admin', 'Template'),
            'title' => Yii::t('admin', 'Title'),
            'url' => Yii::t('admin', 'Url'),
            'text' => Yii::t('admin', 'Html'),
            'cdate' => Yii::t('admin', 'Antedate'),
            'udate' => Yii::t('admin', 'Modified'),
            'active' => Yii::t('admin', 'Visible'),
            'keywords' => Yii::t('admin', 'Keywords'),
            'description' => Yii::t('admin', 'Description'),
            'views' => Yii::t('admin', 'Views'),
            'position' => Yii::t('admin', 'Position'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = array('rSef');
        $criteria->compare('`t`.`parent`', $this->parent);
        $criteria->addSearchCondition('`t`.`title`', $this->title, true);
        $criteria->addSearchCondition('`t`.`url`', $this->url, true);
        $criteria->addSearchCondition('`t`.description', $this->description, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'countCriteria' =>
            array(
                'condition' => $criteria->condition,
                'params' => $criteria->params
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function createPath($id, $old) {
        $t = Pages::model()->findByPk($id);

        $p = false;
        if ($t) {
            $p = Pages::model()->findByPk($t->parent);
        }
        $r = explode('/', trim($t->url, '/'));
        $t->url = trim((!$p ? '' : $p->url) . '/' . end($r), '/');
        $t->setIsNewRecord(false);
        $t->save('url');

        if ($old && $old != $t->url) {
            $sql = 'INSERT INTO {{urls}} (url,target,item,active) VALUES("' . $old . '","pages",' . $id . ',1)';
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    public static function updateChildrenUrl($id, $old) {
        self::createPath($id, $old);
        $child = Pages::model()->findAllByAttributes(array('parent' => $id));
        if (!empty($child)) {
            foreach ($child as $v) {
                self::updateChildrenUrl($v->id, $t->url);
            }
        }
    }

    public function getUID() {
        $ac = Actions::model()->findByAttributes(array("name" => $this->action));
        if ($ac)
            return $ac->id;
        else
            return false;
    }

    public static function getMenuItems($id = 1) {
        return CHtml::listData(Pages::model()->findAllByAttributes(array("parent" => $id)), 'url', 'title');
    }

}
