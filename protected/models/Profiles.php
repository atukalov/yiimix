<?php

class Profiles extends XActiveRecord {

    /**
     * The followings are the available columns in table 'profiles':
     * @var integer $id
     */
    public $entity = "profile";

    public function behaviors() {
        return array('FieldsBehavior' => array('class' => 'application.components.FieldsBehavior'),);
    }

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{profiles}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('id', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('user', 'User ID'),
        );
    }

    public function getUID() {
        return false;
    }

}
