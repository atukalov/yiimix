<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AdminModule extends CWebModule {

    private $_assetsUrl;

    public function init() {

        Yii::setPathOfAlias('admin', './protected/modules/admin');

        $this->getAssetsUrl();

        Yii::app()->language = Language::admin();

        Yii::app()->setComponents(array(
            'user' => array(
                'class' => 'application.components.XWebUser',
                'allowAutoLogin' => true,
                'loginUrl' => '/admin/login',
            ),
            'ePdf' => array(
                'class' => 'ext.html2pdf.EYiiPdf',
                'params' => array(
                    'mpdf' => array(
                        'librarySourcePath' => 'application.vendors.mpdf60.*',
                        'constants' => array(
                            '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                        ),
                        'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder
                    /* 'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                      'mode'              => '', //  This parameter specifies the mode of the new document.
                      'format'            => 'A4', // format A4, A5, ...
                      'default_font_size' => 0, // Sets the default document font size in points (pt)
                      'default_font'      => '', // Sets the default font-family for the new document.
                      'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                      'mgr'               => 15, // margin_right
                      'mgt'               => 16, // margin_top
                      'mgb'               => 16, // margin_bottom
                      'mgh'               => 9, // margin_header
                      'mgf'               => 9, // margin_footer
                      'orientation'       => 'P', // landscape or portrait orientation
                      ) */
                    ),
                    'HTML2PDF' => array(
                        'librarySourcePath' => 'application.vendors.html2pdf.*',
                        'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                    /* 'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                      'orientation' => 'P', // landscape or portrait orientation
                      'format'      => 'A4', // format A4, A5, ...
                      'language'    => 'en', // language: fr, en, it ...
                      'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                      'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                      'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                      ) */
                    )
                ),
            ),
            //...
            'widgetFactory' => array(
                'widgets' => array(
                    'CGridView' => array(
                        'cssFile' => '',
                        'template' => '{items}{pager}',
                        'pager' => array(
                            'header' => '',
                            'firstPageLabel' => '&lt;&lt;',
                            'prevPageLabel' => '<',
                            'nextPageLabel' => '>',
                            'lastPageLabel' => '&gt;&gt;',
                            'cssFile' => '',
                        ),
                    ),
                ),
            ),
                )
        );
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
            'admin.models.*',
            'admin.widgets.*',
            'admin.widgets.shop.*',
        ));
    }

    public function isController($name) {

        $mod = Config::get('admin.modules');

        if (in_array($name, array_keys($mod)))
            return true;
        else
            return false;
    }

    public function beforeControllerAction($controller, $action) {

        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public function getAssetsUrl() {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                    Yii::getPathOfAlias('application.modules.admin.assets'), true, -1, false);
        return $this->_assetsUrl;
    }

    public function registerAssets() {

        $cs = Yii::app()->getClientScript();
        $au = $this->getAssetsUrl();
        $cs->registerScriptFile($au . "/forms.js");
    }

}
