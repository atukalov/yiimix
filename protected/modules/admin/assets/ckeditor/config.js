/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For the complete reference:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // The toolbar groups arrangement, optimized for two toolbar rows.

    config.menu_groups = 'clipboard,table,anchor,link,image';
    config.menu_subMenuDelay = 0;
    config.skin = 'bootstrapck';
    config.toolbarGroups = [
        {name: 'document', groups: ['mode']},
        {name: 'clipboard', groups: ['clipboard']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
        {name: 'links'},
        {name: 'insert'},
        {name: 'others'},
        {name: 'styles'},
        {name: 'colors'},
        {name: 'tools'},
    ];

    // Remove some buttons, provided by the standard plugins, which we don't
    // need to have in the Standard(s) toolbar.
    config.removeButtons = 'Cute,Paste,Copy';
    config.contentsLanguage = 'ru';
    config.language = 'ru';
    config.allowedContent = true;
    config.removePlugins="scayt";
    // Se the most common block elements.
    config.format_tags = 'p;h1;h2;h3;h4;h5;pre';
    config.format_p = {element: 'p', attributes: {'class': 'tit'}};

    // Make dialogs simpler.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    config.filebrowserBrowseUrl = '/admin/filelib' + gets;
    config.filebrowserImageBrowseUrl = '/admin/filelib' + gets;
    config.extraPlugins = 'lightbox,tliyoutube';


    //config.extraPlugins = 'colors';
};
