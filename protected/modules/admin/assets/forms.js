function ajaxFormSend(id, url) {
    $.ajax({
        type: "POST",
        path: url,
        data: formSerialize(id),
                processData: false,
                 async: false,
    cache: false,
    contentType: false,

        success: function (a) {
            notify("saved");
        },
        error: function (html) {
            notify(html);
        },
    });
}


function formSerialize(id) {
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
    

    var formData = new FormData(document.getElementById(id));
    console.log(formData)
    return formData;
}


function notify(text) {

    if ($(".notify").length == 0) {
        $("body").append('<div class="notify">' + text + '</div>');
    } else {
        $(".notify").stop().html(text).css("opacity", "0");
    }

    $(".notify").css("opacity", 0);
    $(".notify").animate({
        "opacity": 1,
    }, 500,
            function () {
                $(".notify").animate({
                    "opacity": 0,
                }, 1000);
            });
}
