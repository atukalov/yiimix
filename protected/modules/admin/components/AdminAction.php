<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AdminAction extends CAction {

    public $exportParams;

    public function url() {
        return 'admin/' . $this->controller->id . '/' . $this->extract($this->controller->action->id);
    }

    public function extract($id) {
        $e = explode('_', $id);
        return $e[0];
    }

    public function render($view, $param = array()) {


        if (Yii::app()->request->isAjaxRequest)
            $this->controller->renderPartial($view, $param);
        else
            $this->controller->render($view, $param);
    }

}

?>
