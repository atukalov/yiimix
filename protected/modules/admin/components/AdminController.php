<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */


Yii::import('application.modules.admin.controllers.*');

class AdminController extends CController {

    public $layout = '/layout/main';
    public $breadcrumbs = array();
    public $widgets = array("main" => array(0), "right" => array(), "menu" => array());

    public function actions() {
        $temp = array();

        $name = str_replace('Controller', '', get_class($this));
        $r = Config::get('admin.' . strtolower($name) . '.action');             
        
        if ($r) {
            foreach ($r as $k => $v) {
                if ($k == "action")
                    $temp["index"] = array("class"=>$v[0]);
                else
                    $temp[$k] = array("class"=>$v[0]);
            }
        }
        return $temp;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array_keys($this->actions()),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isAdmin()==true',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

}
