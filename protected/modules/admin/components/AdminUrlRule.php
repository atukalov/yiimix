<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AdminUrlRule extends CBaseUrlRule {

    public function createUrl($manager, $route, $params, $ampersand) {

        $p = explode('/', $route);
        if ($p[0] == 'admin') {
            //перебираем GET
            $get = array();
            foreach ($params as $k => $v)
                if ($k != 'id')
                    if (is_array($v)) {
                        foreach ($v as $k1 => $v1) {
                            $get[] = $k . '[' . $k1 . ']' . '=' . $v1;
                        }
                    } else
                        $get[] = $k . '=' . $v;

            $gets = '?' . join($ampersand, $get);
            if ($gets == "?")
                $gets = "";
            if (isset($params['id']))
                return $route . '/' . $params['id'] . $gets;
            else
                return $route . $gets;
        } else
            return false;
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
      
        $p = explode("/", $pathInfo);

        if ($p[0] == 'admin' && count($p) == 1)
            return 'admin/default/index';
        else if ($p[0] == 'admin' && $p[1] == 'save')
            return 'admin/default/save';
        else if ($p[0] == 'admin' && $p[1] == 'login')
            return 'admin/default/login';
        else if ($p[0] == 'admin' && count($p) == 2) {
            return 'admin/' . $p[1] . '/index';
        } else if ($p[0] == 'admin' && count($p) > 2) {
            array_shift($p);
            $cn1 = $cn = array_shift($p);

            $admin = Yii::app()->getModule('admin');

            if ($admin->isController($cn)) {
                $cn.='Controller';
                $cn = ucfirst($cn);
                //Проверяем есть файл контроллера


                Yii::import('application.modules.admin.controllers.' . $cn);
                $controller = new $cn(null);
                $actions = array_keys($controller->actions());


                if (count($p) > 1) {
                    $ta = $p[0] . '_' . $p[1];

                    if (in_array($ta, $actions)) {
                        array_shift($p);
                        array_shift($p);
                        if (!empty($p))
                            $_GET['id'] = $p[0];
                        return 'admin/' . $cn1 . '/' . $ta;
                    }else {

                        $ta = array_shift($p);

                        if (in_array($ta, $actions)) {
                            $_GET['id'] = $p[0];
                            return 'admin/' . $cn1 . '/' . $ta;
                        } else {
                            $_GET['id'] = $ta;
                            return 'admin/' . $cn1 . '/index';
                        }
                    }
                } else {
                    $ta = $p[0];
                    if (in_array($ta, $actions)) {
                        return 'admin/' . $cn1 . '/' . $ta;
                    } else {
                        $_GET['id'] = $ta;
                        return 'admin/' . $cn1 . '/index';
                    }
                }

                if (in_array($ta, $actions))
                // var_dump($ta);
                    die;
            } else {
                return false;
            }


            //var_dump($a);
            //die;
        }
        if (substr($pathInfo, 0, 5) == 'admin') {
            $path = explode('/', $pathInfo);
            if (count($path) == 1)
                return 'admin/default/index';
            else if (count($path) == 2) {
                return 'admin/' . $path[1] . '/index';
            } else if (count($path) > 2) {
                //$c=Yii::app()->getController('admin.'.$path[1].'Controller');
                $l = Yii::import('application.modules.admin.controllers.' . $path[1] . 'Controller');
                $b = call_user_func($path[1] . 'Controller' . '::getControllers', array());


                if ($b == '')
                    var_dump($b);
                $c = $path[1] . 'Controller';

                if (in_array($path[1], $c->getControllers()))
                    return 'admin/' . $path[2] . '/';
                else
                    return 'admin/' . $path[1] . '/' . $path[2];
            }
        } else
            return false;

        //parent::parseUrl($manager, $request, $pathInfo, $rawPathInfo);
    }

}

?>
