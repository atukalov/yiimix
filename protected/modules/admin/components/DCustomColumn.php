<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */


Yii::import('zii.widgets.grid.CDataColumn');

class DCustomColumn extends XDataColumn
{

    public $data;
    public $datas = array();
    public $rf;

    /**
     * Initializes the column.
     */
    public function init()
    {
        parent::init();
        $this->registerScript();
        $this->htmlOptions = array_merge($this->htmlOptions, array('class' => 'de'));
    }

   

    protected function renderDataCellContent($row, $data)
    {
        $this->grid->controller->renderPartial($this->rf, array_merge(array("data" => $data), $this->datas));
    }

    protected function getItemValue($row, $data)
    {
        if(!empty($this->value))
            return $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
        elseif(!empty($this->name))
            return CHtml::value($data, $this->name);
        return null;
    }

    public function registerScript()
    {
        Yii::app()->clientScript->registerScript('DCustomColumn', '
           ', CClientScript::POS_HEAD);
    }
    

}
