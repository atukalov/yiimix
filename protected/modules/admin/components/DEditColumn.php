<?php
Yii::import('zii.widgets.grid.CDataColumn');

class DEditColumn extends CDataColumn {

    public $link;
    public $item_id;

    /**
     * Initializes the column.
     */
    public function init() {
        parent::init();
        $this->registerScript();
        $this->htmlOptions=  array_merge($this->htmlOptions,array('class'=>'de'));
    }

    /**
     * Renders the header cell content.
     * This method will render a link that can trigger the sorting if the column is sortable.
     */
    protected function renderHeaderCellContent() {
        if ($this->grid->enableSorting && $this->sortable && $this->name !== null)
            echo '<button onclick="start_edit(\'' . $this->name . '\')" id="dec-' . $this->name . '-control">Edit</button> ' . $this->grid->dataProvider->getSort()->link($this->name, $this->header, array('class' => 'sort-link'));
        elseif ($this->name !== null && $this->header === null) {
            if ($this->grid->dataProvider instanceof CActiveDataProvider)
                echo CHtml::encode($this->grid->dataProvider->model->getAttributeLabel($this->name)) . 'dsf';
            else
                echo CHtml::encode($this->name) . 'dsf';
        } else
            parent::renderHeaderCellContent();
    }

    protected function renderDataCellContent($row, $data) {


        $value = $this->getItemValue($row, $data);
        $text = $this->grid->getFormatter()->format($value, $this->type);
        $item_id = $this->evaluateExpression($this->item_id, array('data' => $data, 'row' => $row));

        if ($value === null) {
            echo $this->grid->nullDisplay;
        } else {
            if(is_numeric($text))
            $cl='tright ';
            else
                $cl='';
            ?><span class="span-<?= $this->name ?>-edit-grid" data-index="<?=$item_id?>"><?= $text ?></span>
            <input name="DEC[<?= $this->name ?>][<?= $item_id ?>]" value="<?= $text ?>"  class="<?=$cl?>dec input-<?= $this->name ?>-edit-grid" style="display:none"><?php
        }
    }

    protected function getItemValue($row, $data) {
        if (!empty($this->value))
            return $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
        elseif (!empty($this->name))
            return CHtml::value($data, $this->name);
        return null;
    }

    public function registerScript() {

        Yii::app()->clientScript->registerScript('DEditColumn', '
function start_edit(name){
$(".span-"+name+"-edit-grid").hide();
$(".input-"+name+"-edit-grid").attr("tabindex",1).show();
$(".input-"+name+"-edit-grid:first").focus();
$("#dec-"+name+"-control").html("Save").attr("onclick","end_edit(\'"+name+"\')");
}
function end_edit(name){
var data=$(".input-"+name+"-edit-grid").serialize();
$.ajax({
type: "post",
url: "' . $this->link . '",
data: "id="+name+"&"+data,
}).done(function(data){
$.fn.yiiGridView.update("'.$this->grid->id.'");
console.log(data)
});


}

           ', CClientScript::POS_HEAD);
    }

}

/*var temp=[];
$(".input-edit-grid").each(function(){
var k=$(this).attr("name");
temp[k]=$(this).val();
});*/