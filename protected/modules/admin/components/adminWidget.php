<?php

class adminWidget extends CWidget
{

    public $title = "adminWidget";
    public $icon = "fa fa-cog";
    public $modal = FALSE;
    public $minimize = FALSE;
    public $fullscreen = FALSE;

    public function buttons()
    {
        
    }

    public function run()
    {
        if($this->modal === TRUE)
        {
            ?>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><?= $this->title ?></h3>
                    </div>
                    <div class="modal-body">
                        <?= $this->content(); ?>
                    </div>
                    <div class="modal-footer">
                        <?php $this->buttons(); ?>                       
                    </div>
                </div>
            </div>

            <?php
        }
        else
        {
            ?>
            <div class="block-title<?= $this->minimize ? " canMinimize" : "" ?>">
                <h2><i class="<?= $this->icon ?> text-warning"></i>&nbsp;&nbsp;<?= $this->title ?></h2>
                <div class="block-options pull-right">
                    <?php $this->buttons(); ?>
                    <?php if($this->minimize): ?>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
                    <?php endif; ?>
                    <?php if($this->fullscreen): ?>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="block-toggle-fullscreen"><i class="fa fa-desktop"></i></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="block-content" style="display: block;">
                <?php
                echo $this->content();
                ?></div><?php
        }
    }

    public function asMenuLink($id = false)
    {
        return "<a href=\"/admin/default/widget".($id ? "/".$id : "")."\" data-remote=\"?target=".$this->target."&item_id=".$this->item_id."\" data-toggle=\"ajaxModal\"><i class=\"".$this->icon." pull-right\"></i> ".$this->title."</a>";
    }

    public function asLink()
    {
        
    }

}
