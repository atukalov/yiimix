<?php
Yii::import('zii.widgets.grid.CDataColumn');

class boolColumn extends CDataColumn {

	public $link;
	public $item_id;

	/**
	 * Initializes the column.
	 */
	public function init() {
		parent::init();
		$this->registerScript();
		$this->htmlOptions = array_merge($this->htmlOptions, array('class' => 'de'));
	}

	protected function renderDataCellContent($row, $data) {

		$value = $this->getItemValue($row, $data);

		if($value === null) {
			echo $this->grid->nullDisplay;
		}
		else {
			if($value == 1)
				$cl = 'enabled';
			else
				$cl = 'desibled';
			?><p class="boolValue <?= $cl ?>"></p><?php
		}
	}

	protected function getItemValue($row, $data) {
		if(!empty($this->value))
			return $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
		elseif(!empty($this->name))
			return CHtml::value($data, $this->name);
		return null;
	}

	public function registerScript() {
		$package = array(
			'basePath' => 'application.modules.admin.components.assets',
			'css' => array('columns.css'),
			//'js' => array('jquery.js', 'functions.js')
		);
		Yii::app()->clientScript
				->addPackage('yiimix-admin', $package)
				->registerPackage('yiimix-admin');
		
		$assetFolder = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets');
		
		Yii::app()->clientScript->registerCssFile($assetFolder.'/columns.css');
	//Yii::app()->clientScript->registerCssFile('./protected/modules/admin/components/assets/columns.css');
	
	}

}
