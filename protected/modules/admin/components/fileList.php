<?php

class fileList {

    static private $extI = array('png', 'jpg', 'gif');
    static private $extD = array('doc', 'pdf');
    static private $extA = array('zip', 'rar');
    static private $extS = array('wav', 'mp3', 'ogg');
    static public $path;
    static public $root;
    static private $files;
    static public $items = array();
    static private $handle;

    public static function init($path, $apath) {

        self::$path = rtrim(rtrim($path, '/') . '/' . $apath,'/');
        self::$root = rtrim($path, '/');

        if (!file_exists(self::$path))
            self::$path = self::$root;
        
        self::$files = scandir(self::$path);
        sort(self::$files);
        
    }

    public static function read() {
        $path = trim(self::$path, '.');
        foreach(self::$files as $file){
            if (self::$root != self::$path && $file == '..') {
                self::dir($file);
            }
            if (is_dir(self::$path . '/' . $file) && $file != '.' && $file != '..') {
                self::dir($file);
            } else if ($file != '.' && $file != '..') {
                self::file($file);
            }
        }
        return self::$items;
    }

    private static function dir($file) {
        
        if ($file == '..') {
            $a = explode('/', rtrim(self::$path, '/'));
            unset($a[count($a) - 1]);
            $url = join('/', $a);
            $url = str_replace(self::$root, '', $url);
            if ($url == '')
                $url = '';
            else
                $url = '?dir=' . $url;

            //var_dump($url);die;
        }else {
            $url = '?dir=' . $file;
        }
        self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-folder-open"></span>', 'url' => '/admin/pages/files' . $url);
    }

    private static function file($file) {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if (in_array($ext, self::$extI))
            self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-picture"></span>', 'url' => '');
        else if (in_array($ext, self::$extD))
            self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-file"></span>', 'url' => '');
        else if (in_array($ext, self::$extA))
            self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-compressed"></span>', 'url' => '');
        else if (in_array($ext, self::$extS))
            self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-music"></span>', 'url' => '');
        else
            self::$items[] = array('name' => $file, 'ico' => '<span class="halflings halflings-warning-sign"></span>', 'url' => '');
    }
    public static function getPath(){return self::$path;}

}

?>
