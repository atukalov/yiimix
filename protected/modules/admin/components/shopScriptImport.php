<?php

class shopScriptImport {

    static public function import($file, $roadmap, $mode) {

        $roadmap = unserialize(file_get_contents('./upload/import/' . $roadmap));
        ini_set('auto_detect_line_endings', TRUE);
        set_time_limit(0);
        if (($handle = fopen("./upload/import/" . $file, "r")) !== FALSE) {
            $header = fgetcsv($handle, 0, ";");

            $cats = array();
            $current_level = 0;

            while (($data = fgetcsv($handle, 0, ";") ) !== FALSE) {
                //Проверяем на категорию
                if ($data[4] == ''){
                    if (substr($data[2], 0, 1) == '!') {
                        // нашел категорию
                        // 
                        //проверяем уровень
                        $level = strlen($data[2]) - strlen(ltrim($data[2], '!'));
                        switch ($level) {
                            case 1:
                                $cats = array();
                                $current_level = 1;

                                $sql = 'INSERT INTO shop_cats (title, url) VALUE ("' . ltrim($data[2], '!') . '","' . $data[3] . '")';
                                Yii::app()->db->createCommand($sql)->execute();
                                $cats[$current_level] = Yii::app()->db->getLastInsertID();

                                break;
                            default:
                                $current_level = $level;

                                Yii::app()->db->createCommand()->insert('shop_cats', array('parent' => $cats[($current_level - 1)], 'title' => ltrim($data[2], '!'), 'url' => $data[3]));
                                $cats[$current_level] = Yii::app()->db->getLastInsertID();

                                break;
                        }
                        //var_dump($level);
                    }
                }else{
                    //Опознали как Товар
                    
                    //Разбираем roadmap, что бы создать массив с данными для insert, отсекаем ненужное
                    
                    $tRoadmap=array();
                    $tRoadmap['cat']=$cats[$current_level];
                    
                    foreach ($roadmap as $k=>$v){
                        if($v!='null')
                            $tRoadmap[$k]=$data[$v];
                        
                    }
                    
                    Yii::app()->db->createCommand()->insert('shop_items', $tRoadmap);
                    unset($tRoadmap);    
                }
            }
        }
        fclose($handle);
        ini_set('auto_detect_line_endings', FALSE);
    }

}

