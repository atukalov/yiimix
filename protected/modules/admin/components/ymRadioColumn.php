<?php

class ymRadioColumn extends CDataColumn {

    public $values = array();
    public $retVar = '';
    public $gSelect = '';
    public $htmlOptions = array();

    public function init() {
        parent::init();
        // $this->filter = $this->values;
        $this->registerClientScript();
    }

    protected function renderDataCellContent($row, $data) {
        $options = $this->htmlOptions;
        //$options['name'] = $this->name . '[' . $data->primaryKey . ']';
        $v = $this->name;
        echo '<a class="button" onclick="selectBut(' . $data->primaryKey . ',\'' . $data->$v . '\');">Выбрать</a>';
        //echo CHtml::activeRadioButtonList( $data, $this->name, $this->values, $options );        
    }

    /**
     * Registers the client scripts for the button column.
     */
    protected function registerClientScript() {

        $js = '
function selectBut(id, text){
var beg=' . $this->gSelect . ';   
jQuery("#pointer-' . $this->retVar . '-"+beg).val(id);
jQuery("#pointertitle-' . $this->retVar . '-"+beg).html(text);
jQuery("#dialog").dialog("close");

}                    
';

        Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->id, $js, CClientScript::POS_BEGIN);
    }

}
