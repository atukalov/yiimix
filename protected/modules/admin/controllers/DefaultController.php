<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DefaultController extends Controller {

    public $layout = '/layout/main';
    public $header;
    public $widgets = array("main" => array(0), "right" => array(), "menu" => array());

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('login'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', "widget", 'dropcache'),
                'users' => array("admin"), //Yii::app()->getModule('user')->getAdmins(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
//var_dump(Yii::app()->user->id);
        //      var_dump(Yii::app()->getModule('user')->getAdmins());
        $this->render('index');
    }

    public function actionWidget($id) {

        $this->exportParams = array();

        if (isset($_GET["entity"]) && isset($_GET["item_id"])) {
            $s = ucwords($_GET["entity"]);
            $s = new $s();
            $obj = $s::model()->findByPk(intval($_GET["item_id"]));
            if ($obj)
                $this->exportParams = $obj;
        }

        $w = FALSE;
        if (is_numeric($id))
            $w = Params::model()->findByPk(intval($id));

        if ($w) {

            $this->widget($w->value, array("modal" => TRUE));
        }
    }

    public function actionSave() {
        
    }

    public function getMenu() {
        return array();
    }

    public function actionDropcache($url = '') {
        Yii::app()->cache->flush();
        if ($url != '') {
            $this->redirect($url);
        }
    }

    public function actionLogin() {
        $this->layout = "/layout/empty";

        if (Yii::app()->user->isGuest) {
            $model = new UserLogin;
            // collect user input data
            if (isset($_POST['UserLogin'])) {
                $model->attributes = $_POST['UserLogin'];
                // validate user input and redirect to previous page if valid
                if ($model->validate()) {
                    $this->lastViset();
                    $this->redirect("/admin");
                }
            }
            // display the login form
            $this->render('login', array('model' => $model));
        } else
            $this->redirect("/admin");
    }

    private function lastViset() {
        $lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
        $lastVisit->lastvisit = time();
        $lastVisit->save();
    }

}
