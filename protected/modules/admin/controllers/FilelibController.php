<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix File Lib Controller
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/manual/filelib
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

Yii::import('application.modules.admin.controllers.*');

function isPngApha($file) {
//$size = getimagesize($file, $info);
//var_dump($info);
//var_dump(exif_imagetype($file));
//var_dump(exif_read_data($file));

    if (exif_imagetype($file) !== 3)
        return false;

    $l = file_get_contents($file);
    //$s = substr($l, 9, 18);
    //unset($l);
    // 6 - RGB + ALPHA
    // 4 - Grayscale + ALPHA
    if (ord($l[25]) == 6 || ord($l[25]) == 4)
        return true;

    return false;
}

class FilelibController extends AdminController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actions() {
        return array(
            'index',
            'update' => false,
            'upload',
            'delete' => false,
            'main' => false,
            'thumb',
            'quality',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('quality', 'thumb', 'index', 'update', 'upload', 'delete', 'main'),
                'users' => Yii::app()->user->getAdmins(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionMain($id = false) {

        if (!$id)
            exit();

        $model = Filelib::model()->findByPk(intval($id));

        if ($model) {
            $pr = $model->primary == 1 ? 0 : 1;
            Yii::app()->db->createCommand("UPDATE {{filelib}} SET `primary`=0 WHERE `target`='pages' and `item`=" . $model->item)->execute();
            Yii::app()->db->createCommand("UPDATE {{filelib}} SET `primary`=" . $pr . " WHERE `id`=" . $model->id)->execute();
        }
    }

    public function actionDelete($id = false) {

        if (!$id)
            exit();

        $model = Filelib::model()->findByPk(intval($id));

        if ($model) {
            $path = "./uploads/" . $model->target . "/" . $model->item . "/";

            if (file_exists($path . $model->filename))
                unlink($path . $model->filename);

            $model->delete();
        }
    }

    public function actionUpdate() {
        
    }

    public function actionQuality($id) {
        $this->layout = "//layouts/empty";
        $l = Filelib::model()->findByPk($_GET["id"]);

        $path = "/uploads/" . $l->target . "/" . $l->id;

        if (!file_exists(realpath("." . $path) . '/temp'))
            mkdir(realpath("." . $path) . '/temp', 0777);

        if ($l->type == "jpg") {
            $image = imagecreatefromjpeg("." . $path . "/" . $l->filename);
        }

        $s = array("jpg" => array(100, 90, 80, 70, 60, 50), "png" => array(0, 4, 5, 6, 7, 8, 9), "gif" => array(1));
        if ($image) {
            foreach ($s as $k => $v) {
                foreach ($v as $b) {
                    if ($k == "jpg") {
                        imagejpeg($image, "." . $path . "/temp/" . "test" . $b . ".jpg", $b);
                    }
                    if ($k == "png") {
                        imagepng($image, "." . $path . "/temp/" . "test" . $b . ".png", $b);
                    }
                    if ($k == "gif") {
                        imagegif($image, "." . $path . "/temp/" . "test.gif");
                    }
                }
            }
        }
        imagedestroy($image);
    }

    public function actionThumb() {
        $this->layout = "//layouts/empty";
        $l = Filelib::model()->findByPk(intval($_GET["id"]));


        $sizes = Config::get("filelib.thumbs.size");
        if (empty($sizes))
            throw new CHttpException(508, "Не задан размер мимниатюр");


        $cs = Yii::app()->clientScript;
        $cs->registerScript("filelibThumb", "var imgSource='" . $l->imageSrc() . "'", CClientScript::POS_BEGIN);
        $cs->registerCoreScript("jquery");


        $assetUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.views.filelib.assets'));


        $cs->registerScriptFile($assetUrl . '/script.js', CClientScript::POS_END);
        $cs->registerCssFile($assetUrl . '/style.css');
        $cs->registerScriptFile($assetUrl . "/cropit.js");
        $this->render("thumb", array(
            "sizes" => $sizes,
        ));
    }

    public function actionIndex() {
        $this->layout = "//layouts/empty";

        if (!isset($_GET["e"]) || !isset($_GET["id"]))
            throw new CHttpException("404", "Page not found");


        $entity = CHtml::encode($_GET["e"]);
        $id = intval($_GET["id"]);


        $res = Filelib::model()->findAllByAttributes(array('target' => $entity, 'item' => $id));
        // var_dump($res);
        $this->render('index', array("images" => $res));
    }

    public function actionUpload() {

//
        if (isset($_FILES['files']) && !empty($_FILES)) {
            $images = $_FILES['files'];
            $icount = count($images['name']);

            if (!empty($images)) {

                $entity = $_POST['Filelib']['entity'];
                $item = $_POST['Filelib']['item'];

                //if (isset($_POST['Filelib'])) {
                //  $model = new Filelib();
                //$model->attributes = $_POST['Filelib'];


                $path = "./uploads";
                $path = realpath($path);
                $path .= "/" . $entity;

                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                $path .= "/" . $item;
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }


                $count = intval(Yii::app()->db->createCommand("SELECT count(*) FROM {{filelib}} WHERE target='{$entity}' and item={$item}")->queryScalar());
                $count++;
                for ($i = 0; $i < $icount; $i++) {
                    $tn = $images['tmp_name'][$i];
                    $n = $images['name'][$i];

                    $r = rename($tn, $path . '/' . $n);
                    if ($r) {
                        $model = new Filelib;

                        $model->target = $entity;
                        $model->item = $item;
                        $model->filename = $n;
                        $model->pos = $count;

                        if (!$model->save())
                            var_dump($model->getErrors());
                    }
                }
            }
        }
    }

    public function getMenu() {
        return array();
    }

}
