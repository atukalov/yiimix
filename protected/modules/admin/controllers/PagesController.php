<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class PagesController extends AdminController {
    
    public function getMenu() {
        return
                array(
                    
                    '<i class="fa fa-bars"></i> Меню' => array('url' => '/admin/pages/menu', 'class' => 'menu-ico'),
                    '<i class="fa fa-file-text-o"></i> Статьи' => array('url' => '/admin/pages/articles', 'class' => 'art-ico'),
                    '<i class="fa fa-puzzle-piece"></i> Списки' => array('url' => '/admin/pages/lists', 'class' => 'lists-ico'),
                    '<i class="fa fa-usd"></i> Прайсы' => array('url' => '/admin/pages/price', 'class' => 'price-ico'),
                    'sep',
                    '<i class="fa fa-exchange"></i> Импорт' => array('url' => '/admin/pages/import', 'class' => 'import-ico'),
                    'sep',
                    '<i class="fa fa-university"></i> Шаблоны' => array('url' => '/admin/pages/logic', 'class' => 'logic-ico'),
                    '<i class="fa fa-wrench"></i> Поля' => array('url' => '/admin/pages/fields', 'class' => 'field-ico'),
                    'sep',
                    '<i class="fa fa-line-chart"></i> SEO' => array('url' => '/admin/pages/seo', 'class' => 'seo-ico'),
                   
        );
    }

}

?>
