<?php

/*
 * Yiimix Profile Controller
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

Yii::import('application.modules.admin.controllers.*');

class ProfileController extends AdminController {

    public function getMenu() {
        return
                array(                   
                   
                    'Mailbox' => array('url' => '/admin/profile/mail', 'class' => 'letter-ico'),
                    'Planner' => array('url' => '/admin/profile/planner', 'class' => 'cal-ico'),
                    'Target' => array('url' => '/admin/compass/templates', 'class' => 'target-ico'),
                    'sep',
                    'Config' => array('url' => '/admin/pages/price', 'class' => 'pref-ico'),
                    
                   
        );
    }

}
    