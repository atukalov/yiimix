<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class ServiceController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array_keys($this->actions()),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isAdmin()==true',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function parentController() {
        return false;
    }

    public static function getControllers() {
        return array('dostavka');
    }

    public function getMenu() {
        return
                array(
                    'Файлы' => array('url' => '/admin/service/files', 'class' => 'menu-ico'),
                    'sep',
                    //'Обновления' => array('url' => '/admin/service/updater', 'class' => 'update-ico'),
                    'Сбросить кэш' => array('url' => '/admin/service', 'class' => 'cache-ico'),
                    'Настройки' => array('url' => '/admin/service', 'class' => 'pref-ico'),
                    'База' => array('url' => '/admin/service', 'class' => 'database-ico'),
                    'sep',
                    'Обновления' => array('url' => '/admin/service/updater  ', 'class' => 'update-ico'),
        );
    }

}

?>
