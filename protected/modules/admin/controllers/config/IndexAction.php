<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.blog
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/blog
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run() {

        $cond = 'class="blog"';

        $maskedAction = array('index', 'install', 'shop');
        $res = Config::get("admin.config.action");
        $ac = array_keys($res);

        $temp = array();
        foreach ($ac as $v) {
            if (!in_array($v, $maskedAction))
                $temp[] = $v;
        }

        $res = Yii::app()->cache->get('admin.config.modules.'.Language::$current);

        if (!$res) {
            $urls = array();
            // var_dump(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'));die;
            if ($handle = opendir(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'))) {
                Yii::import('application.modules.admin.controllers.config.modules.*');
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && $entry != '.DS_Store') {
                        $c = explode('.', $entry);
                        $c = $c[0];
                        $l = strtolower(str_replace('Action', '', $c));                     
                        $r = new $c($this->controller, $l);
                        $urls[] = array('id' => count($urls), 'title' => Yii::t('admin', $r->title), 'url' => 'admin/config/'.$l);
                    }
                }
                closedir($handle);
            }
            Yii::app()->cache->set('admin.config.modules.'.Language::$current, $urls);
        } else {
            $urls = $res;
        }
        //var_dump($urls);
        //die;

        $this->render('index', array(
            'catalog' => $temp,
            'cat' => new CArrayDataProvider($urls),
        ));
    }

}

?>
