<?php

/*
 * Yiimix Online Shop
 *
 * @category   YimMix
 * @package    yiimix.blog
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/blog
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class ShopAction extends AdminAction {

    public function run() {
        Yii::import('application.modules.Shop.models.Modules');
        
        $client = new SoapClient('http://loc.yiimix.ru/index.php?r=Shop/default/service2');
        //var_dump($client);
        //$funcs = $client->__getFunctions();
       // var_dump($funcs);

        $temp = array();
        try {
            $temp = $client->getM(1);
            $cat=array();
            foreach ($temp as $v)
                $cat[]=$v->_attributes;
            
        } catch (SoapFault $e) {
            echo $client->__getLastResponse();
        }

  

        $this->render('shop', array(
            'catalog' => $cat,
        ));
    }

}

?>
