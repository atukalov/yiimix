<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.blog
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/blog
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class BlogConfigModel extends CFormModel
{

    public $layout; //Шаблон отображения
    public $canUserCreate; //Возможно ли пользователям писать в блог
    public $commentUserRole; //Роль пользователя на которой разрешены комментарии
    public $moderatePost; //Модерировать блог
    public $moderateComment; //Модерировать коментарии

    public function rules()
    {
        return array(
            array('layout, canUserCreate, commentUserRole, moderatePost, moderateComment', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'layout' => 'Шаблон отображения',
            'canUserCreate' => 'Разрешить пользователям размещать материалы',
            'commentUserRole' => 'Группа пользователей для которой разрешены комментарии',
            'moderatePost' => 'Премодерация текстов',
            'moderateComment' => 'Премодерация коментариев',
        );
    }

    public function getData()
    {

        $res = Config::get("config.blog");

        if(!$res)
        {
            $this->layout = "main";
            $this->canUserCreate = true;
            $this->commentUserRole = "registered";
            $this->moderatePost = true;
            $this->moderateComment = true;
        }
        else
        {
            foreach($res as $k => $v)
            {
                $this->$k = $v;
            }
        }
    }

}
