<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.blog
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/blog
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run() {

        Yii::import("admin.controllers.config.blog.*");
        $model = new BlogConfigModel();
        $model->getData();

        //Получаем layouts
        $layoutsPath = Yii::getPathOfAlias("application.views.layouts");
        $files = array();
        $dh = opendir($layoutsPath);
        while (false !== ($filename = readdir($dh))) {
            $path_parts = pathinfo($filename);
            if ($path_parts['extension'] == "php")
                $files[$path_parts['filename']] = $path_parts['filename'];
        }
        
       $res=Yii::app()->authManager->getAuthItems(2);
       
       //var_dump($res["user"]->description);die;
       $roles=array();
        foreach($res as $k=>$v){
            $roles[$k]=$v->description;
            
        }
        

        $this->render('blog/index', array(
            'model' => $model,
            'layouts'=>$files,
            'roles'=>$roles,
        ));
    }

}

?>
