<?php

/*
 * Yiimix installer action
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/blog
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction
{

    public function run()
    {

        if(isset($_GET["a"]))
        {
            echo Yii::app()->session['progress'];
            exit();
        }
        else
        {
            if(isset($_GET["i"]))
            {
                echo "install";
                $l = Yii::import("application.extensions.refspam.Install");
                $m = new $l();
                $m->cmd("install");
            }

            $this->render('install/index', array(
            ));
        }
    }

}

?>
