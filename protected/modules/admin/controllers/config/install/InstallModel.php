<?php

/*
 * Yiimix Install Model
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class InstallModel extends CFormModel
{

    public $filename;

    public function rules()
    {
        return array(
            array('filename', 'required'),
            array('filename', 'file', 'types' => 'zip, gz', 'safe' => false),
        );
    }

    public function install()
    {        
         $model->image=CUploadedFile::getInstance($model,'filename');
         $model->image->saveAs('path/to/localFile');
        
    }
    
    
}
