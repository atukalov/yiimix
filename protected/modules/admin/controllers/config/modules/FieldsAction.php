<?php

class FieldsAction extends AdminAction {

    public $title = 'Custom Fields';

    public function run() {

        $res = Yii::app()->cache->get('admin.config.modules.'.Language::$current);

        if (!$res) {
            $urls = array();
            // var_dump(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'));die;
            if ($handle = opendir(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'))) {
                Yii::import('application.modules.admin.controllers.config.modules.*');
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && $entry != '.DS_Store') {
                        $c = explode('.', $entry);
                        $c = $c[0];
                        $l = strtolower(str_replace('Action', '', $c));                     
                        $r = new $c($this->controller, $l);
                        $urls[] = array('id' => count($urls), 'title' => Yii::t('admin', $r->title), 'url' => 'admin/config/'.$l);
                    }
                }
                closedir($handle);
            }
            Yii::app()->cache->set('admin.config.modules.'.Language::$current, $urls);
        } else {
            $urls = $res;
        }
        
        
        $entity = Config::get('fields.entity');
        $en=array();
        if(!empty($entity)){
            foreach ($entity as $k => $v) {
                $en[]=array('id'=>  count($en),'title'=>$v[0],'name'=>$k);                
            }
            
        }
        //var_dump($entity);

        $this->render('modules/fields',array(
            'entity'=>new CArrayDataProvider($en),
            'modules'=>$urls
            
        ));
    }

}
