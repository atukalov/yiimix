<?php

class FilelibAction extends AdminAction {

    public $title = 'Filelib';

    public function run() {

        $paths = Config::get('filelib.paths');
        $qu = Config::get('filelib.quality');
        $ths = Config::get('filelib.thumbs');
          $max = Config::get('filelib.max');
       


        $this->render('modules/filelib',array(
            'paths'=>$paths,
            'qu'=>$qu,
            'ths'=>$ths,
            'max'=>$max,
           
           
        ));
    }

}
