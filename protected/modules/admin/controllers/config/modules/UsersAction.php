<?php

class UsersAction extends AdminAction{
    
    public $title='Users';

    public function run() {
           $this->render('modules/users');
    }
    
}