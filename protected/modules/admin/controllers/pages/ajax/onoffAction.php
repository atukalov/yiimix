<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class onoffAction extends AdminAction {

    public function run($id) {
        
        $model=Pages::model()->findByPk(intval($id));
        $model->isNewRecord=false;
        if($model){
            if($model->active==0)
                $model->active=1;
            else
                $model->active=0;
            
            $model->save("active");                
            
        }
        
    }

}

?>
