<?php
/*
 * Lists Update Action
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AjaxAction extends AdminAction {

    public function run() {

        if (Yii::app()->request->isAjaxRequest && isset($_POST['action'])) {

            $ac = Actions::model()->findByAttributes(array('name' => $_POST['action']));
            if (isset($_POST['num']) && $_POST['num'] == -1)
                $model = new Pages();
            else
                $model = Pages::model()->findByPk(intval($_POST['num']));

            $model->action = $_POST['action'];
            $form = new CActiveForm(null, $model, null);
            if ($ac) {
                $ids = ActionFields::model()->findAllByAttributes(array('target' => 'pages', 'action' => $ac->id));

                $t = array();
                if (!empty($ids))
                    foreach ($ids as $v) {
                        $t[] = $v->field;
                    }

                $f = Fields::model()->findAllByPk($t);

                if (!empty($f)) {

                    foreach ($f as $field) 
                        echo $field->input($form, $model);
                           
                        
                       
                }
            }
        }
    }

}
