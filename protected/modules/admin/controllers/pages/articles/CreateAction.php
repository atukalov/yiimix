<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

function translit($s) {
    mb_internal_encoding('utf8');
    $s = (string) $s; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace("/\s+/u", ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ ]/iu", "", $s); // очищаем строку от недопустимых символов
    $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
    return $s; // возвращаем результат
}

class CreateAction extends AdminAction {

    public function run($id = false) {

        $model = new Pages();
        if ($id)
            $model->parent = intval($id);

        $r = Yii::app()->db->createCommand("SHOW TABLE STATUS WHERE name='{{pages}}'")->queryRow();
        $model->id = intval($r["Auto_increment"]);
        if (isset($_GET["action"]) && intval($_GET["action"]) > 0) {
            $ac = Actions::model()->findByPk(intval($_GET["action"]));
            if ($ac)
                $model->action = $ac->name;
            else
                throw new CHttpExection(404, "Page not found");

            $model->cdate = new CDbExpression('NOW()');
            $model->udate = new CDbExpression('NOW()');
            $model->isNewRecord=true;
            if ($model->save()) {
                $this->controller->redirect('/admin/pages/articles/update/' . $model->id);
            } else {
                print_r($model->getErrors());die;
            }
        }

        if (isset($_POST['Pages'])) {

            $model->attributes = $_POST['Pages'];
            $model->cdate = new CDbExpression('NOW()');
            $model->udate = new CDbExpression('NOW()');
            if ($model->url == "")
                $model->url = translit($model->title);

            if ($model->save()) {
                Pages::createPath($model->id);
                Pages::updateChildrenUrl($model->id);
                $p = $model->parent == 0 ? '' : $model->parent;
                $this->controller->redirect('/admin/pages/articles/' . $p);
            }
        }
        Yii::app()->clientScript->registerScriptFile("/js/bootstrap-tooltip.js");
        Yii::app()->clientScript->registerScriptFile("/js/bootstrap-confirmation.js");

        Yii::app()->clientScript->registerScriptFile("/js/files.js");

        $this->controller->render('articles/create', array('model' => $model));
    }

}
