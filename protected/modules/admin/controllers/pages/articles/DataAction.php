<?php

class DataAction extends AdminAction {

    public function run($id = false) {

        $this->controller->layout = '';
        //читаем параметры
        $curPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $rowsPerPage = isset($_GET['rows']) ? $_GET['rows'] : 5;
        $sortingField = isset($_GET['sidx']) ? $_GET['sidx'] : 1;
        $sortingOrder = isset($_GET['sord']) ? $_GET['sord'] : 1;
        if (!$sortingField)
            $sortingField;

        $res = new CActiveDataProvider('Pages', array(
            'criteria' => array(
                'condition' => '1=1',
            ),
            'pagination' => array(
                'pageVar' => "page",
                'pageSize' => $rowsPerPage,
            ),
        ));

        $response = new stdClass();
        //сохраняем номер текущей страницы, общее количество страниц и общее количество записей
        $response->page = $res->pagination->currentPage;
        $response->total = $res->totalItemCount;
        $response->records = $res->itemCount;

        $i = 0;
        foreach ($res->getData() as $k => $v) {
            $response->rows[$i]['id'] = $v->id;
            $response->rows[$i]['cell'] = array($v->id, $v->title, $v->url, $v->active);
            $i++;
        }
        echo json_encode($response);
    }

}
