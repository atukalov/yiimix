<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DeleteAction extends AdminAction {

    public function run($id) {        
        echo $id;
        $model = Pages::model()->findByPk(intval($id));
        $model->setIsNewRecord(false);
        if($model){
            $model->delete();
        }       

       // $this->controller->redirect('/admin/pages/articles/'.$parent);
    }

}

?>
