<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run($id = false) {
        $parent = $model = false;

        $cacheID = 'ArticlesIndex' . Yii::app()->user->id . 'options';
        $options = Yii::app()->cache->get($cacheID);
        if ($options === false) {
            $options = array();
            $options['mode'] = 0;
            Yii::app()->cache->set($cacheID, $options);
        }

        if (isset($_GET['mode']) && $_GET['mode'] != $options) {
            $options['mode'] = intval($_GET['mode']);
            Yii::app()->cache->set($cacheID, $options);
        }

        if ($id) {
            $options['mode'] = 0;
            $model = Pages::model()->findByPk(intval($id));
            $parent = $model->id;
        }



        $catalog = new Pages("search");
        $catalog->unsetAttributes();


        if (isset($_GET["Pages"])) {
            $tt = false;
            foreach ($_GET["Pages"] as $k => $v)
                if ($v != '')
                    $tt = true;
            if ($tt) {
                $catalog->setAttributes($_GET["Pages"]);
                $options['mode'] = 1;
            } else
                $catalog->parent = intval($id);
        } else
            $catalog->parent = intval($id);


        if ($options['mode'] == 1)
            unset($catalog->parent);



        $max = Yii::app()->db->createCommand()
                ->from("{{pages}}")
                ->select("SUM(views) as m")
                ->queryRow();

        $ac = Actions::model()->findAll();

        $this->render('articles/index', array(
            'catalog' => $catalog,
            'parent' => $parent,
            'parents' => Pages::getParents($id),
            'model' => $model,
            'max' => $max["m"],
            'actions' => $ac,
            'options' => $options,
        ));
    }

}

?>
