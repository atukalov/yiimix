<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

function transliterate($st) {
    $st = strtr($st, "абвгдежзийклмнопрстуфыэАБВГДЕЖЗИЙКЛМНОПРСТУФЫЭ", "abvgdegziyklmnoprstufieABVGDEGZIYKLMNOPRSTUFIE"
    );
    $st = strtr($st, array(
        'ё' => "yo", 'х' => "h", 'ц' => "ts", 'ч' => "ch", 'ш' => "sh",
        'щ' => "shch", 'ъ' => '', 'ь' => '', 'ю' => "yu", 'я' => "ya",
        'Ё' => "Yo", 'Х' => "H", 'Ц' => "Ts", 'Ч' => "Ch", 'Ш' => "Sh",
        'Щ' => "Shch", 'Ъ' => '', 'Ь' => '', 'Ю' => "Yu", 'Я' => "Ya",
    ));
    return $st;
}

class UpdateAction extends AdminAction {

    public function run($id) {
        
        $model = Pages::model()->findByPk(intval($id));
        $model->registerModules('update');
        
        $model->oldUrl=$model->url;

        $r = explode('/', trim($model->url, '/'));
        $model->url = end($r);

        if (isset($_POST['Pages'])) {
            
           // var_dump($_FILES);die;

            $model->attributes = $_POST['Pages'];
            if ($model->url == "" && $model->parent > 0) {
                $model->url = strtolower(transliterate($model->title));
            }

            $model->udate = new CDbExpression('NOW()');

            $model->setIsNewrecord(false);
            if ($model->save()) {
                $model->setIsNewrecord(false);
                Pages::createPath($model->id,$model->oldUrl);
                $model->setIsNewrecord(false);
                //Pages::updateChildrenUrl($model->id);

                if (Yii::app()->request->isAjaxRequest) {
                    echo "Ok";
                    die;
                } else {
                    $p = $model->parent == 0 ? '' : $model->parent;
                    $this->controller->redirect('/admin/pages/articles/' . $p);
                }
            } else {
                var_dump($model->getErrors());
                die;
            }
        }
        Yii::app()->clientScript->registerScriptFile("/js/bootstrap-tooltip.js");
        Yii::app()->clientScript->registerScriptFile("/js/bootstrap-confirmation.js");
        Yii::app()->clientScript->registerScriptFile("/js/files.js");


        $this->controller->render('articles/update', array(
            'model' => $model,
        ));
    }

}

?>
