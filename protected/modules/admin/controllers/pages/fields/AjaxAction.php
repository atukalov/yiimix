<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AjaxAction extends AdminAction {

    public function run() {
      
        if (Yii::app()->request->isAjaxRequest && isset($_POST['F'])) {
            if (!ActionFields::model()->findByAttributes(
                            array('target' => $_POST['F']['target'],
                                'action' => $_POST['F']['action'],
                                'field' => $_POST['F']['field']))) {

                $model = new ActionFields();
                $model->attributes = $_POST['F'];
                $model->target = $_POST['F']['target'];
               // $model->name='_';
                $model->isNewRecord=true;
                if(!$model->save())
                    print_r($model->getErrors ());
            } else {
                ActionFields::model()->deleteAllByAttributes(
                        array('target' => $_POST['F']['target'],
                            'action' => $_POST['F']['action'],
                            'field' => $_POST['F']['field']));
            }
        }
        
        $id = "fields." . $_POST['F']['target'] . "." . $_POST['F']['action'];
        Yii::app()->cache->delete($id);
    }

}
