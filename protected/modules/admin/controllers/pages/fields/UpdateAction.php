<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateAction extends AdminAction {

    public function run($id) {

        $model = Fields::model()->findByPk(intval($id));
          $model->setScenario("update");
        $model->oldname = $model->name;
        $model->oldtarget= $model->target;

        if (isset($_POST['Fields'])) {

            $model->attributes = $_POST['Fields'];
            $model->cerber();
            $model->isNewRecord=false;

            if ($model->save()) {
                $model->fieldUpdate();
                Yii::app()->cache->flush();
                $this->controller->redirect('/admin/pages/fields');
            }
        }

        $this->controller->render('fields/update', array(
            'model' => $model,
        ));
    }

}
