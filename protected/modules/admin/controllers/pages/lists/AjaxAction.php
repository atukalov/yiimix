<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class AjaxAction extends AdminAction {

    public function run() {

        if(Yii::app()->request->isPostRequest && isset($_POST['F'])) {
            if($_POST['F']['val'] == 1) {
                if(!ActionFields::model()->findByAttributes(
                                array('target' => $_POST['F']['target'],
                                    'action' => $_POST['F']['action'],
                                    'field' => $_POST['F']['field']))) {

                    $model = new ActionFields();
                    $model->attributes = $_POST['F'];
                    $model->target = $_POST['F']['target'];
                    $model->save();
                      
                }
            }
            else {
                $model = ActionFields::model()->findByAttributes(
                        array('target' => $_POST['F']['target'],
                            'action' => $_POST['F']['action'],
                            'field' => $_POST['F']['field']));
                if($model)
                    $model->delete();
            }
        }
    }

}
