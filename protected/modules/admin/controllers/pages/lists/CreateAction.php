<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class CreateAction extends AdminAction {

    public function run($id = 0) {

        $model = new Lists('create');
        $model->parent = intval($id);

        if (isset($_POST['Lists'])) {

            $model->attributes = $_POST['Lists'];
            $model->isNewRecord = true;
            if ($model->save()) {
                $p = $model->parent == 0 ? '' : $model->parent;
                $this->controller->redirect('/admin/pages/lists/' . $p);
            } else {
                print_r($model->getErrors());
                Yii::app()->end();
            }
        }

       /* $fields = Fields::model()->findAllByAttributes(array('target' => 'lists'));
        $usesFields = ActionFields::model()->findAllByAttributes(array('target' => 'lists', 'action' => $model->id));
        $fields = new CActiveDataProvider('Fields', array(
            'criteria' => array(
                'condition' => 'entity="lists"',
            ),
            'pagination' => array(
                'pageSize' => 25,
            ),
        ));*/

        $this->controller->render('lists/create', array(
            'model' => $model,
            //'fields' => $fields
        ));
    }

}
