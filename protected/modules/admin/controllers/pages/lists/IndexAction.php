<?php

/*
 * Lists Index Action
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run($id = false) {

        $cat = $id ? intval($id) : 0;
        $model = false;

        if ($id)
            $model = Lists::model()->findByPk(intval($id));


        $catalog = new CActiveDataProvider('Lists', array(
            'criteria' => array(
                'condition' => 'parent=' . $cat,
            ),
            'pagination' => array(
                'pageSize' => 25,
            ),
        ));
        

        $tm = Lists::model()->findAllByAttributes(array('parent' => 0));

        $this->controller->render('lists/index', array(
            'catalog' => $catalog,
            'parent' => $cat,
            'parents' => Lists::getParents($cat),
            'model' => $model,
            'tm' => $tm,
        ));
    }

}

?>
