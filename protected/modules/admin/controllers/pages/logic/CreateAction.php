<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class CreateAction extends AdminAction {

    public function run() {

        if (isset($_POST['Actions'])) {

            $t = Actions::model()->findByAttributes(array('name' => $_POST['Actions']['name']));
            if (!$t) {
                $model = new Actions('Create');
                $model->attributes = $_POST['Actions'];                
                $model->isNewRecord=true;
                if ($model->save()) {
                    // Создаем Controller
                    $action = file_get_contents('./protected/controllers/pages/template/action.php');
                    $action = str_replace('{name}', $model->name, $action);

                    file_put_contents('./protected/controllers/pages/' . $model->name . 'Action.php', $action);

                    // Создаем View
                    $view = file_get_contents('./protected/controllers/pages/template/view.php');
                    $view = str_replace('{name}', $model->name, $view);

                    if (!file_exists('./protected/views/pages/' . $model->name))
                        mkdir('./protected/views/pages/' . $model->name, 0755);

                    file_put_contents('./protected/views/pages/' . $model->name . '/index.php', $view);

                    $this->controller->redirect('/admin/pages/logic');
                }else {
                    print_r($model->getErrors());
                    Yii::app()->end();
                }
            } else {

                $this->controller->redirect('/admin/pages/logic');
            }
        } else {

            $this->controller->render('logic/create', array('model' => new Actions));
        }
    }

}

?>
