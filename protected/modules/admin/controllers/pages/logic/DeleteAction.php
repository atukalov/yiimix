<?php

/*
 * Logic Delete Template
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DeleteAction extends AdminAction {

    public function run($id) {

        $model = Actions::model()->findByPk(intval($id));

        if ($model) {

            //Удаляем поля
            ActionFields::model()->deleteAllByAttributes(array("target" => "pages", "action" => $model->id));
            //Удаляем файлы
            //Удаляем контроллер
            if (file_exists("./protected/controllers/pages/" . $model->name . "Action.php"))
                unlink("./protected/controllers/pages/" . $model->name . "Action.php");
            foreach (glob("./protected/views/pages/" . $model->name . "/*") as $file)
                unlink($file);
            if (file_exists("./protected/views/pages/" . $model->name))
                rmdir("./protected/views/pages/" . $model->name);
            $model->delete();
        }
    }

}
