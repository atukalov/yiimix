<?php

/*
 * Yiimix Шаблоны Pages
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class EditAction extends AdminAction
{

    public function run($id)
    {
        $action = Actions::model()->findByAttributes(array('id' => intval($id)));

        if(!$action)
            throw new CHttpException(404, 'Страница не найдена');
        else
        {
            $views = scandir('./protected/views/pages/'.$action->name);

            $this->controller->render('logic/edit', array(
                'action' => $action,
                'actions' => Actions::model()->findAll(),
                'views' => $views,
            ));
        }
    }

}
