<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class NewAction extends AdminAction
{

    public function run($id)
    {
        $this->controller->layout=false;

        if(Yii::app()->request->isPostRequest  )
        {
              $model=new LogicFile();
              $model->attributes=$_POST['LogicFile'];
              $model->createNew();
              
        }
        else
        {
            $model= new LogicFile();
            $model->action=CHtml::encode($id);
            $model->type=1;
            $this->controller->render('logic/new', array('model' => $model));
        }
    }

}

?>
