<?php
/*
 * Yiimix Save Templates Code Action
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class SaveAction extends AdminAction {

    public function run() {

        if (Yii::app()->request->isPostRequest) {

            if (isset($_POST['LogicFile'])) {

                $model = new LogicFile();
                $model->setAttributes($_POST['LogicFile']);
                //var_dump($_POST['LogicFile']['content']);

                if ($model->save()) {
                    if (!Yii::app()->request->isAjaxRequest)
                        $this->controller->redirect('/admin/pages/logic?action=' . $model->action);
                } else {
                    throw new CException('File not saved');
                }
            }
        }
    }

}

?>
