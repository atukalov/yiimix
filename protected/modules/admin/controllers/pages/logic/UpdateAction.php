<?php

/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateAction extends AdminAction {

    public function run($id) {

        if (isset($_POST['Actions'])) {

            $model = Actions::model()->findByAttributes(array('name' => $_POST['Actions']['oldname']));

            $model->attributes = $_POST['Actions'];
            $model->oldname = $_POST['Actions']['oldname'];

            if ($model->name != $model->oldname) {
                $model->save();
                $on = './protected/controllers/pages/' . $model->oldname . 'Action.php';
                $nn = './protected/controllers/pages/' . $model->name . 'Action.php';
                rename($on, $nn);
                $of = './protected/views/pages/' . $model->oldname;
                $nf = './protected/views/pages/' . $model->name;
                rename($of, $nf);
            } else {
                $model->save();
            }

            $this->controller->redirect('/admin/pages/logic');
        } else {
            $model = Actions::model()->findByPk(intval($id));
            $model->oldname = $model->name;

           // $fields = Fields::model()->findAllByAttributes(array('target' => 'pages'));
           // $usesFields = ActionFields::model()->findAllByAttributes(array('target' => 'pages', 'action' => $model->id));
            $fields = new CActiveDataProvider('Fields', array(
                'criteria' => array(
                    'condition' => 't.target="pages"',
                    
                ),
                'pagination' => array(
                    'pageSize' => 25,
                ),
            ));
           // var_dump($fields->getData());


            $this->controller->render('logic/update', array(
                'model' => $model,
                'fields' => $fields,
            ));
        }
    }

}

?>
