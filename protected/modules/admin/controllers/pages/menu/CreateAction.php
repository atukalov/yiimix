<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class CreateAction extends AdminAction {

    public function run($id = false) {

        $parent = !$id ? 0 : intval($id);

        $model = new Menus;
        $model->parent = $parent;
        $model->isNewRecord=true;




        if (isset($_POST['sc']) && $_POST['sc'] = "top") {

            $model->name = $_POST['name'];
            if ($model->save(array("parent", "name"))) {
                $model->main_id = $model->id;
                $model->save("main");
                echo "/admin/pages/menu/" . $model->id;
                exit;
            }
        } else if (isset($_POST['Menus'])) {

            $model->attributes = $_POST['Menus'];
            if ($model->save()) {
                $p = $model->parent == 0 ? '' : $model->parent;
                $this->controller->redirect('/admin/pages/menu/' . $p);
            }
        } else {

            $max = Yii::app()->db->createCommand()
                            ->from("{{menus}}")
                            ->select(new CDbExpression("MAX(pos) as pos"))
                            ->where("parent=" . $parent)->queryRow();

            if ($max)
                $model->pos = $max["pos"] + 1;

            $this->render('menu/create', array(
                'model' => $model,
            ));
        }
    }

}

?>
