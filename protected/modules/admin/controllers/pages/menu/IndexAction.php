<?php

/*
 * Menu Index Action
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run($id = false) {

        $cat = $id ? intval($id) : 1;
        //$model = false;
        
        $model = Menus::model()->findByPk($cat);
        if(!$model)
            throw new CHttpException(404,'Page not found');
        //var_dump($model);die;
        $catalog = new CActiveDataProvider('Menus', array(
            'criteria' => array(
                'condition' => 'parent=' . $cat,
                'order'=>'pos ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));
        
        $tm = Menus::model()->findAllByAttributes(array("parent"=>0,"op"=>0));
        
        $this->controller->render('menu/index', array(
            'catalog' => $catalog,
            'model' => $model,
            'parent' => $cat,
            'parents' => Menus::getParents($cat),
            'tm'=>$tm,
        ));
    }

}

?>
