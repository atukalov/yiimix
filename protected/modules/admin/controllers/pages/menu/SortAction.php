<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class SortAction extends AdminAction {

    public function run($id = false) {
        $this->controller->layout = "";

        if (Yii::app()->request->isPostRequest && isset($_POST["sorter"])) {

            $temp = explode(",", $_POST["sorter"]);
            if (!empty($temp))
                foreach ($temp as $k => $v) {

                    $com = Yii::app()->db->createCommand();
                    $com->update("{{menus}}", array("pos" => $k), "id=:id", array(":id" => $v));
                }
        } else {
            $id = !$id ? 1 : intval($id);
            $res = Menus::model()->findAllByAttributes(array("parent" => $id), array("order" => "pos ASC"));

            if ($res) {
                ?><div class="sblock"><ul id="sortable"><?php
                foreach ($res as $v) {
                    ?> <li class="btn-success" data-index="<?= $v->id ?>"><?= $v->name ?></li><?php
                        }
                        ?></ul></div>
                <style>
                    .modal{ -webkit-user-select: none;
                            -khtml-user-select: none;
                            -moz-user-select: none;
                            -o-user-select: none;
                            user-select: none;}
                    .modal-dialog{width: 350px}
                    .sblock{overflow: hidden}
                    #sortable{margin: 0 0 0 6px;padding: 0}
                    #sortable li{
                        box-shadow: 0 1px 3px rgba(0,0,0,.5);
                        border-radius: 4px;
                        padding: 4px 10px;
                        width: 300px;
                        margin: 2px 0;
                        display: block;
                        float: left;
                        clear: both;
                        cursor: pointer;

                    }

                    #sortable .ui-sortable-helper{
                        background-color: #ef8a80 !important;

                    }
                </style>
                <script>
                    Array.prototype.in_array = function (p_val) {
                        for (var i = 0, l = this.length; i < l; i++) {
                            if (this[i] == p_val) {
                                return true;
                            }
                        }
                        return false;
                    }

                    function getSorter() {

                        var temp = new Array();
                        $("#sortable>li").each(function (index, val) {
                            var a = $(val).data("index");
                            if (a !== undefined && !temp.in_array(a))
                                temp.push(a);
                        });

                        $.ajax(
                                {type: "POST",
                                    url: "/admin/pages/menu/sort/<?= $id ?>",
                                    data: "sorter=" + temp.toString(),
                                    success: function (a) {
                                        $('.modal').modal("hide");
                                    },
                                    error: function () {
                                        console.log("Ajax error save sort value; SortAction: 82");
                                    }
                                }
                        );


                    }
                    $(function () {
                        $(".modal #sortable").sortable({items: "> li"});
                        $(".modal #sortable").disableSelection();
                    })
                </script>                   
                <?php
            }
        }
    }

}
?>