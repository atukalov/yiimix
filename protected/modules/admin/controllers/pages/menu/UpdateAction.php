<?php

/*
 * Menu Update Action
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateAction extends AdminAction {

    public function run($id) {

        $model = Menus::model()->findByPk(intval($id));

        if(isset($_POST['Menus'])) {
            $model->attributes = $_POST['Menus'];
            if($model->save()) {
                
                $parent = $model->parent == 0 ? '/' : '/'.$model->parent;
                $this->controller->redirect('/admin/pages/menu'.$parent);
            }
        }


        $this->controller->render('menu/update', array(
            'model' => $model,
        ));
    }

}

?>
