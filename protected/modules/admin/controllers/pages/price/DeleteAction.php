<?php

/**
 * Price Delete Action
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DeleteAction extends AdminAction {

    public function run($id) {
        if(Yii::app()->request->isPostRequest){

        $model = Price::model()->findByPk(intval($id));
        $p=$model->parent==0?'':$model->parent;
        $model->delete();
        $this->controller->redirect('/admin/pages/price/'.$p);
    }
    }

}

?>
