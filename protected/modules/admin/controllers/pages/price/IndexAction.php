<?php

/*
 * Price Index Action
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run($id = false) {
        
        $cat = $id ? intval($id) : 0;
        
        $model = false;
        
        if($id)
            $model = Price::model()->findByPk(intval($id));

        $catalog = new CActiveDataProvider('Price', array(
            'criteria' => array(
                'condition' => 'parent='.intval($id),
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));
        
        if($cat!=0)  $cat=Price::model()->findByPk($cat);

        $this->controller->render('price/index', array(
            'cats' => $catalog,
            'model' => $model,
            'parent'=>$cat
        ));
    }

}

?>
