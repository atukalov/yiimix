<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/*
 * Функция преобрахзованиея массива в вид array($key=>$value ...)
 * по названиям столбцов
 * @var $ar - исходный массив
 * @var $k - поле, которое будет ключом
 * @var $v - поле, которое станет значением 
 * @result - возращает результирующий массив
 */
function iter_array($ar, $k, $v) {
    $temp = array();

    foreach ($ar as $val)
        $temp[$val[$k]] = $val[$v];

    return $temp;
}

/*
 * Функция сравнения массивов вида array($key=>$value ...) 
 * @var $target - исходный массив, все разные элементы будут удалены
 * @var $dest - массив с которым сравнивать
 */
function diff_array(&$target, $dest){
       
    foreach ($dest as $k=>$v){
        if($v == $target[$k])
            unset($target[$k]);                
    }    
}


class QSaveAction extends AdminAction {

    public function run() {

        if (Yii::app()->request->isAjaxRequest) {

            $id = $_POST['id'];
            $data = $_POST['DEC'][$id];
            $key = array_keys($data);
            $res = iter_array(Yii::app()->db->createCommand('select id,' . $id . ' FROM {{price}} WHERE id IN(' . join(',', $key) . ')')->queryAll(),'id',$id);

            diff_array($data, $res);
            
            foreach($data as $k=>$v){
                $sql='UPDATE {{price}} SET `'.$id.'`="'.$v.'" WHERE `id`='.$k;
                Yii::app()->db->createCommand($sql)->execute();                
            }
            
           // var_dump($data);
        }
    }

}

?>
