<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateAction extends AdminAction {

    public function run($id) {

        $model =  Price::model()->findByPk(intval($id));

        if(isset($_POST['Price'])) {

            $model->attributes = $_POST['Price'];
            if($model->save()) {
                $p = ($model->parent == 0) ? '' : $model->parent;
                $this->controller->redirect('/admin/pages/price/'.$p);
            }
        }

        $this->controller->render('price/update', array(
            'model' => $model,
            'parent'=>  Price::model()->findByPk($model->parent)
        ));
    }

}

?>
