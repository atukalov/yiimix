<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run() {

        $user = User::model()->findByPk(Yii::app()->user->id);
        
        $sql="SELECT * FROM tbl_planner WHERE user=".Yii::app()->user->id." and MONTH(date)=".date("m")." ORDER BY date ASC";
        $res=Yii::app()->db->createCommand($sql)->queryAll();
        $temp=array();
        foreach($res as $v){
            $temp[]=date('d',strtotime($v['date']));
        }
       // var_dump($temp);
        
        
        $this->controller->render('planner/index', array(
            'user' => $user,
            'target'=>$temp,
            'targets'=>$res,
        ));
        
    }

}

?>
