<?php

/*
 * Yiimix  Delete SEF Action
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/sef
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DeleteAction extends AdminAction
{

    public function run($id)
    {

        if(intval($id) > 0){
            $res=Urls::model()->deleteByPk(intval($id));
            if($res)
                echo "ok";
            else
                echo "error";
        }

    }

}
