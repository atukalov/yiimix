<?php
/*
 * Yiimix Ajax Create SEF Action
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/sef
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class CreateAction extends AdminAction {

    public function run($id, $target) {

        if (isset($_POST["Urls"]) && Yii::app()->request->isPostRequest) {

            $model = new Urls();
            $model->isNewRecord=true;
            $model->setAttributes($_POST["Urls"]);
            
            
            if ($model->url == "") {
                echo "Empty URL field";
                die;
            }

            if ($model->save()) {
                echo "ok";
            } else {
                print_r($model->getErrors());
            }
            
            die;
        } else {

            $model = new Urls();
            $model->target = $target;
            $model->item = intval($id);
            $model->active = 1;

            $page = Pages::model()->findByPk($model->item);
            ?>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Добавить альтернативный URL</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="fa fa-info-circle"></i> Информация</h4> SEF модуль предназначен для переноса URL страницы на новый URL.<br>
                    Используя этот адрес, вы будете перенаправлены <span class="text-danger">(301 Redirect)</span> на основной URL.
                </div>


                <p style="padding: 10px 20px; font-size: 15px"><strong class="text-warning"><?= $page->title ?></strong><br style="font-size: .9em"><span class="label label-default">Current:</span> /<?= $page->url ?></p>

                <?php
                $form = $this->controller->beginWidget('CActiveForm', array(
                    'id' => 'sef-create',
                    'enableAjaxValidation' => false,
                    "htmlOptions" => array("class" => "form-horizontal form-bordered"),
                ));
                ?>
                <?php echo $form->hiddenField($model, 'target'); ?>
                <?php echo $form->hiddenField($model, 'item'); ?>
                    <?php echo $form->hiddenField($model, 'active'); ?>
                <div class="form">
                        <?php echo $form->errorSummary($model); ?>
                    <div class="form-group">
                            <?php echo $form->labelEx($model, 'url', array("class" => "col-md-2 control-label")); ?>
                        <div class="col-md-10">
            <?php echo $form->textField($model, 'url', array("class" => "form-control")); ?>
                        </div>
                    </div>

                </div><!-- form -->
            <?php $this->controller->endWidget(); ?>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <a data-target="ajax-form" onclick="ajaxModalSubmit($(this))" data-id="pages-grid" data-href="<?= Yii::app()->request->requestUri ?>" data-index="<?= $form->id ?>"  class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Добавить</a>
                </div>
                <div class="pull-left">
                    <a href="<?= Yii::app()->createUrl("admin/sef/index") ?>" class="btn btn-alt btn-sm btn-info"><i class="fa fa-link"></i> Управлять SEF</a>
                </div>
            </div>

            <script>


            </script>

            <?php
        }
        // $this->render('ajax/create', array('model' => $model));
    }

}
