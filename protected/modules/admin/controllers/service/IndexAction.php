<?php

class IndexAction extends AdminAction {

    public function run($id = false) {

        $m = 'pages_' . $id;

        if (key_exists($m, $this->controller->actions()))
            $this->controller->run($m);
        else {

            $cat = isset($_GET['cat']) ? intval($_GET['cat']) : 0;
            $model = false;
            if ($cat > 0)
                $model = ShopCats::model()->findByPk($cat);

            $catalog = new CActiveDataProvider('Pages', array(
                'criteria' => array(
                    'condition' => 'parent=' . $cat,
                ),
                'pagination' => array(
                    'pageSize' => 25,
                ),
            ));

            $this->controller->render('index', array(
                'cats' => $catalog,
                'model' => $model
            ));
        }
    }

}

?>
