<?php

class SitemapAction extends AdminAction {

    public function run($process = false) {


        if ($process) {
            $filename = './sitemap.xml';
            $split = false;
            $cSplit = 0;
            $extend = false;

            $r = Config::get('sitemap', true);

            if ($r && !empty($r)) {

                if (isset($r['split'][0])) {
                    $cSplit = $split = ($r['split'][0] == 0 ? false : $r['split'][0]);
                }

                if (isset($r['extend'][0]))
                    $extend = $r['extend'][0];


                $count = 0;
                if ($split) {
                    foreach ($r['entity'][0] as $entity)
                        $count+=intval($entity::model()->count());

                    if ($count > $r['split'][0]) {
                        $split = true;
                    } else {
                        $split = false;
                    }
                }
                //$extend = true;

                if (!$split) {
                    $f = fopen($filename, 'w');
                    fputs($f, '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n");
                    fputs($f, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\r\n");

                    foreach ($r['entity'][0] as $entity) {
                        $sm = $entity::$sitemap;
                        $sql = 'SELECT `id`,`' . $sm['url'] . '`,`' . $sm['lastmod'] . '`' . (!$sm['changefreq'] ? '' : ',`' . $sm['changefreq'] . '`') . ' FROM {{' . strtolower($entity) . '}} WHERE `active`=1';
                        $res = Yii::app()->db->createCommand($sql)->query();
                        while (($i = $res->read())) {                        

                            fputs($f, '  <url>' . "\r\n" . '    <loc>' . Yii::app()->getBaseUrl(true) . '/' . $i['url'] . '</loc> ' . "\r\n");
                            if ($extend) {
                                if ($i[$sm['lastmod']] == '0000-00-00 00:00:00') {
                                    $time = date('Y-m-d H:i:s', time());
                                    $sql = 'UPDATE {{' . strtolower($entity) . '}} SET `' . $sm['lastmod'] . '`= NOW() WHERE `id`=' . $i['id'];
                                    Yii::app()->db->createCommand($sql)->execute();
                                } else
                                    $time = $i[$sm['lastmod']];
                                fputs($f, '    <lastmod>' . $time . '</lastmod> ' . "\r\n");
                                if ($sm['changefreq'])
                                    fputs($f, '    <changefreq>' . $i[$sm['changefreq']] . '</changefreq> ' . "\r\n");

                                //fputs($f, '    <priority>' . $priority . '</priority> ' . "\r\n");
                            }
                            fputs($f, '  </url>' . "\r\n");
                        }
                    }

                    fputs($f, '</urlset>');
                    fclose($f);
                } else {
                    $f = fopen($filename, 'w');
                    fputs($f, '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n");
                    fputs($f, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\r\n");
                    fclose($f);
                }
            }
        } else {
            $model = new SitemapModel();


            $this->controller->render('sitemap', array(
                'model' => $model
            ));
        }
    }

}

?>
