<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class CreateAction extends AdminAction {

    public function run($f = null) {

        if ($f === null)
            $f= Yii::app()->params['uploadFolder'];
        //var_dump($f);die;
        
        if (isset($_POST['Cfolder'])) {            
            if($_POST['Cfolder']['name']!='')
            mkdir($_POST['Cfolder']['dir'].'/'.$_POST['Cfolder']['name'], 0777, true);
        }


        $this->controller->renderPartial('files/cfolder', array(
            'f' => $f,
        ));
    }

}

?>
