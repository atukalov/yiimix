<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class DeleteAction extends AdminAction {

    public function run($f = null, $dir = null) {

        if ($dir === null)
            $dir = Yii::app()->params['uploadFolder'];

        if ($f === null)
            die();
        $files = explode(';', $f);
        $mes='';
        if (count($files) == 0)
            die;
        else{
            $temp=array();
            foreach($files as $v){
            
            if(is_dir($dir . '/' .$v)){
                 $fi = scandir($dir . '/' .$v);
                 
                 if(count($fi)>2){
                     $mes.='<br>'.'<p class="error">Вы не можете удалить папку <b>'.$v.'</b>, так как она содержит файлы, сначала удалите файлы из этой папки. Потом сможете удалить папку. Папка удалена из запроса</p>';
                 }else
                     $temp[]=$v;
            }else
                $temp[]=$v;
            }
            }
            if(count($temp)>0){
                $mes.='<br>'.'<p>Вы действительно хотите удалить файлы: <b>'. join(', ',$temp).'</b></p>';
                $del=true;
            }else{$del=false;}
//var_dump($temp);
            $f=join(';',$temp);

        if (isset($_POST['Dfolder'])) {
            if ($_POST['Dfolder']['files'] != '') {
                $files = explode(';', $_POST['Dfolder']['files']);

                foreach ($files as $v) {
                    if (file_exists($v)) {
                        if (is_dir($files . '/' . $v))
                            rmdir($files . '/' . $v);
                        else
                            unlink($files . '/' . $v);
                    }
                }
            }
        }


        $this->controller->renderPartial('files/dfolder', array(
            'mes'=>$mes,
            'f' => $f,
            'dir' => $dir,
            'del'=>$del,
        ));
    }

}

?>
