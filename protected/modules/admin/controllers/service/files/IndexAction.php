<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run($id = false) {

        $m = 'files_' . $id;

        if (key_exists($m, $this->controller->actions()))
            $this->controller->run($m);
        else {
            
            $files=array();
            
            
            $path='./images/admin';
            if(isset($_GET['dir']))
                    fileList::init($path,$_GET['dir']);
                else
            fileList::init($path,'');
                
            $res=  fileList::read();

            $this->controller->render('files/index', array(
                'catalog' => $res,
            ));
        }
        
        
    }

}

?>
