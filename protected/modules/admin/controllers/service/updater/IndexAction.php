<?php

/*
 * Yiimix Updater
 * 
 * @category   YimMix Updater
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class IndexAction extends AdminAction {

    public function run() {
        ini_set("soap.wsdl_cache_enabled", 0);
        $cl = new SoapClient('http://loc.yiimix.ru/updates/quote');
        var_dump($cl->getCurrent('yiimix.base'));
        var_dump($cl->getAllVer());
    }

}

?>
