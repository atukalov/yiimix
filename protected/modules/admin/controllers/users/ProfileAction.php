<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class ProfileAction extends AdminAction {

    public function run($id=null) {
        if($id===null)
            $id=Yii::app()->user->getId();

        $this->controller->render('profile', array(
            'model' => User::model()->findByPk(intval($id))
        ));
    }

}

?>
