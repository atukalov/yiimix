<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateAction extends AdminAction {

    public function run($id) {

        $profiles = Profiles::model()->findByPk(intval($id));
         //var_dump ($profiles);die;
        if ($profiles===null){
            $profiles = new Profiles;
            $profiles->id=intval($id);
            $profiles->isNewRecord=true;
            if(!$profiles->save()){
                var_dump ($profiles->getErrors());die;
            }
            unset($profiles);
        }

        $model = User::model()->findByPk(intval($id));
        $profiles = Profiles::model()->findByPk(intval($id));

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $profiles->attributes = $_POST['Profiles'];
            if ($model->save() && $profiles->save()) {
                echo 'Ok';
                Yii::app()->end();
            } else {
                var_dump($profiles->getErrors());
                var_dump($model->getErrors());
                Yii::app()->end();
            }
        } else {

            $model->registerModules('update');

            $this->render('update', array(
                'model' => $model
            ));
        }
    }

}

?>
