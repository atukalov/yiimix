<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class PosAction extends AdminAction
{

    public function run()
    {
        $this->controller->layout = false;

        if(Yii::app()->request->isAjaxRequest && !empty($_POST))
        {

            if(Yii::app()->user->isAdmin())
            {
                Config::lockInterface(
                    Yii::app()->user->id, $_POST["path"], array("main" => array_map('intval', $_POST["main"]),
                    "left" => array_map('intval', $_POST["left"])
                ));
            }
        }
        else
        {

            var_dump($_POST);
            die;
        }
    }

}

?>
