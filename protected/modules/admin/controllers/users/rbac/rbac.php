<?php
$auth=Yii::app()->authManager;
$auth->clearAll();
$role = $auth->createRole('guest');
$role->addChild('adminLogin');

$role = $auth->createRole('user',"Регистрированный пользователь");
$role->addChild('guest');

$role = $auth->createRole('moderator');
$role->addChild('user');
$role->addChild('adminDashboard');
$role->addChild('adminLogin');

$role = $auth->createRole('freelancer');
$role->addChild('moderator');

$bizRule='return Yii::app()->user->id==$params["post"]->authID;';
$task=$auth->createTask('pages_fr_update','pages_update1',$bizRule);
$task->addChild('pages_update');
$role->addChild('pages_fr_update');


$role = $auth->createRole('content');
$role->addChild('moderator');
$role->addChild('lists');
$role->addChild('lists_create');
$role->addChild('lists_update');
$role->addChild('lists_delete');
$role->addChild('pages');
$role->addChild('pages_create');
$role->addChild('pages_update');
$role->addChild('pages_delete');


$role = $auth->createRole('developer');
$role->addChild('content');
$role->addChild('template');
$role->addChild('template_create');
$role->addChild('template_update');
$role->addChild('template_delete');
$role->addChild('template_edit');


$role = $auth->createRole('admin');
$role->addChild('developer');

$auth->assign('freelancer', 1);
$auth->save();