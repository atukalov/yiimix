<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FileLibBehavior extends CActiveRecordBehavior {

    public function events() {
        return array_merge(parent::events(), array(
            'onBeforeDelete' => 'beforeDelete',
        ));
    }

    public function behaviors() {
        return array();
    }

    /**
     * Регистрируем Widgets по сценарию
     */
    public function registerModules($action) {
        $model = $this->getOwner();
        if ($action == 'update') {
            Yii::app()->controller->widgets['right']['FileLibWidget'] = array('admin.extensions.filelib.FileLibWidget', array('target' => $model->entity, 'item' => $model->id));
            Yii::app()->controller->widgets['right']['FilesWidget'] = array('admin.extensions.filelib.FilesWidget', array('item' => $model->id));
        }
    }

    public function beforeDelete($event) {
        $err = false;
        $ern = array();
        $target = trim(trim($this->owner->tableName(), '}'), '{');
        $files = Filelib::model()->findAllByAttributes(array('target' => $target, 'item' => $event->sender->id));
        if (!empty($files)) {
            $dir = realpath('./uploads/' . $target . '/' . $event->sender->id);
            foreach ($files as $k => $v) {
                if (file_exists($dir . '/' . $v->filename)) {
                    unlink($dir . '/' . $v->filename);
                    if (($ern = error_get_last()) !== null)
                        $err = true;
                }
            }
            if (file_exists($dir))
                rmdir($dir);

            if (($ern = error_get_last()) !== null)
                $err = true;
        }

        if ($err == true) {
            $event->isValid = false;
            throw new CHttpException('500', 'Article Delete Error');
        } else {
            Filelib::model()->deleteAllByAttributes(array('target' => $target, 'item' => $event->sender->id));
            $event->isValid = true;
        }
    }

}
