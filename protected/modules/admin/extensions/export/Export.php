<?php

class Export
{

    private $_driver;
    private $_data;
    public $content;

    public function __construct($driver = false)
    {
        if($driver)
            $this->setDriver($driver);
    }

    public function setDriver($driver)
    {
        $path = __DIR__."/drivers/".$driver.".php";
        require $path;
        $this->_driver = new $driver();
    }

    public function getDriver()
    {
        return $this->driver->id;
    }

    public function bySql($sql)
    {
        $this->_data = Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function byModel($model)
    {
        $model = new $model();
        $this->_data = $model->findAll();
    }

    public function setData($data)
    {
        $this->_data = $data;
    }

    public function prepare()
    {
        $this->content = $this->_driver->run($this->_data);
    }
    
    public function save($filename)
    {        
        file_put_contents($filename, $this->content);
    }

}
