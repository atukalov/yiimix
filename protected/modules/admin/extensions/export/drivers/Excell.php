<?php

class Excell extends Driver
{

    public function run($data)
    {
        if(empty($data))
            return '';

        $text = '<?xml version="1.0"?>'
            .PHP_EOL. '<?mso-application progid="Excel.Sheet"?>'
            .PHP_EOL. '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">'
            .PHP_EOL. '<Styles>'
            .PHP_EOL. '<Style ss:ID="bold">'
            .PHP_EOL. '<Font ss:Bold="1"/>'
            .PHP_EOL. '</Style>'
            .PHP_EOL. '</Styles>'
            .PHP_EOL. '<Worksheet ss:Name="WorksheetName">'
            .PHP_EOL. '<Table>';
        foreach($data as $key => $value)
        {
            $text.=PHP_EOL.'<Row>';
            foreach($value as $k => $v)
            {
                 $text.=PHP_EOL.'<Cell><Data ss:Type="String">'.$v.'</Data></Cell>';
            }
            $text.=PHP_EOL.'</Row>';
        }

        $text.=PHP_EOL.'</Table>'.PHP_EOL.'</Worksheet>'.PHP_EOL.'</Workbook>';
        return iconv("UTF-8", "CP1251", $text);
    }

}
