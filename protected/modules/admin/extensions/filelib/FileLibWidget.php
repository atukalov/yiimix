<?php

class FileLibWidget extends adminWidget
{

    public $item;
    public $target;
    public $items = array();

    public function init()
    {
        parent::init();


        $this->title = Yii::t('admin','Attached Files');
        $this->icon = "fa fa-camera-retro";

        $this->minimize = TRUE;
        $this->fullscreen = TRUE;

        if(!$this->target || !$this->item)
            exit();
        
        $this->items = Filelib::model()->findAllByAttributes(array("item" => $this->item, "target" => $this->target));        
        $this->registerScript();
    }

    
    
    public function content()
    {
        ?> 
        <div class="content-float"><?php
            $this->widget("zii.widgets.CListView", array(
                'id' => 'filelibview',
                'template' => "{items}",
                'dataProvider' => new CArrayDataProvider($this->items),
                'itemView' => 'application.modules.admin.views.filelib._files',
            ));
            ?></div>




        <?php
    }

    private function registerScript()
    {
        $sc = Yii::app()->getClientScript();
        $sc->registerScriptFile("/js/lightbox2/js/lightbox.min.js");
        $sc->registerCssFile("/js/lightbox2/css/lightbox.css");
        //$sc->registerScript("images","jQuery('.lb').lightbox()");
    }

}
