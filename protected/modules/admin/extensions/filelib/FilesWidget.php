<?php

class FilesWidget extends adminWidget {

    public $item;

    public function init() {
        $this->title = Yii::t('admin','Upload files');
        $this->icon = "fa fa-files-o";

        parent::init();
    }

    public function buttons() {
        ?><a onclick="filesUpload()" class="btn btn-sm btn-alt btn-primary"  title="<?=Yii::t('admin','Upload')?>" id="fileupload-link"><i class="fa fa-upload"></i></a>
        <a onclick="resetUploadForm()" class="btn btn-sm btn-alt btn-danger"  title="<?=Yii::t('admin','Clear Form')?>" id="fileupload-clear"><i class="fa fa-refresh"></i></a>
        <?php
    }

    public function content() {
        $this->controller->renderPartial("admin.views.filelib._upload", array("id" => $this->item, "update" => "filelibview"));
    }

}
