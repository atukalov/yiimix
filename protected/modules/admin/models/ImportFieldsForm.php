<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ImportFieldsForm extends CFormModel {

    public $title;
    public $url;
    public $art;
    public $preview_text;
    public $detail_text;
    public $preview_image;
    public $detail_image;
    public $price;
    public $old_price;
    public $meta_keywords;
    public $meta_description;
    public $active;
    public $count;
    public $selled;
    public $position;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('title, url,art,preview_text,detail_text,preview_image,detail_image,price,old_price,meta_keywords,meta_description,active,count,selled, position', 'safe'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'title' => 'Название',
            'url' => 'Url',
            'art' => 'Артикул',
            'preview_text' => 'Аннонс',
            'detail_text' => 'Описание',
            'preview_image' => 'Маленькая картинка',
            'detail_image' => 'Картинка',
            'price' => 'Цена',
            'old_price' => 'Старая цена',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'active' => 'Показывать',
            'count' => 'Количество на складе',
            'selled' => 'Продано',
            'position' => 'Позиция',
        );
    }

}