<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ImportForm extends CFormModel {

    public $system;
    public $file;
    public $mode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('system, file, mode', 'required'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    
 
    
    public function attributeLabels() {
        return array(
            'system' => 'Импортировать из',
            'file' => 'Файл для импорта',
            'mode' => 'Режим импорта',
        );
    }

}