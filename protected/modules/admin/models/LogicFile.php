<?php
/*
 * Yiimix Tempales Files FormModel
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class LogicFile extends CFormModel {

    public $fileName;
    public $file;
    public $content;
    public $action;
    public $type;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('file,fileName,action,type,content', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
        );
    }
    private function getFilename(){
       
        if ($this->type == 0) {
            $this->fileName=Yii::app()->basePath.'/controllers/pages/' . $this->action . 'Action.php';
        } elseif($this->type==1) {
            $this->fileName = Yii::app()->basePath.'/views/pages/' . $this->action . '/' . $this->file;
        } 
    }

    public function load() {
       $this->getFilename();            
       $this->content = file_get_contents($this->fileName);
    }

    public function save() {
        $this->getFilename();
        
        rename($this->fileName, $this->fileName . '.bak');
        file_put_contents($this->fileName, $this->content);
       // var_dump($content);
        $content=file_get_contents($this->fileName);
        if($content==$this->content)
            unlink($this->fileName . '.bak');
        return true;
    }
    
    
    public function createNew() {
        $this->getFilename();
        
        //var_dump($this->fileName);die;
        if($this->fileName!='')
           file_put_contents($this->fileName, '/*'.$this->file.'*/'); 
        
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('admin','File Name'),
        );
    }

}
