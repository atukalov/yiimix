<?php

/*
 * Yiimix Tempales Files FormModel
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class SitemapModel extends CFormModel {

    public $split;
    public $extend;
    public $entity;
    public $media;
    public $length;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('split,extend,media,length', 'required'),
            array('split,extend,media,length', 'numerical', 'integerOnly' => true),
        );
    }

    public function load() {
        $cfg = Config::get('sitemap', true);
        $this->split = $cfg['split'][0];
        $this->extend = $cfg['extend'][0];
        $this->media = $cfg['media'][0];
        $this->length = $cfg['length'][0];

    }

    public function save() {

        return true;
    }

    public function attributeLabels() {
        return array(
            'split' => Yii::t('admin', 'Split file'),
            'extend' => Yii::t('admin', 'Extended syntax'),
            'media' => Yii::t('admin', 'Use media file'),
            'length' => Yii::t('admin', 'Count row'),
        );
    }

}
