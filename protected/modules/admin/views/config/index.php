<?php
/*
 * Yiimix configurater IndexAction
 *
 * @category   YimMix
 * @package    yiimix.config
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
//$this->breadcrumbs["Config"] = Yii::app()->createUrl("admin/config");
$this->breadcrumbs[] = 'Options';
$this->pageTitle = 'Components options';
?>
<div class="panel panel-default" id="rbac-panel">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Components options") ?></b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">    
            <a href="<?php echo Yii::app()->createUrl("admin/config/install") ?>" class="btn btn-sm  btn-success"><i class="fa fa-upload"></i> Install</a>
            <a href="<?php echo Yii::app()->createUrl("admin/config/install") ?>" class="btn btn-sm btn-alt btn-warning"><i class="fa fa-shopping-cart"></i> On-line shop</a>
        </div>
        <div class="pull-right">
            <a href="<?php echo Yii::app()->createUrl("admin/config/install") ?>" class="btn btn-sm btn-alt btn-default"><i class="gi gi-cogwheel"></i> Installed Modules</a>
        </div>

    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'pageslist',
            'dataProvider' => $cat,
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable table-responsive",
            'columns' => array(
                array(
                    'class' => 'CDataColumn',
                    'name' => 'title',
                    'type' => 'raw',
                    'header' => 'title',
                    'value' => 'CHtml::link($data["title"], "/".$data["url"])',
                ),
            ),
            'pager' => array(
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'htmlOptions' => array(
                    'class' => 'pagination',
                    'cssFile' => "",
                )
            ),
        ));
        ?>
    </div>
</div>
