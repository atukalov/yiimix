<?php

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'upload-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )
);

echo $form->labelEx($model, 'filename');
echo $form->fileField($model, 'filename');
echo $form->error($model, 'filename');
// ...
echo CHtml::submitButton('Submit');
$this->endWidget();
