<div class="col-md-8 col-lg-9">
    <div class="panel panel-default" id="rbac-panel">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-list text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", " Custom Fields Options") ?></b></h2>
        </div>
        <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
            <div class="pull-left">    
                <a  class="btn btn-sm btn-success"><i class="fa fa-magnet"></i> <?=Yii::t('admin','Assign')?></a>
            </div>
          
        </div>
        <div class="panel-body" style="padding: 0;margin: 0;">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'pageslist',
                'dataProvider' => $entity,
                'cssFile' => false,
                'template' => '{items}{pager}',
                'enableHistory' => true,
                'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable table-responsive",
                'columns' => array(
                    'name',
                    'title',
                ),
                'pager' => array(
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination',
                        'cssFile' => "",
                    )
                ),
            ));
            ?>

        </div>
    </div>
</div>

<div class="col-md-4 col-lg-3">
    <div class="block">
        <div class="block-title">

            <h2><strong>Модули</strong></h2>
        </div>
        <ul class="nav nav-pills nav-stacked" style="padding-bottom: 20px" id="top-menu">

            <?php
            foreach ($modules as $v) :
                 $cl = Yii::app()->request->requestUri == '/'.$v['url'] ? " class=\"active\"" : "";                
                ?>            
                <li<?=$cl?>>
                    <a href="/<?= $v['url'] ?>"><?= $v['title'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>



</div>