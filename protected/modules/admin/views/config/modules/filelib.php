<div class="col-md-8 col-lg-9">
    <div class="panel panel-default" id="rbac-panel">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-list text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", " Custom Fields Options") ?></b></h2>
        </div>
        <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
            <div class="pull-left">    
                <a  class="btn btn-sm btn-success"><i class="fa fa-magnet"></i> <?=Yii::t('admin','Assign')?></a>
            </div>
          
        </div>
        <div class="panel-body" style="padding: 0;margin: 0;">
          
            
            
        </div>
    </div>
</div>

<div class="col-md-4 col-lg-3">
    
  <?php $this->widget('admin.widgets.WModules'); ?>
</div>