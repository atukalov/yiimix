<?php


$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>new CArrayDataProvider($catalog),
    'itemView'=>'_view',  
    'sortableAttributes'=>array(
        'title',        
    ),
));