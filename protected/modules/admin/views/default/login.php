<?php Yii::app()->clientScript->registerCoreScript("jquery")?>
<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"><img src="/images/admin/logo.png" width=""></h1>

        </div>
        <h3>Welcome to Yiimix</h3>
        <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
        </p>
        <p>Login in. To see it in action.</p>

        <?php echo CHtml::beginForm("/admin/login", "post", array("role" => "form", "class" => "m-t")); ?>
        <div class="form-group">
            <?php echo CHtml::activeTextField($model, 'username', array("placeholder" => "Username", "class" => "form-control", "required" => "")) ?>
        </div>
        <div class="form-group">
            <?php echo CHtml::activePasswordField($model, 'password', array("placeholder" => "Password", "class" => "form-control", "required" => "")) ?>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeCheckBox($model, 'rememberMe'); ?>
            <?php echo CHtml::activeLabelEx($model, 'rememberMe'); ?>
        </div>
        <?php echo CHtml::submitButton(Yii::t('app',"Login"), array("class" => "btn btn-primary block full-width m-b")); ?>

        <?php echo CHtml::link("<small>" . Yii::t('app',"Lost Password?") . "</small>", '/user/recovery'); ?>


        <?php echo CHtml::endForm(); ?>

    </div>
</div>

