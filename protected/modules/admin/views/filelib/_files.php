<div class="image-box">
    <?php if ($data->kind == "pdf"): ?>
        <i class="fa fa-file-pdf-o text-warning" style="font-size:82px;margin: 15px 0 0 0;display:block; text-align: center"></i>
    <?php elseif ($data->kind == "image"): ?>    
        <a class="image" href="/uploads/<?= $data["target"] ?>/<?= $data["item"] ?>/<?= $data["filename"] ?>" data-lightbox="roadtrip"
           style="background-image: url('/uploads/<?= $data["target"] ?>/<?= $data["item"] ?>/<?= $data["filename"] ?>')">       
        </a>   
    <?php elseif ($data->kind == "sound text-primary"): ?>
        <i class="fa fa-file-sound-o" style="font-size:82px;margin: 15px 0 0 0;display:block; text-align: center"></i>
    <?php elseif ($data->kind == "arch"): ?>
        <i class="fa fa-file-archive-o" style="font-size:82px;margin: 15px 0 0 0;display:block; text-align: center"></i>
    <?php endif; ?>

    <div class="actions">
        <div class="dropdown">
            <?php if ($data["primary"] == 1): ?>
                <button onclick="fileMain(<?= $data["id"] ?>)" class="btn btn-xs btn-warning btn-alt " type="button" title="Отметить как обычную" >  
                    <i class="fa fa-check-circle"></i>
                <?php else: ?>
                    <button onclick="fileMain(<?= $data["id"] ?>)" class="btn btn-xs btn-default btn-alt" type="button" aria-expanded="true"  title="Отметить как главную">   
                        <i class="fa fa-check-circle-o"></i>
                    <?php endif; ?>                    
                </button>

                <button onclick="fileUpdate(<?= $data["id"] ?>)"class="btn btn-xs btn-info btn-alt" type="button" aria-expanded="true"  title="Информация">   
                    <i class="fa fa-info-circle"></i>
                </button>
                <button onclick="fileCode(<?= $data["id"] ?>)" class="btn btn-xs btn-default btn-alt" type="button"  title="Получить код">   
                    <i class="fa fa-code"></i>
                </button>
                <button trigger="click" data-toggle="confirmation-popout" data-placement="top" data-index="<?= $data["id"] ?>" class="btn btn-xs btn-danger btn-alt" type="button" style="float: right" title="Удалить?">   
                    <i class="fa fa-trash"></i>
                </button>


        </div>
    </div>

</div>