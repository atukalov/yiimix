<?php
echo CHtml::form($this->createUrl('admin/filelib/upload'), 'post', array('id' => 'filelib-upload', 'enctype' => 'multipart/form-data'));
echo CHtml::hiddenField("Filelib[entity]", "pages");
echo CHtml::hiddenField("Filelib[item]", $id);
?>
<?php
$this->widget('CMultiFileUpload', array(
    'name' => 'files[]',
    'accept' => 'jpeg|jpg|gif|png|svg|zip|pdf|mp3|avi',
    'remove' => '<span class="label label-danger"><i class="fa fa-times"></i></span>',
    'options' => array(
        'onFileSelect' => 'function(e, v, m){$(".MultiFile-list").show(); $("#fileupload-link,#fileupload-clear").removeAttr("disabled"); }',
        'afterFileSelect' => 'function(e, v, m){ }',
        'onFileAppend' => 'function(e, v, m){  }',
        'afterFileAppend' => 'function(e, v, m){ }',
        'onFileRemove' => 'function(e, v, m){  }',
        'afterFileRemove' => 'function(e, v, m){if($(".MultiFile-label").length == 0){$(".MultiFile-list").hide(); $("#fileupload-link,#fileupload-clear").attr("disabled",true); }}',
    ),
));
?>
</form>
<?php
Yii::app()->clientScript->registerScript("fileupload", " 
    function resetUploadForm(){
    $('#filelib-upload').reset();
    $('.MultiFile').remove(); 
    $('.MultiFile-list').html('');
    $('.MultiFile-applied').attr('style','');
    $('#fileupload-link,#fileupload-clear').attr('disabled',true);
}
    function filesUpload() {
        var formData = new FormData();

        $('input[name=\'files[]\']').each(function (i, el) {

            console.log(i, el.files[0]);
            formData.append('files[]', el.files[0]);
        });

        $('#filelib-upload input').each(function () {
            if ($(this).attr('type') !== 'file')
                formData.append($(this).attr('name'), $(this).val());
        });

        $.ajax({
            url: '" . $this->createUrl('/admin/filelib/upload') . "',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data)
            {              
                $.fn.yiiListView.update('" . $update . "', {ajaxUpdate: '" . $update . "'});
                    $('#ajaxModal').modal('hide');
                    resetUploadForm();

            }
        });


    }", CClientScript::POS_END);



