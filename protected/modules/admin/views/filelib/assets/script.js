function changeSize(w,h){
    
$('.cropit-image-preview').css({
        width: w + 'px',
        height: h + 'px'
    });

    $('.image-editor').cropit('previewSize', {width: w, height: h});

    if (!$('.image-editor').cropit('isZoomable'))
        $('.cropit-image-zoom-container').hide();
    else
        $('.cropit-image-zoom-container').show();    
}

$('.image-editor').cropit({
    imageBackground: true,
    imageBackgroundBorderWidth: [35, 35, 35, 35],
    imageState: {
        src: imgSource
    }
});
$('.export').click(function() {
    var imageData = $('.image-editor').cropit('export');
    window.open(imageData);
});


$('#sizes').change(function() {
    if ($(this).val() != "0") {
        var s = $(this).val().split('x');
        var w = s[0];
        var h = s[1];
    } else {
        var w = parseInt($("input#w").val());
        var h = parseInt($("input#h").val());      
    }
    changeSize(w,h);
});