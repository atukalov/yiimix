<?php
$this->widget("zii.widgets.CListView", array(
    'dataProvider' => new CArrayDataProvider($images,array('pagination'=>false)),
    'itemView' => '_images', // refers to the partial view named '_post'
    'template' => '{items}',
));
?>
<style>
    .item-img{width:200px;float:left;height:160px;border:solid 1px #eee; border-radius: 4px;margin: 4px;position: relative}
    .item-img img{width: 100%; max-height: 100%}
    .item-img p{position: absolute; display: block;
    top:0;left:0;
    background: #cc0;
    margin:0;
    padding:4px;}
    </style>
<script>

    function GetUrlParam(paramName)
    {
        var oRegex = new RegExp('[\?&]' + paramName + '=([^&]+)', 'i');
        var oMatch = oRegex.exec(window.top.location.search);

        if (oMatch && oMatch.length > 1)
            return decodeURIComponent(oMatch[1]);
        else
            return '';
    }

    $("img").click(function() {
        url = $(this).attr("src");

        funcNum = GetUrlParam('CKEditorFuncNum');

        window.top.opener.CKEDITOR.tools.callFunction(funcNum, url);

//	window.top.opener.SetUrl( encodeURI( fileUrl ).replace( '#', '%23' ) ) ;

        window.top.close();
        window.top.opener.focus()
    });

</script>