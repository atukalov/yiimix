<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?php echo $this->pageTitle ?></title>

        <link href="/css/ant/bootstrap.min.css" rel="stylesheet">
        <link href="/css/ant/animate.css" rel="stylesheet">
        <link href="/css/ant/style.css" rel="stylesheet">
        <link href="/css/ant/font-awesome/font-awesome.css" rel="stylesheet">

    </head>

    <body class="gray-bg">

        <? echo $content ?>


        <script async=""src="/js/ant/jquery-2.1.1.js"></script>
        <script async=""src="/js/ant/bootstrap.min.js"></script>

    </body>

</html>