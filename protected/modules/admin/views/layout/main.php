<?php
/*
 * Yiimix
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
?>
<!DOCTYPE html>

<html lang="<?= Yii::app()->language ?>">
    <head>
        <title><?php echo $this->pageTitle ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="Content-language" content ="<?= Yii::app()->language ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
 


        <?php
        Yii::app()->getModule("admin")->registerAssets();
        $au=Yii::app()->getModule("admin")->getAssetsUrl();
        
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript("jquery");
        
       $cs->registerCssFile($au."/bootstrap.min-3.0.css");
       $cs->registerCssFile($au."/main-3.0.css");
       $cs->registerCssFile($au."/plugins-3.0.css");
       $cs->registerCssFile($au."/themes-3.0.css");
       $cs->registerCssFile($au."/themes/modern.css");
      
        
        
        
        $cs->registerScriptFile($au."/js/bootstrap.min-3.0.js");
        $cs->registerScriptFile($au."/js/plugins-3.0.js");
        $cs->registerScriptFile($au."/js/app-3.0.js");
        $cs->registerScriptFile($au."/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js");
        
        // $cs->registerCssFile("");
        ?>

    </head>
    <body>

        <div id="page-wrapper">

            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>yiimix</strong> control panel</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
                </div>
            </div>
        </div>

        <div id="page-container" class="sidebar-partial  sidebar-no-animations sidebar-visible-lg">
            <?php $this->widget("newMainMenu") ?>


            <div id="main-container">

                <header class="navbar navbar-default">
                    <ul class="nav navbar-nav-custom">
                        <li>
                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');
                                    this.blur();">
                                <i class="fa fa-bars fa-fw"></i>
                            </a>
                        </li>


                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="gi gi-settings"></i>
                            </a>
                            <ul class="dropdown-menu text-left">
                                <li class=""><a href="/admin/default/dropcache?url=<?= Yii::app()->request->requestUri ?>"><i class="gi gi-bin"></i> <?=Yii::t('admin','Drop cache')?></a></li>
                                <li class=""><a href="/admin/service/sitemap"><i class="fa fa-sitemap"></i> <?=Yii::t('admin','Sitemap generator')?></a></li>
                                <li class="divider"></li>
                                <li class=""><a href="/"><i class="gi gi-settings"></i> <?=Yii::t('admin','Files manager')?></a></li>
                            </ul>
                        </li>
                    </ul>

                    <!--  <ul class="nav navbar-nav-custom pull-right">
                          <li>
                              <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');
                                      this.blur();">
                                  <i class="gi gi-share_alt"></i>
                                  <span class="label label-primary label-indicator animation-floating">4</span>
                              </a>
                          </li>
  
                      </ul>-->
                </header>



                <div id="page-content" style="min-height: 646px;">

                    <!--    <div id="sidebar-alt">
                          <div id="sidebar-alt-scroll">
                              <div class="sidebar-content">
                                  <a href="/admin" class="sidebar-title">
                                      <i class="gi gi-comments pull-right"></i> <strong>Chat</strong>UI
                                  </a>
                                  <ul class="chat-users clearfix">
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-online">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-online">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-online">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-online">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-away">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-away">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" class="chat-user-busy">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)">
                                              <span></span>
                                              <img src="" alt="avatar" class="img-circle">
                                          </a>
                                      </li>
                                  </ul>
                                  <div class="chat-talk display-none">
                                      <div class="chat-talk-info sidebar-section">
                                          <button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
                                              <i class="fa fa-times"></i>
                                          </button>
                                          <img src="" alt="avatar" class="img-circle pull-left">
                                          <strong>John</strong> Doe
                                      </div>
                                      <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 210px;">
                                          <ul class="chat-talk-messages" style="overflow: hidden; width: auto; height: 210px;">
                                              <li class="text-center"><small>Yesterday, 18:35</small></li>
                                              <li class="chat-talk-msg animation-slideRight">Hey admin?</li>
                                              <li class="chat-talk-msg animation-slideRight">How are you?</li>
                                              <li class="text-center"><small>Today, 7:10</small></li>
                                              <li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
                                          </ul><div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; left: 1px; background: rgb(255, 255, 255);"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; left: 1px; background: rgb(51, 51, 51);"></div></div>
                                      <form action="index.php" method="post" id="sidebar-chat-form" class="chat-form">
                                          <input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
                                      </form>
                                  </div>
                                  <a href="javascript:void(0)" class="sidebar-title">
                                      <i class="fa fa-globe pull-right"></i> <strong>Activity</strong>UI
                                  </a>
                                  <div class="sidebar-section">
                                      <div class="alert alert-danger alert-alt">
                                          <small>just now</small><br>
                                          <i class="fa fa-thumbs-up fa-fw"></i> Upgraded to Pro plan
                                      </div>
                                      <div class="alert alert-info alert-alt">
                                          <small>2 hours ago</small><br>
                                          <i class="gi gi-coins fa-fw"></i> You had a new sale!
                                      </div>
                                      <div class="alert alert-success alert-alt">
                                          <small>3 hours ago</small><br>
                                          <i class="fa fa-plus fa-fw"></i> <a href="page_ready_user_profile.php"><strong>John Doe</strong></a> would like to become friends!<br>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Accept</a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Ignore</a>
                                      </div>
                                      <div class="alert alert-warning alert-alt">
                                          <small>2 days ago</small><br>
                                          Running low on space<br><strong>18GB in use</strong> 2GB left<br>
                                          <a href="page_ready_pricing_tables.php" class="btn btn-xs btn-primary"><i class="fa fa-arrow-up"></i> Upgrade Plan</a>
                                      </div>
                                  </div>
                                  <a href="page_ready_inbox.php" class="sidebar-title">
                                      <i class="fa fa-envelope pull-right"></i> <strong>Messages</strong>UI (5)
                                  </a>
                                  <div class="sidebar-section">
                                      <div class="alert alert-alt">
                                          Debra Stanley<small class="pull-right">just now</small><br>
                                          <a href="page_ready_inbox_message.php"><strong>New Follower</strong></a>
                                      </div>
                                      <div class="alert alert-alt">
                                          Sarah Cole<small class="pull-right">2 min ago</small><br>
                                          <a href="page_ready_inbox_message.php"><strong>Your subscription was updated</strong></a>
                                      </div>
                                      <div class="alert alert-alt">
                                          Bryan Porter<small class="pull-right">10 min ago</small><br>
                                          <a href="page_ready_inbox_message.php"><strong>A great opportunity</strong></a>
                                      </div>
                                      <div class="alert alert-alt">
                                          Jose Duncan<small class="pull-right">30 min ago</small><br>
                                          <a href="page_ready_inbox_message.php"><strong>Account Activation</strong></a>
                                      </div>
                                      <div class="alert alert-alt">
                                          Henry Ellis<small class="pull-right">40 min ago</small><br>
                                          <a href="page_ready_inbox_message.php"><strong>You reached 10.000 Followers!</strong></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>-->


                    <?php
                    $v = explode("_", $this->action->id);
                    if (is_array($v) && isset($v[0])) {
                        $v = $v[0];
                    }

                    if (file_exists(Yii::getPathOfAlias("admin.views." . $this->id . "." . $v) . "/header.php")) {
                        $this->renderPartial("admin.views." . $this->id . "." . $v . ".header", array());
                    } else if (file_exists(Yii::getPathOfAlias("admin.views." . $this->id) . "/header.php")) {
                        $this->renderPartial("admin.views." . $this->id . ".header", array());
                    }

                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links' => $this->breadcrumbs,
                        "tagName" => "ul",
                        "encodeLabel" => false,
                        "homeLink" => "<li><a href=\"" . Yii::app()->createUrl("admin") . "\"><i class=\"fa fa-home\"></i></a></li>",
                        "separator" => "",
                        "activeLinkTemplate" => "<li><a href=\"{url}\">{label}</a></li>",
                        "inactiveLinkTemplate" => "<li>{label}</li>",
                        "htmlOptions" => array("class" => "breadcrumb breadcrumb-top")));
                    ?>


                    <?php
                    $modules = Yii::app()->controller->widgets;
                    //$modules = Config::getInterface(Yii::app()->controller);
                    //if(count())
//var_dump($modules);die;
                    if (isset($_GET["interface"]) && $_GET["interface"] == "config") {
                        $main_class = "row draggable-blocks ui-sortable";
                        $col_class = " column";
                        $add_block = "<div class=\"block drb\"></div>";
                    } else {
                        $main_class = "row";
                        $col_class = "";
                        $add_block = "";
                    }
                    $size = !empty($modules["right"]) ? 8 : 12;


                    echo CHtml::openTag("div", array("class" => $main_class));
                    echo CHtml::openTag("div", array("class" => "col-lg-" . $size . $col_class . " main"));
                    ?>
                    <div id="ajaxModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer"></div></div></div>
                    </div>
                    <?php
                    if (empty($modules["main"]))
                        echo $content;
                    else
                        foreach ($modules["main"] as $v) {

                            if (!$v instanceof Params)
                                echo $content;
                            else {
                                if (isset($_GET["interface"]) && $_GET["interface"] == "config")
                                    $widss = array("class" => "block", "data-index" => $v->id);
                                else
                                    $widget_class = array("class" => "block");

                                echo CHtml::openTag("div", $widget_class);
                                $this->widget($v->value);
                                echo CHtml::closeTag("div");
                            }
                        }

                    echo $add_block;
                    echo CHtml::closeTag("div");

                    echo CHtml::openTag("div", array("class" => "col-lg-4" . $col_class . " left"));
                    if (!empty($modules["right"]))
                        foreach ($modules["right"] as $v) {
                            if (isset($_GET["interface"]) && $_GET["interface"] == "config")
                                $widget_class = array("class" => "block", "data-index" => $v->id);
                            else
                                $widget_class = array("class" => "block");

                            echo CHtml::openTag("div", $widget_class);
                            $this->widget($v[0], $v[1]);
                            echo CHtml::closeTag("div");
                        }
                    echo $add_block;
                    echo CHtml::closeTag("div");
                    echo CHtml::closeTag("div");
                    ?>
                </div>

                <footer class="clearfix">
                    <div class="pull-right">
                        Crafted by <a href="http://tukalov.ru" target="_blank">Anatoly Tukalov</a>
                    </div>
                    <div class="pull-left">
                        <span id="year-copy">2014-15</span> © <a href="http://yiimix" target="_blank">Yiimix 0.1</a>
                    </div>
                </footer>
            </div>


        </div>




        <style>
            .drb{
                background: none;border:dashed 1px #ccc
            }
        </style>
        <script src="/js/uiDraggable.js"></script>
        <script>

                                function stopRKey(evt) {

                                    if (evt.srcElement.id == "search_string")
                                        return true;
                                    var evt = (evt) ? evt : ((event) ? event : null);
                                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                                    if ((evt.keyCode == 13) && (node.type == "text")) {
                                        return false;
                                    }
                                }

                                document.onkeypress = stopRKey;


                                function ajaxModalSubmit(el) {
                                    var up = el.data("id");


                                    //e.preventDefault();
                                    $.ajax({
                                        method: "POST",
                                        url: el.data("href"),
                                        data: $("#" + el.data("index")).serialize(),
                                        dataType: "html",
                                        success: function (a) {
                                            if (a == "ok") {
                                                $.fn.yiiGridView.update(up, {ajaxUpdate: up});

                                            } else {
                                                console.log(a);
                                            }
                                            $('#ajaxModal').modal('hide');
                                        },
                                        error: function (a) {
                                            console.log(a);
                                        }

                                    });

                                }

                                $(function () {

                                    $('[data-target="ajaxModal"]').on('click', function (e) {
                                        e.preventDefault();

                                        if ($(this).data("func"))
                                            eval($(this).data("func"));
                                        else
                                            $('#ajaxModal .modal-dialog').load($(this).attr('href') + ($(this).data('remote') != "" ? $(this).data('remote') : ""));

                                        $('#ajaxModal').modal();

                                        $(".modal-dialog").draggable({handle: ".modal-header"});


                                    });

                                    $('[data-target="ajax-form"]').on('click', function (e) {
                                        var up = $(this).data("update");
                                        e.preventDefault();
                                        $.ajax({
                                            method: "POST",
                                            url: $(this).data("href"),
                                            data: $("#" + $(this).data("index")).serialize(),
                                            dataType: "html",
                                            success: function (a) {
                                                if (a == "ok") {
                                                    $.fn.yiiGridView.update(up, {ajaxUpdate: up});
                                                    $('#ajaxModal').modal('hide');
                                                } else {
                                                    console.log(a);
                                                }
                                            }

                                        })

                                    });

                                    $('#ajaxModal').on("hide", function (e) {
                                        if (is_loading !== true) {
                                            e.preventDefault();
                                            return false
                                        }
                                    });

                                    UiDraggable.init();
                                });
        </script>


    </body>
</html>
