<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'orders-form',
    // 'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered", 'enctype' => 'multipart/form-data'),
        ));
?>

<div class="form">
    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'name', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'name', array("class" => "form-control")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phone', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'phone', array("class" => "form-control")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'email', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->emailField($model, 'email', array("class" => "form-control")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'from', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'from', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'to', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'to', array("class" => "form-control")); ?>
        </div>
    </div> 
    <div class="form-group">
        <?php echo $form->labelEx($model, 'volume', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'volume', array("class" => "form-control")); ?>
        </div>
    </div> 
    <div class="form-group">
        <?php echo $form->labelEx($model, 'date', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'date', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'comment', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textarea($model, 'comment', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'file', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'file', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'page_id', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'page_id', array("class" => "form-control")); ?>
        </div>
    </div>
     <div class="form-group">
        <?php echo $form->labelEx($model, 'grus_number', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'grus_number', array("class" => "form-control")); ?>
        </div>
    </div>
</div><!-- form -->

<?php $this->endWidget(); ?>

<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/ckeditor.js'); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/adapters/jquery.js'); ?>

<script>

    var gets = "?e=lists&id=<?= $model->id ?>";

    function formSend() {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
        $('#lists-form').submit();
    }


</script>
