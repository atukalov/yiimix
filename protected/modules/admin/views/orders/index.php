<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */





$this->breadcrumbs[] = 'Заказы';


$this->pageTitle = 'Заказы';
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-money text-info"></i>&nbsp;&nbsp;<b>Заказы</b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">



    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">

        <?php
        $this->widget('XGridView', array(
            'id' => 'pages-grid',
            'dataProvider' => $catalog->search(),
            'filter' => $catalog,
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable table-responsive",
            'columns' => array(
                //   'id',
                array(
                    'name' => 'kind',
                    'type' => 'raw',
                    'value' => 'Orders::getKind($data->id)'
                ),
                'name',
                'phone',
                'email',
                'from',
                'to',
               // 'volume',
                'date',
                array('class' => 'XButtonColumn',
                    'template' => '{update}&nbsp;{delete}',
                    'updateButtonUrl' => '"/admin/orders/update/".$data->id',
                    'deleteButtonUrl' => '"/admin/orders/delete/".$data->id')
            ),
        ))
        ?>

    </div>


</div>
<style>
    .grid-view{margin-bottom: 0}
    #pages-grid th:first-child,
    #pages-grid td:first-child{padding-left: 20px}
</style>

<script>

    $("#selector").on("click", function () {
    });

    function enableSort() {
        $('#ajaxModal .modal-footer').html("").append('<button type="button" onclick="getSorter()" class="btn btn-sm btn-default">Применить</button>');
        $('#ajaxModal .modal-title').html("").html("<i class=\"fa fa-sort text-info\"></i>&nbsp;&nbsp;Сортировка страниц");
        $('#ajaxModal .modal-body').html("").load("");

    }

    function sefDialog(entity, id) {
        $('#ajaxModal .modal-content').html("").load("<?= Yii::app()->createUrl("admin/sef/ajax/create", array()) ?>?target=" + entity + "&id=" + id);
        $('#ajaxModal').modal("show");
    }


    function setVisible(id) {
        $.ajax({
            url: "/admin/pages/ajax/onoff/" + id,
            dataType: "html",
            success: function (a) {
                $.fn.yiiGridView.update('pages-grid');
                return false;
            },
            error: function (a) {
                console.log(a)
            }
        })

    }

    function processFilter(id) {
        if (id.hasClass("btn-alt")) {
            id.removeClass("btn-alt");
            $(".main").removeClass("col-lg-12").addClass("col-lg-8");
            $(".left").show();
        } else {
            id.addClass("btn-alt");
            $(".main").removeClass("col-lg-8").addClass("col-lg-12");
            $(".left").hide();
        }

    }

</script>

