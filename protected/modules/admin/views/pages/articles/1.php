<?php

/*
 * Контроллер шаблона test
 * YiiMix 0.1
 */

class contactsAction extends CAction {

    public function run($id) {

        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));

        $page = Pages::model()->findByPk($id);

        // Регистрируем мета тэги
        $this->controller->pageTitle = $page->title;
        $cs = Yii::app()->getClientScript();
        $cs->registerMetaTag($page->keywords, 'keywords');
        $cs->registerMetaTag($page->description, 'description');

        // Отображаем страницу
        $this->controller->render('contacts/index', array(
            'model' => $model,
            'page' => $page,
        ));
    }

}
?>