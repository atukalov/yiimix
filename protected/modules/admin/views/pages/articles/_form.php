<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'article-form',
    'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered","enctype"=>"multipart/form-data"),
        ));
?>
<?php echo $form->hiddenField($model, 'id'); ?>

<div class="form">
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model, 'oldUrl'); ?>
    
    <fieldset>
        <legend class="collapsed" data-toggle="collapse" data-target="#demo"><i class="fa fa-angle-down"></i> <?= Yii::t('admin', 'Page Options') ?></legend>
        <div class="row collapse out" id="demo">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'active', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <label class="switch switch-danger"><?php echo $form->checkbox($model, 'active'); ?>  <span></span></label>         

                    </div>
                </div>        
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'action', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <?php echo $form->dropDownList($model, 'action', CHtml::listData(Actions::model()->findAll(), 'name', 'title'), array('data-placeholder' => 'Выберите шаблон', "tabindex" => -1, "class" => "select-chosen")); ?>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'parent', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <?php echo $form->textField($model, 'parent', array("class" => "form-control")); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'cdate', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <?php echo $form->textField($model, 'cdate', array("class" => "form-control")); ?>


                    </div>
                </div>
            </div>
        </div>

    </fieldset>

    <fieldset>
        <legend class="collapsed" data-toggle="collapse" data-target="#seo"><i class="fa fa-angle-down"></i> SEO</legend>
        <div class="row collapse out" id="seo">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'keywords', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'keywords', array("class" => "form-control")); ?>
                </div>
            </div>     

            <div class="form-group">
                <?php echo $form->labelEx($model, 'description', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'description', array("class" => "form-control")); ?>
                </div>
            </div>

        </div>
    </fieldset>

    <fieldset>
        <legend class="" data-toggle="collapse" data-target="#option"><i class="fa fa-angle-down"></i> <?= Yii::t('admin', 'Page Data') ?></legend>
        <div class="row collapse in " id="option">

            <div class="form-group">
                <?php echo $form->labelEx($model, 'title', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'title', array("class" => "form-control")); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'url', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'url', array("class" => "form-control")); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'text', array("class" => "col-md-6 control-label")); ?>            
                <div class="col-md-12">
                    <?php echo $form->textarea($model, 'text', array('class' => 'ckeditor')); ?>
                </div>
            </div>

            <?php $this->widget("admin.widgets.fields.formFields", array("model" => $model, "form" => $form)) ?>

        </div> 
    </fieldset>
</div><!-- form -->
<?php $this->endWidget(); ?>



<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/ckeditor.js'); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/adapters/jquery.js'); ?>

<style>
    .page{width:100%}
    .page td:first-child{width:180px}
</style>
<script>
    $(document).ready(function () {

        $('.chosen-container').dropdown();

        // $(".editor").ckeditor();
        $("#Pages_action").change(function () {

            var val = $(this).val();

<?php if ($model->id): ?>
                var id =<?= $model->id ?>;
<?php else: ?>
                var id = -1;
<?php endif; ?>

            var $editors = $("textarea.editor");
            if ($editors.length) {

                $editors.each(function () {

                    var editorID = $(this).attr("id");
                    var instance = CKEDITOR.instances[editorID];

                    if (instance) {
                        instance.destroy(true);
                    }

                    //CKEDITOR.replace(editorID);
                });

            }

            $.ajax({
                type: "post",
                url: "/admin/pages/articles/ajax",
                data: {action: val, num: id},
                dataType: "html",
            }).done(function (data) {
                $("div#add").html(data);
                $(".select-chosen").chosen();
                $(".editor").ckeditor();

            });


        });

    });

</script>
