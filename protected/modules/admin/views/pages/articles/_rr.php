<a href="<?= Yii::app()->createUrl("admin/pages/articles", array("id" => $data->id)) ?>">
    <?= $data->title ?></a>

<br><progress value="<?= $data->views ?>" max="<?= $max ?>"
              title="<?= Yii::t("app", "{n} просмотр|{n} просмотра|{n} просмотров", $data->views) ?>"></progress>
<br>
<?php if($data->active == 1): ?>
    <a onclick="setVisible(<?= $data->id ?>)" class="btn btn-alt btn-xs btn-default"><b class="text-success"><i class="fa fa-check"></i></b> Опубликован</a>
<?php else: ?>
    <a onclick="setVisible(<?= $data->id ?>)" class="btn btn-alt btn-xs btn-default"><b class="text-danger"><i class="fa fa-check"></i></b> Не активен</a>
<?php endif; ?>
<a href="<?= Yii::app()->createUrl("admin/pages/articles/update", array("id" => $data->id)) ?>" class="btn btn-alt btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
<a href="/<?= $data->url ?>" class="btn btn-alt btn-xs btn-warning" target="_blank"><i class="fa fa-eye"></i></a>
<a href="<?= Yii::app()->createUrl("admin/pages/articles/delete", array("id" => $data->id)) ?>" class="btn btn-alt btn-xs btn-danger delete"><i class="fa fa-trash"></i></a>

<?php
Yii::app()->clientScript->registerScript("adelete", "
jQuery(document).on('click','a.delete',function() {
	if(!confirm('Вы уверены, что хотите удалить данный элемент?')) return false;
	
        jQuery.ajax({
		type: 'POST',
		url: jQuery(this).attr('href'),
		success: function(data) {                       
			 $.fn.yiiGridView.update('pages-grid');			
		},
		error: function(XHR) {      
                      console.log(XHR);			 
		}
                });	
	return false;
});
", CClientScript::POS_READY);
