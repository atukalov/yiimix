<span><span class="label label-info">Current</span>&nbsp;&nbsp;&nbsp;/<?= $data->url ?></span><br>
<div class="pp">
    <?php
    if (!empty($data->rSef)):
        foreach ($data->rSef as $v):
            ?>
            <a onclick="popoverInit(<?= $v->id ?>)" id="pp<?= $v->id ?>"><i class="fa fa-trash-o text-danger"></i></a> <?= $v->url ?><br>
        <?php endforeach; ?>
    <?php endif; ?>

    <a onclick="sefDialog('pages',<?= $data->id ?>)" class="label label-default"><i class="fa fa-plus"></i> SEF</a>

</div>
<style> 
    .pp{color:#888; font-weight: normal}
    .popover.top>.arrow:after{border-top-color: #000}
    .popover{ background: #000;color:#fff; box-shadow: 0 2px 6px rgba(0,0,0,.8);opacity: 1}
    .popover .btn{color:#fff}
    .popover .btn-danger{background: red;border-color: #fff}
    .popover .btn-default{background: gray;border-color: #fff}
    .popover .btn-danger:hover{background: #a00;}
    .popover .btn-default:hover{background: #333}
    .popover-title{font-size: 1em; color:#aaa}
    .popover-content{padding: 0;}
    .popover-content .block-section {margin: 5px 0 15px 0}
</style> 
<?php
Yii::app()->clientScript->registerScript("popover-sef", "
    
    function popoverInit(id){   
    $('#pp'+id).popover({
        placement: 'top',
        title: 'Вы хотите удалить?',
        html: true,
        animation: true,
        trigger: 'manual',
        content: \"<div class='block-section text-center'><div class='btn-group'><a class='btn btn-sm btn-danger' onclick='popoverYes(\"+id+\")'>&nbsp;&nbsp;Да&nbsp;&nbsp;</a><a class='btn btn-sm btn-default' onclick='popoverNo(\"+id+\")'>&nbsp;Нет&nbsp;</a></div>\"
    });
    $('#pp'+id).popover('show');
    }

    function popoverYes(id){
        $.ajax({        
        type: 'post',
        url: '/admin/sef/delete/'+id,
        dataType: 'html',
        success: function(a){
            if(a=='ok')
                $.fn.yiiGridView.update('pages-grid');
                else
                alert(a)
        },
        error: function(a){
            alert(a);
            }
        });        
        $('#pp'+id).popover('hide');
       return  false;
    }
    function popoverNo(id){           
        $('#pp'+id).popover('hide');
    return  false;
    }
    
    ", CClientScript::POS_END);
?>
