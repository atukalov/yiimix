<?php
$this->breadcrumbs = array(
    "Статьи" => Yii::app()->createUrl("admin/pages/articles"),
    "Новая статья",
);

$this->pageTitle = "Новая статья";
?>

<div class="col-lg-8">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="formSend()"  class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a>
            </div>
            <h2><i class="fa fa-pencil text-info"></i> Новая статья</h2>
        </div>
        <?php
        $this->renderPartial('articles/_form', array('model' => $model));
        ?>
    </div>
</div>

<div class="col-lg-4">
    <div class="block">
        <div class="block-title" >
            <h2><i class="fa fa-camera-retro text-warning"></i>&nbsp;&nbsp;Прикрепленные файлы</h2>
        </div>
        <?php
        $this->widget("FileLibWidget", array("item" => $model->id, "entity" => "pages"));
        ?>
    </div>
</div>

<div class="col-lg-4">
    <div class="block">
        <div class="block-title" >
            <h2><i class="fa fa-files-o text-warning"></i>&nbsp;&nbsp;Закачать файлы</h2>
            <div class="block-options pull-right">
                <a onclick="filesUpload()" class="btn btn-sm btn-alt btn-primary" title="Закачать"><i class="fa fa-upload"></i></a>
            </div>
        </div>
        <?php
        $this->renderPartial("admin.views.filelib._upload", array("model" => new Filelib, "id" => $model->id, "update" => "filelibview"));
        ?>
    </div>
</div>
<script>
     var gets = "?e=pages&id=<?= $model->id ?>";
    
    function formSend() {

        var f = "article";
        var id =<?= $model->parent ?>;

        $.ajax({
            type: "POST",
            path: "/admin/pages/articles/create/" + id,
            data: formSerialize(f),
            success: function () {
                notify("saved");
                window.location.href = "/admin/pages/articles/<?= $model->parent ?>";
            },
            error: function (html) {
                notify(html);
            },
        });
    }

    function formSerialize(id) {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();

        return $("#" + id).serialize();
    }

    function notify(text) {

        if ($(".notify").length == 0) {
            $("body").append('<div class="notify">' + text + '</div>');
        } else {
            $(".notify").stop().html(text).css("opacity", "0");
        }

        $(".notify").css("opacity", 0);
        $(".notify").animate({
            "opacity": 1,
        }, 500,
                function () {
                    $(".notify").animate({
                        "opacity": 0,
                    }, 1000);
                });
    }


</script>