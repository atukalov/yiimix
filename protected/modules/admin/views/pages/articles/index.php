<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */


$des = $des1 = "";
if ($options['mode'] === 1)
    $des = "disabled";

if ($parent !== false)
    $des1 = "disabled";

function trimWhenMore($str, $size = 30) {
    if (mb_strlen($str, "UTF-8") <= $size)
        return $str;
    else
        return mb_substr($str, 0, $size, "UTF-8") . "...";
}

if (!$parent) {
    if ($model) {
        $this->breadcrumbs[Yii::t('admin', 'Pages')] = Yii::app()->createUrl("admin/pages/articles");
        $this->breadcrumbs[] = $model->title;
    } else
        $this->breadcrumbs = array(
            Yii::t('admin', 'Pages'),
        );
}
else {

    $this->breadcrumbs[Yii::t('admin', 'Pages')] = Yii::app()->createUrl("admin/pages/articles");

    foreach ($parents as $v) {
        if ($v->id != $model->id)
            $this->breadcrumbs[trimWhenMore($v->title)] = Yii::app()->createUrl("admin/pages/articles", array("id" => $v->id));
    }

    $this->breadcrumbs[] = $model->title;
}

$this->pageTitle = Yii::t('admin', 'Pages');
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Pages") ?></b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <?php
            if ($model) {
                $o = $model->parent == 0 ? array() : array("id" => $model->parent);
                ?>
                <a href="<?php echo Yii::app()->createUrl("admin/pages/articles", $o) ?>" class="btn btn-sm btn-alt btn-default"><i class="fa fa-chevron-up"></i></a>

            <?php } ?>
            <div class="btn-group" >
                <button <?= $des ?> class="btn btn-sm btn-success  dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-plus"></i> <?= Yii::t("admin", "Create") ?>&nbsp;
                    <span class="caret"></span>
                </button>

                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li class="dropdown-header"><span class="text-success text-uppercase">Выберите шаблон...</span></li>
                    <?php
                    if (!empty($actions))
                        foreach ($actions as $v) {
                            echo '<li><a href="' . Yii::app()->createUrl("admin/pages/articles/create", array("id" => $parent, "action" => $v->id)) . '">' . $v->title . '</a></li>';
                        }
                    ?>
                </ul>

            </div>

            <?php
            if (!$model) {
                $des = "disabled";
            }
            $res = $catalog->search();

            if (count($res->getData()) <= 1)
                $des = "disabled";
            ?>
            <button <?= $des ?> data-func="enableSort()" data-toggle="modal" data-target="ajaxModal" id="sort-modal" class="btn btn-sm btn-alt btn-warning"><i class="fa fa-sort"></i> <?= Yii::t("admin", "Sort") ?></button>


        </div>
        <div class="pull-right">
            <?php
            $this->widget("WSelector", array(
                'varName' => 'mode',
                'id' => 'mode',
                'value' => $options["mode"],
                'items' => array(
                    'linked' => array('title' =>  Yii::t("admin", "Tree mode"), 'value' => 0, 'icon' => 'fa fa-link text-info'),
                    'unlinked' => array('title' => Yii::t("admin", "List mode"), 'value' => 1, 'icon' => 'fa fa-unlink text-info'),
                ),
                'ajax' => array(
                    'success' => ' if($("#mode .active").attr("data-index")==1){$("#sort-modal,#dropdownMenu1").prop("disabled",true);}else{$("#sort-modal,#dropdownMenu1").prop("disabled",false);} $.fn.yiiGridView.update("pages-grid");',
                ),
                'options' => $des1,
            ));
            ?>   
        </div>

    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">

        <?php
        $this->widget('XGridView', array(
            'id' => 'pages-grid',
            'dataProvider' => $catalog->search(),
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable table-responsive",
            'columns' => array(
                array(
                    "class" => "DCustomColumn",
                    "data" => '$data',
                    "datas" => array("max" => $max),
                    "rf" => "articles/_rr",
                    "header" => "Заголовок",
                    "name" => "title",
                ),
                array(
                    "class" => 'DCustomColumn',
                    "data" => '$data',
                    "rf" => "articles/_sef",
                    "header" => "Url",
                    "name" => "url",
                ),
            //'position',
            ),
        ))
        ?>

    </div>
    <div class="panel-body" style="border-top: 1px solid #ddd; background: #fafafd">        

        <div class="col-lg-6 pull-right">
            <?php $this->widget('WModelSearch', array('model' => 'Pages')); ?>

        </div><!-- /input-group -->
    </div>

</div>
<style>
    .grid-view{margin-bottom: 0}
    #pages-grid th:first-child,
    #pages-grid td:first-child{padding-left: 20px}
</style>

<script>

    $("#selector").on("click", function () {
    });

    function enableSort() {
        $('#ajaxModal .modal-footer').html("").append('<button type="button" onclick="getSorter()" class="btn btn-sm btn-default">Применить</button>');
        $('#ajaxModal .modal-title').html("").html("<i class=\"fa fa-sort text-info\"></i>&nbsp;&nbsp;Сортировка страниц");
        $('#ajaxModal .modal-body').html("").load("<?= Yii::app()->createUrl("admin/pages/articles/sort", array("id" => $parent)) ?>");

    }

    function sefDialog(entity, id) {
        $('#ajaxModal .modal-content').html("").load("<?= Yii::app()->createUrl("admin/sef/ajax/create", array()) ?>?target=" + entity + "&id=" + id);
        $('#ajaxModal').modal("show");
    }


    function setVisible(id) {
        $.ajax({
            url: "/admin/pages/ajax/onoff/" + id,
            dataType: "html",
            success: function (a) {
                $.fn.yiiGridView.update('pages-grid');
                return false;
            },
            error: function (a) {
                console.log(a)
            }
        })

    }

    function processFilter(id) {
        if (id.hasClass("btn-alt")) {
            id.removeClass("btn-alt");
            $(".main").removeClass("col-lg-12").addClass("col-lg-8");
            $(".left").show();
        } else {
            id.addClass("btn-alt");
            $(".main").removeClass("col-lg-8").addClass("col-lg-12");
            $(".left").hide();
        }

    }

</script>

