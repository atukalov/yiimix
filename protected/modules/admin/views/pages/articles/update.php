<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */



$this->breadcrumbs = array(
    Yii::t('admin', 'Pages') => Yii::app()->createUrl("admin/pages/articles"),
);
mb_internal_encoding("UTF-8");
$ps = Pages::getParents($model->id);
if ($ps)
    foreach ($ps as $v) {
        if (mb_strlen($v->title) > 25)
            $text = mb_substr($v->title, 0, 25) . "...";
        else
            $text = $v->title;
        $this->breadcrumbs[$text] = Yii::app()->createUrl("admin/pages/articles", array("id" => $v->id));
    }
array_pop($this->breadcrumbs);
$this->breadcrumbs[] = Yii::t('admin', 'Update') . ": <strong>" . $model->title . "</strong>";

$this->pageTitle = $model->title;
?>

<div class="block">

    <div class="block-title">
        <div class="block-options pull-right">
            <a onclick="ajaxFormSend('article-form', '/admin/pages/articles/update/<?= $model->id ?>')" class="btn btn-sm btn-alt btn-primary enable-tooltip" title="<?= Yii::t('admin', 'Save') ?>"><i class="fa fa-save"></i></a>

            <div class="btn-group btn-group-sm">
                <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default dropdown-toggle enable-tooltip" data-toggle="dropdown" title="Options">
                    <span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                    <li>
                        <a href="javascript:void(0)" onclick="lockBlockPosition()"><i class="fa fa-lock pull-right"></i>Lock Block Position</a>
                        <a href="javascript:void(0)"><i class="fa fa-archive pull-right"></i>Save History Point</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <?php
                        /*  if(isset($_GET["interface"]) && $_GET["interface"] == "config")
                          {
                          $list = Config::getInterface($this, CONFIG::INTERFACE_SOURCELIST);
                          foreach($list as $v)
                          {
                          $widget = Yii::import($v->value);
                          $widget = new $widget();
                          echo $widget->asMenuLink($v->id);
                          }
                          }
                          else
                          {
                          $list = Config::getInterface($this, CONFIG::INTERFACE_MENU);
                          if(empty($list))
                          echo "<span style=\"padding-left:13px;color:#ccc\">All Widgets On Board!</li>";
                          else
                          foreach($list as $v)
                          {
                          $widget = Yii::import($v->value);
                          $widget = new $widget();
                          echo $widget->asMenuLink($v->id);
                          }
                          } */
                        ?>
                    </li>
                </ul>
            </div>


        </div>
        <h2 ><i class="fa fa-pencil text-info"></i>&nbsp;&nbsp;<?= Yii::t('admin', 'Update') ?>: <strong><?= mb_strlen($model->title) > 70 ? mb_substr($model->title, 0, 70) . "..." : $model->title ?></strong></h2>
    </div>
    <?php
    $this->renderPartial('articles/_form', array('model' => $model));
    ?>

</div>
<style>
    .btn-group{white-space: nowrap}
    .popover{width: 140px !Important;}
    .popover-title{font-weight:500; text-align: center; font-size: 14px}

</style>

<script>

    $(document).ready(function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            onConfirm: function () {

                fileDelete($(this).attr("data-index"));
            },
        });
    });

    var gets = "?e=pages&id=<?= $model->id ?>";




</script>