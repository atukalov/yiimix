
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'fields-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    "htmlOptions" => array("class" => "form-horizontal form-bordered"),
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->hiddenField($model, 'oldname'); ?>
<?php echo $form->hiddenField($model, 'oldtarget'); ?>

<div class="form" style="overflow: hidden">

    <?php echo $form->errorSummary($model); ?>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'target', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->dropDownList($model, 'target', Fields::getValues(), array("class" => "select-chosen")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'name', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'name', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'title', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'title', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php $types = array('DIGIT' => 'Число', 'STR' => 'Строка', 'TEXT' => 'Текст', 'DATE' => 'Дата/Время', 'BOOL' => 'Логическое'); ?>
        <?php echo $form->labelEx($model, 'type', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->dropDownList($model, 'type', $types, array("class" => "select-chosen")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php
        $inputs = array('text' => 'Простое поле ввода', 'area' => 'Редактор', 'editor' => 'Html редактор', 'file' => 'Файл', 'date' => 'Выбор даты', 'check' => 'Чекбокс', 'select' => 'Выпадающий список', 'custom' => 'Свой класс');
        ?>
        <?php echo $form->labelEx($model, 'input', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->dropDownList($model, 'input', $inputs, array("class" => "select-chosen")); ?>
        </div>

    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'all', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-1">
            <label class="switch switch-primary"><?php echo $form->checkbox($model, 'all'); ?>  <span></span></label>         

        </div>
        <div class="col-md-2" >
            <?php echo $form->labelEx($model, 'multi', array("class" => "col-md-2 control-label", "style" => "float:right; margin-right:20px")); ?>
        </div>
        <div class="col-md-1">
            <label class="switch switch-primary"><?php echo $form->checkbox($model, 'multi'); ?><span></span></label>        

        </div>
        <div class="col-md-2" >
            <?php echo $form->labelEx($model, 'req', array("class" => "col-md-2 control-label", "style" => "float:right; margin-right:20px")); ?>
        </div>
        <div class="col-md-1">
            <label class="switch switch-primary"><?php echo $form->checkbox($model, 'req'); ?>  <span></span></label>        

        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'data', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-5">
            <?php echo $form->textarea($model, 'data', array("class" => "form-control", 'style' => 'height:260px')); ?>
        </div>
        <div class="col-md-5">
            Применяется для получения вариантов выбора в поле. Может принимать SQL или JSON массив<br>
            <br><b>пример SQL:</b><br> <code style="color:darkorchid;font-size: 1.4em">sql: SELECT `field1`, `field2` FROM {{table}} WHERE ....</code><br>
            Обязательно указываем сначала кодовое слово "sql:" и пробел, не допускаются пробелы в начале строки. Используются первые два поля. Первое - ключ, второе - значение, ключ является результатом выбора.
            <br><br><b>пример JSON:</b><br>
            <code style="color:darkorchid;font-size: 1.4em">json: {"key":"value", "key1":"value1", ...}</code><br>
            Массив  ключ => значение, ключ является результатом.
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'custom', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-5">
            <?php echo $form->textField($model, 'custom', array("class" => "form-control")); ?>
        </div>
        <div class="col-md-5">Именование Класса обработчика в формате alias: "<span style="color:red">application.modules.admin.components.CCustomField</span>"</div>
    </div>

</div><!-- form -->
<?php $this->endWidget(); ?>



