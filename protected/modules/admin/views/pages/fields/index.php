<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle =  Yii::t('admin', 'Fields');
?>
<div class="col-lg-12">
    <div class="block">

        <div class="block-title">


            <div class="block-options pull-right">
                <a href="<?php echo Yii::app()->createUrl("admin/pages/fields/create") ?>" class="btn btn-sm btn-alt btn-success"><i class="fa fa-plus"></i></a>
            </div>
            <h2><strong><?= Yii::t('admin', 'Fields') ?></strong></h2>
        </div>
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $fields,
            'template' => '{items}{pager}',
            'cssFile' => '',
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
            'columns' => array(
                //  'id',
                'target',
                'title',
                'name',
                'type',
                'input',
                array('class' => 'boolColumn',
                    'name' => 'multi',
                ),
                array('class' => 'boolColumn',
                    'name' => 'all',
                ),
                array('class' => 'boolColumn',
                    'name' => 'req',
                ),
                //'data',
                array(// display a column with "view", "update" and "delete" buttons
                    'class' => 'CButtonColumn',
                    'template' => '{update}{delete}',
                    'updateButtonUrl' => '"/admin/pages/fields/update/".$data->id',
                    'deleteButtonUrl' => '"/admin/pages/fields/delete/".$data->id'
                ),
            ),
        ))
        ?>
    </div>
</div>