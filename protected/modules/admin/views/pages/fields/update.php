<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = "Новое поле";

$this->breadcrumbs = array(
    "Поля" => Yii::app()->createUrl("admin/pages/fields"),
   $this->pageTitle,
);

?>

<div class="col-lg-12">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="$('#fields-form').submit()"  class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a>
            </div>
            <h2><i class="fa fa-pencil text-info"></i> Новое поле</h2>
        </div>

        <?php $this->renderPartial('fields/_form', array('model' => $model)); ?>
    </div>
</div>