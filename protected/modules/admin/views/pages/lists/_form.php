<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'lists-form',
   // 'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered",'enctype' => 'multipart/form-data'),
    ));
echo $form->hiddenField($model, 'parent');
?>

<div class="form">
    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'name', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'name', array("class" => "form-control")); ?>
        </div>
    </div>

    <?php $this->widget("admin.widgets.fields.formFields", array("model" => $model, "form" => $form)) ?>


</div><!-- form -->

<?php $this->endWidget(); ?>

<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/ckeditor.js'); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/adapters/jquery.js'); ?>

<script>

    var gets = "?e=lists&id=<?= $model->id ?>";

    function formSend() {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
        $('#lists-form').submit();
    }


</script>
