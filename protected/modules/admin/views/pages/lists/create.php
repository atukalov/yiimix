<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = "Новый элеменит списка";


if ($model->parent == 0) {
    $this->breadcrumbs = array(
        "Блоки" => Yii::app()->createUrl("admin/pages/lists"),
        $this->pageTitle,
    );
} else {
    $r = Lists::model()->findByPk($model->parent);

    $this->breadcrumbs = array(
        "Блоки" => Yii::app()->createUrl("admin/pages/lists"),
        $r->name => Yii::app()->createUrl("admin/pages/lists", array("id" => $r->id)),
        $this->pageTitle,
    );
}
?>

<div class="col-lg-12">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="formSend()"  class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a>
                <?php if ($model->parent == 0): ?>
                    <a onclick="customFields()"  class="btn btn-sm btn-alt btn-warning"><i class="fa fa-navicon"></i></a>
                <?php endif; ?>
            </div>
            <h2><i class="fa fa-pencil text-info"></i> <?= $this->pageTitle ?></h2>
        </div>

        <?php $this->renderPartial('lists/_form', array('model' => $model)); ?>
    </div>
</div>

<?php
if ($model->parent == 0)
    $this->widget('admin.widgets.fields.fieldsForItem', array('model' => $model, 'entity' => 'lists'));
?>