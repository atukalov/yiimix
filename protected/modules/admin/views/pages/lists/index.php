<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

function trimWhenMore($str, $size = 30) {
    if (mb_strlen($str, "UTF-8") <= $size)
        return $str;
    else
        return mb_substr($str, 0, $size, "UTF-8") . "...";
}

$this->pageTitle = "Списки";

if (!$parent) {

    $this->breadcrumbs = array(
        "Блоки",
    );
} else {

    $p = Lists::model()->findByPk(intval($parent));
    $this->breadcrumbs["Блоки"] = Yii::app()->createUrl("admin/pages/lists");
    foreach ($parents as $v) {
        if ($v->id != $model->id)
            $this->breadcrumbs[trimWhenMore($v->name)] = Yii::app()->createUrl("admin/pages/lists", array("id" => $v->id));
    }

    $this->breadcrumbs[] = $p->name;
}
?>
<?php
$des = "";
if ($parent === 0)
    $des = "disabled";
?>

<div class="panel panel-default" id="rbac-panel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="gi gi-show_thumbnails text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Lists") ?></b></h3>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <?php
            if ($model) {
                $o = $model->parent == 0 ? array() : array("id" => $model->parent);
                ?>
                <a href="<?php echo Yii::app()->createUrl("admin/pages/lists", $o) ?>" class="btn btn-sm btn-alt btn-default"><i class="fa fa-chevron-up"></i></a>

            <?php } ?>

            <a href="<?php echo Yii::app()->createUrl("admin/pages/lists/create", array("id" => 0)) ?>" class="btn btn-sm btn-alt btn-success"><i class="fa fa-plus"></i> <?= Yii::t("admin", "Create Section") ?></a>
        </div>
        <div class="pull-right">
            <a <?= $des ?> href="<?php echo Yii::app()->createUrl("admin/pages/lists/create", array("id" => $parent)) ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= Yii::t("admin", "Create Item") ?></a>
            <a <?= $des ?>  data-func="enableSort()" data-toggle="modal" data-target="ajaxModal" id="sort-modal" class="btn btn-sm btn-alt btn-warning"><i class="fa fa-sort"></i> <?= Yii::t("admin", "Sort") ?></a>
        </div>
    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">

        <?php
        $c = array();

        if ($model) {
            $labels = $model->FieldsBehavior->getFieldsLabels();
            if (!empty($labels))
                foreach ($labels as $k => $v) {
                    $c[$k] = array('header' => $v, 'value' => '$data->' . $k);
                }
        }

        if ($parent == 0) {
            $c[] = array(
                'class' => 'XButtonColumn',
                'template' => '{items}&nbsp;&nbsp;&nbsp;&nbsp;{update} {delete}',
                'buttons' => array(
                    'items' => array(
                        'url' => '"/admin/pages/lists/".$data->id',
                        'imageUrl' => 'fa fa-arrow-right',
                        'label' => 'Элементы',
                        'options' => array('class' => 'update btn btn-xs btn-alt btn-primary')
                    )
                ),
                'updateButtonUrl' => '"/admin/pages/lists/update/".$data->id',
                'deleteButtonUrl' => '"/admin/pages/lists/delete/".$data->id'
            );
        } else {
            $c[] = array(
                'class' => 'XButtonColumn',
                'template' => '{update} {delete}',
                'updateButtonUrl' => '"/admin/pages/lists/update/".$data->id',
                'deleteButtonUrl' => '"/admin/pages/lists/delete/".$data->id'
            );
        }
        $this->widget('XGridView', array(
            'dataProvider' => $catalog,
            'cssFile' => false,
            'template' => '{items}{pager}',
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
            'columns' =>
            array_merge(array(
                'id',
                'name',
                    ), $c),
        ));
        ?>

    </div>
    <div class="panel-body " style="border-top: 1px solid #ddd; background: #fafafd">
        <a href="<?php echo Yii::app()->createUrl("admin/pages/fields") ?>" class="btn btn-sm  btn-primary"><i class="fa fa-list"></i> <?= Yii::t("admin", "Section Fields") ?></a>
    </div>

</div>
<div id="dialog"></div>
<script>

    function fileCopy() {

        alert($(this).attr("disabled"));
    }
    function recalc() {
        $(".but.popup").each(function () {
            var p = $(this).position();
            var id = $(this).attr("id");
            var w1 = $(this).width() + 26;
            $("#i" + id).css("min-width", (w1 - 7) + "px");
            var w = $("#i" + id).width();
            $("#i" + id).css("left", (p.left + w1 - w - 2) + "px").css("top", (p.top + 29) + "px");
        });
    }

    $(document).ready(function () {

        recalc();
        //$(".popupinner a").prop("disabled","disabled");

        $(".btn.delete").on("click", function () {
            return false;
        }

        $(".but.popup").click(function () {
        recalc();
        var p = false;
        var id = $(this).attr("id");
        if ($(this).hasClass("sel"))
            p = true;
        if (p) {
            $(this).removeClass("sel");
            $("#i" + id).hide();
        } else {
            $(this).addClass("sel");
            $("#i" + id).show();
        }


    });
    $(".grid-view td").die("click");
    $(".selectrow").change(function () {
        $(".selected").removeClass("selected");
        $("input.selectrow:checked").each(function () {
            $(this).parent().parent().addClass("selected");
        });
    });
    });
            function unselect() {
                $('.file-items').removeClass('selected');
                updateButtons();
            }
    function selectAll() {
        $('.file-items').addClass('selected');
        updateButtons();
    }

    function create_folder() {
        var dir = '<?= fileList::getPath() ?>';
        $.ajax({
            url: '/admin/pages/files/cfolder?f=' + dir,
            success: function (a) {

                $('#dialog').html(a).dialog();
            }
        });
        return false;
    }

    function delete_folder() {
        if ($('.file-items.selected').length > 0) {
            var dir = '<?= fileList::getPath() ?>';
            var f = '';
            $('.file-items.selected').each(function () {
                f = f + ';' + $('p', this).html();
            });
            f = f.replace(';', '');
            //alert($f);

            $.ajax({
                url: '/admin/pages/files/dfolder?dir=' + dir + '&f=' + f,
                success: function (a) {

                    $('#dialog').html(a).dialog();
                }
            });
        }
        return false;
    }

    function create_folder_process() {

        var data = $('form#cfolder').serialize();
        $.ajax({
            type: "POST",
            url: '/admin/pages/files/cfolder',
            data: data,
            success: function (a) {
                $('#dialog').hide();
                window.location.reload();
            }
            //dataType: dataType
        });
        return false;
    }

    function updateButtons() {
        if ($('.file-items.selected').length > 0)
            $('.tbuttons').removeClass('gray');
        else
            $('.tbuttons').addClass('gray');
    }
    $(window).ready(function () {

        $('.file-items').click(function () {
            if ($(this).hasClass('selected'))
                $(this).removeClass('selected');
            else
                $(this).addClass('selected');
            updateButtons();
        });
        $('.file-items').dblclick(function () {
            if ($(this).attr('data-index') != '') {

                window.location.href = $(this).attr('data-index');
            }
        });
    });

</script>
