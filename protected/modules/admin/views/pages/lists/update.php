<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

$this->pageTitle = "Редактирование: " . $model->name;

if ($model->parent == 0) {
    $this->breadcrumbs = array(
        "Блоки" => Yii::app()->createUrl("admin/pages/lists"),
        $model->name,
    );
} else {
    $r = Lists::model()->findByPk($model->parent);

    $this->breadcrumbs = array(
        "Блоки" => Yii::app()->createUrl("admin/pages/lists"),
        $r->name => Yii::app()->createUrl("admin/pages/lists", array("id" => $r->id)),
        $model->name,
    );
}
?>

<div class="col-lg-12">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="$('#lists-form').submit()"  class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a>
                <?php if ($model->parent == 0): ?>
                    <a onclick="customFields()"  class="btn btn-sm btn-alt btn-warning"><i class="fa fa-navicon"></i></a>
                <?php endif; ?>
                <a href="/admin/pages/lists<?= $model->parent != 0 ? "/" . $model->parent : "/" ?>"  class="btn btn-sm btn-alt btn-default"><i class="fa fa-angle-up"></i></a>
            </div>
            <h2><i class="fa fa-pencil text-info"></i> <?= $this->pageTitle ?></h2>
        </div>

        <?php $this->renderPartial('lists/_form', array('model' => $model, 'fields' => $fields)); ?>
    </div>
</div>

<?php
if ($model->parent == 0)
    $this->widget('admin.widgets.fields.fieldsForItem', array('model' => $model, 'entity' => 'lists'));
?>

<script>
    var gets = "?e=pages&id=0";
</script>