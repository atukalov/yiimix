<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>


<?php
//$bn = basename($model->file, '.php');

$ext=  explode(".", basename($model->fileName));
$bn=$ext[0];
?><div class="form" style="padding: 0"><?php

    $form = $this->beginWidget('CActiveForm', array(
    'id' => 'file-' . $bn,
    'enableAjaxValidation' => false,
    'action' => '/admin/pages/logic/save',
    ));

    echo ($form->hiddenField($model, 'file'));
    echo ($form->hiddenField($model, 'type'));
    echo ($form->hiddenField($model, 'action'));


    //echo ($form->labelEx($model, 'file') .'<br>');
    echo ($form->textarea($model, 'content', array('style' => 'width:100%','id' => 'file_' . $bn)) . '<br>');
    ?></div><?
$this->endWidget();






