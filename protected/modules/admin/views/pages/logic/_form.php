<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'template-create',
        'enableAjaxValidation' => false,
        "htmlOptions" => array("class" => "form-horizontal form-bordered"),
    ));
    ?>
    <?php echo $form->hiddenField($model, 'oldname') ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'title', array("class" => "col-sm-2 control-label")); ?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'title', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'name', array("class" => "col-sm-2 control-label")); ?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'name', array("class" => "form-control")); ?>
        </div>
    </div>

    <input type="submit" value="Create">
</div>


<?php $this->endWidget(); ?>



