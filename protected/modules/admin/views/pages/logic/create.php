<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */


$this->pageTitle = Yii::t('admin', 'Create template');

$this->breadcrumbs = array(
    Yii::t("admin", "Templates")=>'/admin/pages/logic/',
    $this->pageTitle
);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= $this->pageTitle ?></b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <a onclick="$('#template-create').submit();return false;" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= Yii::t("admin", "Save") ?></a>
        </div>

    </div>
    <div class="panel-body">
        <?php $this->renderPartial('logic/_form', array('model' => $model)); ?>
    </div>
</div>
