<?php
/*
 * Yiimix Code Editor
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = Yii::t('app', 'Template') . ": " . $action->title;

$this->breadcrumbs = array(
    Yii::t("admin", "Templates") => '/admin/pages/logic',
    $action->title,
);
?>

<div class="block">
    <div class="block-title" style="margin-bottom: 0;border-bottom: none">
        <div class="block-options pull-right">
            <a href="/admin/pages/logic/update/<?=$action->id?>" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> <?=Yii::t('admin','Edit')?></a>
            <a href="#modal-new" class="btn btn-sm btn-alt btn-success" data-toggle="modal"><i class="fa fa-plus"></i></a>
            <a onclick="ajaxCurrentFormSend()" class="btn btn-sm btn-alt btn-primary" style="margin-left: 10px">
                <i class="fa fa-save"></i>
            </a>
        </div>
        <h3><i class="fa fa-code text-primary"></i> <?= Yii::t('app', 'Code Edit') ?>: <strong><?= $action->title ?></strong></h3>
      
    </div>

    <?php
    $fs = Yii::app()->params['editAria_fontSize'];
    $ft = Yii::app()->params['editAria_fontFamily'];

    $this->beginWidget('system.web.widgets.CClipWidget', array('id' => 'Controller: ' . $action->name . 'Action.php'));

    $ff = new LogicFile();
    $ff->type = 0;
    $ff->action = $action->name;
    $ff->load();
    $bn = basename($ff->fileName, '.php');
   
    Yii::app()->clientScript->registerScript('file_' . $bn, 'editAreaLoader.init({id: "file_' . $bn . '",cursor_position: "begin",font_size: ' . $fs . ',font_family: "' . $ft . '",start_highlight: true,allow_resize: "both",allow_toggle: false,word_wrap: false,language: "ru",syntax: "php",min_height: 600});');
    $this->renderPartial('logic/_edit', array('model' => $ff));
    $this->endWidget();

    foreach ($views as $v) {
        if ($v != '.' && $v != '..') {
            $fn = explode('.', $v);
            $this->beginWidget('system.web.widgets.CClipWidget', array('id' => ($v == 'index.php' ? 'View: ' : '') . $v));

            $ff = new LogicFile();
            $ff->type = 1;           
            $ff->file = $v;
            $ff->action = $action->name;
            $ff->load();           

            $ext = explode('.', $ff->file);
            $bn = $ext[0];
           
            Yii::app()->clientScript->registerScript('file_' . $bn, 'editAreaLoader.init({id: "file_' . $bn . '",cursor_position: "begin",font_size: ' . $fs . ',font_family: "' . $ft . '",start_highlight: true,allow_resize: "both",allow_toggle: false,word_wrap: false,language: "ru",syntax: "' . $ext[1] . '",min_height: 600});');
            $this->renderPartial('logic/_edit', array('model' => $ff));
            $this->endWidget();
        }
    }

    $tabParameters = array();
    foreach ($this->clips as $key => $clip)
        $tabParameters['tab' . (count($tabParameters) + 1)] = array('title' => $key, 'content' => $clip);

    $this->widget('application.components.yiimix.widgets.CTabView', array('tabs' => $tabParameters,
        'cssFile' => ''));
    ?>

</div>

<div id="modal-new" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title">Modal Title</h3>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= Yii::t('admin', 'Close') ?></button>
                <button type="button" class="btn btn-sm btn-primary" onclick="saveNewForm()"><?= Yii::t('admin', 'Create') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('/js/edit_area/edit_area_full.js', CClientScript::POS_END);
$cs->registerScript('formandeditarea', ' var migTI = false;
                function pauseA() {
                    if ($(".view.active").length == 0) {
                        var pel = "#" + $(".view:first").attr("id");
                    } else {
                        var pel = "#" + $(".view.active").attr("id");
                    }
                    var p = $(".yiiTab a[href=\'" + pel + "\']").html().split("> ")[1];
                    clearInterval(migTI);

                    $(".yiiTab a[href=\'" + pel + "\']").html(p);
                }
                function mig() {
                    if ($(".view.active").length == 0) {
                        var pel = "#" + $(".view:first").attr("id");
                    } else {
                        var pel = "#" + $(".view.active").attr("id");
                    }
                    if ($(".yiiTab a[href=\'" + pel + "\'] i").hasClass("text-danger")) {
                        $(".yiiTab a[href=\'" + pel + "\'] i").removeClass("text-danger");
                        $(".yiiTab a[href=\'" + pel + "\'] i").addClass("text-white");


                    } else {
                        $(".yiiTab a[href=\'" + pel + "\'] i").addClass("text-danger");
                        $(".yiiTab a[href=\'" + pel + "\'] i").removeClass("text-white");
                    }
                }

                function ajaxCurrentFormSend() {

                    var eaID = "frame_file_indexAction";

                    if ($(".view.active").length == 0) {
                        var pel = $(".view:first").attr("id");
                        var el = $(".view:first form");
                    } else {
                        var pel = $(".view.active").attr("id");
                        var el = $(".view.active form");
                    }
                    var eaID= $("textarea[name=\'LogicFile[content]\']", el).attr("id");

                    var p = $(".yiiTab a[href=\'#" + pel + "\']").text();
                    $(".yiiTab a[href=\'#" + pel + "\']").html("<i class=\"fa fa-save text-danger\"></i> " + p);
                    migTI = setInterval(mig, 100);


                    $("#"+eaID, el).val(editAreaLoader.getValue(eaID));

                    $.ajax({
                        url: "/admin/pages/logic/save",
                        method: "POST",
                        data: el.serialize(),
                        dataType: "html",
                        success: function (a) {
                            setTimeout(pauseA, 500);
                             console.log(a);

                        },
                        error: function (a) {
                            console.log(a);
                        }
                    });

                }', CClientScript::POS_END);
?>
<script>

    function saveNewForm() {

        $.ajax({
            url: "/admin/pages/logic/new/<?= $action->name ?>",
            method: "POST",
            data: $("#new-file").serialize(),
            dataType: "html",
            success: function (a) {

            },
            error: function (a) {
                console.log(a);
            }
        });
        $("#modal-new").hide();
        window.location.reload();
    }

    $("#modal-new").on("show.bs.modal", function () {
        $(".modal-body", this).load("/admin/pages/logic/new/<?= $action->name ?>");



    });
</script>