<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */


$this->breadcrumbs = array(
    Yii::t("admin", "Templates"),
);
$this->pageTitle = Yii::t("admin", "Templates");
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Templates") ?></b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <a href="<?php echo Yii::app()->createUrl("admin/pages/logic/create") ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= Yii::t("admin", "Create") ?></a>
        </div>

    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">

        <?php
        $this->widget('XGridView', array(
            'dataProvider' => Actions::model()->search(),
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
            'columns' => array(
              
                 array(
                    'class' => 'CDataColumn',
                    //'labelExpression' => '$data->name',
                    //'urlExpression' => '"/admin/pages/logic/edit/".$data->id',
                     'value'=>'$data->title." [".$data->name."]"',
                    'name' => 'title'
                ),
              
                array(// display a column with "view", "update" and "delete" buttons
                    'class' => 'XButtonColumn',
                    'template' => '{code}{update}{delete}',
                    'buttons' => array(
                        'code'=>array('url' => '"/admin/pages/logic/edit/".$data->id', 'imageUrl'=>'fa fa-code','options'=>array('class'=>'btn btn-xs btn-primary')),
                        'update' => array('url' => '"/admin/pages/logic/update/".$data->id'),
                        'delete' => array('url' => '"/admin/pages/logic/delete/".$data->id'),
                    )
                ),
            ),
        ));
        ?>
    </div>
</div>

<style>
   
    
</style>