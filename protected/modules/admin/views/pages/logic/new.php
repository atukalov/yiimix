<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new-file',
    'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered"),
        ));
?>


<div class="form">
    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model, 'type'); ?>
    <?php echo $form->hiddenField($model, 'action'); ?>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'file', array("class" => "col-md-3 control-label")); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'file', array("class" => "form-control")); ?>
        </div>
    </div>    
    <?php $this->endWidget(); ?>
