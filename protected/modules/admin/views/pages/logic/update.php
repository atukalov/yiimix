<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */


$this->pageTitle = Yii::t('admin', 'Update template');

$this->breadcrumbs = array(
    Yii::t("admin", "Templates") => '/admin/pages/logic/',
    $this->pageTitle
);
?>
<div class="col-md-6">
    <div class="block">
        <div class="block-title">
            <h3><i class="fa fa-edit text-primary"></i> <?= $this->pageTitle ?>: <strong><?= $model->title ?></strong></h3>            
            <div class="block-options pull-right">
                <a href="/admin/pages/logic/edit/<?= $model->id ?>" class="btn btn-default btn-sm"><i class="fa fa-code"></i> <?= Yii::t('admin', 'Code') ?></a>


                <a href="<?php echo Yii::app()->createUrl("admin/pages/logic/create") ?>" class="btn btn-sm btn-alt btn-primary" style="margin-left: 10px"><i class="fa fa-save"></i></a></div>
        </div>
        <?php $this->renderPartial('logic/_form', array('model' => $model, 'fields' => $fields)) ?>

    </div>
</div>


<?php if (isset($fields)): ?>
    <div class="col-md-6">
        <div class="block">
            <div class="block-title">           
                <div class="block-options pull-right">  <a href="<?php echo Yii::app()->createUrl("admin/pages/fields/create") ?>" class="btn btn-sm btn-alt btn-success"><i class="fa fa-plus"></i></a></div>
                <h3><i class="fa fa-list text-primary"></i> <?= Yii::t('admin', 'Custom Fields') ?></h3>
            </div>

            <?php
            $this->widget('XGridView', array(
                'dataProvider' => $fields,
                'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
                'template' => '{items}{pager}',
                'cssFile' => '',
                'columns' => array(
                    'title',
                    'name',
                    'type',
                    array('class' => 'boolColumn',
                        'name' => 'multi',
                    ),
                    array('class' => 'boolColumn',
                        'name' => 'all',
                    ),
                    array('class' => 'boolColumn',
                        'name' => 'req',
                    ),
                    array(
                        'class' => 'XCheckBoxColumn',
                        'selectableRows' => 2,
                        'name' => 'id',
                        'checked' => 'Fields::isChecked(' . $model->id . ',"pages",$data->id)',
                        'checkBoxHtmlOptions' => array('class' => 'select'),
                        'showSelectAll' => false
                    ),
                ),
            ))
            ?>
        </div>
    </div>
<?php endif; ?>


<script>

    $('.select').change(function () {
        var val = 0;
        if ($(this).is(':checked'))
            val = 1;

        $.ajax({
            type: "POST",
            url: "/admin/pages/fields/ajax",
            data: {"F[action]":<?= $model->id ?>, "F[target]": "pages", "F[field]": $(this).val(), "F[val]": val},
            dataType: "html",
        }).done(function (da) {
        });


    });

</script>