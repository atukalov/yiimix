<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>



<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'menu',
    'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered"),
        ));
?>
<div class="form">

    <?php echo $form->errorSummary($model); ?>


    <?php echo $form->hiddenField($model, 'parent'); ?>    

    <div class="form-group">
        <?php echo $form->labelEx($model, 'name', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'name', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'url', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'url', array("class" => "form-control")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'pos', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'pos', array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'options', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'options', array("class" => "form-control")); ?>
        </div>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'query', array("class" => "col-md-2 control-label")); ?>
        <div class="col-md-10">
            <?php echo $form->textField($model, 'query', array("class" => "form-control")); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php
//Yii::app()->getClientScript()->registerCoreScript("jquery");
Yii::app()->getClientScript()->registerScriptFile("/js/bootstrap-select.js", CClientScript::POS_END);
//Yii::app()->getClientScript()->registerScript("selectpicker", "$('#mySelect').selectpicker();", CClientScript::POS_END);
?>
<script>
    $(document).ready(function () {
    });
</script>