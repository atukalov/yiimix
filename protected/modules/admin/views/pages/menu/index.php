<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->breadcrumbs = Menus::adminBreadcrumbs($model->id);
?>
<div class="col-md-8 col-lg-9">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Menu") ?></b></h2>
        </div>
        <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
            <div class="pull-left">
                <a href="<?php echo Yii::app()->createUrl("admin/pages/menu/create", array("id" => $model->id)) ?>" class="btn btn-sm  btn-success"><i class="fa fa-plus"></i> <?= Yii::t('admin', 'Create') ?>&nbsp;</a>
            </div>
            <div class="pull-right">
                <a data-func="enableSort()" data-toggle="modal" data-target="ajaxModal" id="sort-modal" class="btn btn-sm btn-alt btn-warning"><i class="fa fa-sort"></i> <?= Yii::t('admin', 'Sort') ?>&nbsp;</a>
            </div>
        </div> 
        <div class="panel-body" style="padding: 0;margin: 0;">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider' => $catalog,
                'cssFile' => false,
                'template' => '{items}{pager}',
                'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
                'columns' => array(
                    array(
                        'class' => 'CLinkColumn',
                        'labelExpression' => '$data->name',
                        'urlExpression' => '"/admin/pages/menu/".$data->id',
                        'header' => 'Название'
                    ),
                    'url',
                    'pos',
                    array(// display a column with "view", "update" and "delete" buttons
                        'class' => 'XButtonColumn',
                        'template' => '{update} {delete}',
                        'updateButtonUrl' => '"/admin/pages/menu/update/".$data->id',
                        'deleteButtonUrl' => '"/admin/pages/menu/delete/".$data->id'
                    ),
                )
            ))
            ?>
        </div>
    </div>
</div>
<div class="col-md-4 col-lg-3">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="createTop()" class="btn btn-sm btn-alt btn-success">+</a>

            </div>
            <h2><strong>Меню</strong></h2>
        </div>
        <ul class="nav nav-pills nav-stacked" style="padding-bottom: 20px" id="top-menu">

            <?php
            foreach ($tm as $k => $v) :
                $cl = $model->main_id == $v->main_id ? " class=\"active\"" : "";
                ?>            
                <li<?= $cl ?>>
                    <a href="/admin/pages/menu/<?= $v->id ?>"><?= $v->name ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>



</div>

<!-- Modal -->
<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ddd
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<style>

    .bactive{ background-color:rgb(217, 237, 247);padding: 10px 15px; border-radius: 4px; overflow: hidden}
    .bactive input{border: 1px solid rgb(187, 207, 217); border-radius: 4px;
                   box-shadow: inset 0 1px 3px rgba(0,0,0,.15); float: left; padding: 2px 4px}
    .bactive a{width: auto;float: right; padding: 6px 6px !important; margin: 0 !important}

</style>
<script>
    
 
    function enableSort() {
        $('#ajaxModal .modal-footer').html("").append('<button type="button" onclick="getSorter()" class="btn btn-sm btn-default">Применить</button>');
        $('#ajaxModal .modal-title').html("").html("<i class=\"fa fa-sort text-info\"></i>&nbsp;&nbsp;Сортировка меню");
        $('#ajaxModal .modal-body').html("").load("<?= Yii::app()->createUrl("admin/pages/menu/sort", array("id" => $parent)) ?>");

    }
    
    function createTop() {
        $("#top-menu").append('<li class="bactive"><input id="newmenu"><a onclick="topMenu()" class="label label-info animation-pulse">Добавить</a></li>');
        $("#newmenu").focus()
    }

    function topMenu() {
        if ($("#newmenu").val() == "") {
            alert("Поле не должно быть пустым!");
        } else {
            $.ajax({
                type: "POST",
                url: "/admin/pages/menu/create",
                data: "name=" + $("#newmenu").val() + "&sc=top",
                success: function (a) {
                    console.log(a);
                    window.location.href = a;
                },
                error: function (a) {
                    alert(a);
                }
            })

        }

    }

    $("#myModal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        //alert(1);
        $(e.target).find("#myModal .modal-body").load(link.attr("href"));
    });


</script>