<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->breadcrumbs = array_merge(Menus::adminBreadcrumbs($model->parent, true), array( $model->name));
?>

<div class="col-lg-12">
    <div class="block">
        <div class="block-title">
            <div class="block-options pull-right">
                <a onclick="formSend()"  class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a>
            </div>
            <h2><i class="fa fa-pencil text-info"></i>&nbsp;&nbsp;<?=$model->name ?></h2>
        </div>
        <?php $this->renderPartial('menu/_form', array('model' => $model)); ?>
    </div>
</div>

<script>
    function formSend() {

        var f = "menu";
        var id =<?= $model->parent ?>;

        $.ajax({
            type: "POST",
            path: "/admin/pages/articles/create/" + id,
            data: formSerialize(f),
            success: function () {
                notify("saved");
                window.location.href = "/admin/pages/menu/<?= $model->parent ?>";
            },
            error: function (html) {
                notify(html);
            },
        });
    }

    function formSerialize(id) {

        return $("#" + id).serialize();
    }

    function notify(text) {

        if ($(".notify").length == 0) {
            $("body").append('<div class="notify">' + text + '</div>');
        } else {
            $(".notify").stop().html(text).css("opacity", "0");
        }

        $(".notify").css("opacity", 0);
        $(".notify").animate({
            "opacity": 1,
        }, 500,
                function () {
                    $(".notify").animate({
                        "opacity": 0,
                    }, 1000);
                });
    }


</script>