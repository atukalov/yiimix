<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>



<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'shop-items-_form-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
        ));
?>



<?php echo $form->errorSummary($model); ?>


<?php echo $form->hiddenField($model, 'parent'); ?>
<div class="form">
    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'price'); ?>
        <?php echo $form->textField($model, 'price'); ?>
        <?php echo $form->error($model, 'price'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'metrix'); ?>
        <?php echo $form->textField($model, 'metrix'); ?>
        <?php echo $form->error($model, 'metrix'); ?>
    </div>
    <div class="row">
        <td align="center"><?php echo CHtml::submitButton('Сохранить'); ?>
        <p class="note">Поля отмеченные <span class="required">*</span> должны быть заполнены.</p>
        <?php echo $form->error($model, 'metrix'); ?>
    </div>

</div><!-- form -->



<?php $this->endWidget(); ?>


