<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = "Прайс-листы";
$this->buttonBar = array(
    array(
        "class" => "link",
        "text" => "Добавить",
        "url" => array("pages/price/create", "id"=>( isset($model->id) ? $model->id : 0)),
        "options" => array("class" => "but green"),
    ),
);

?>


<style>
    .dec{padding: 3px 3px;margin: 0;
         //font-size: .9em;
         border: 1px solid #abb;
         font-weight: 800;
         width: 100%;
         background: #ffd;
         border-radius: 2px;}
    .tright{text-align: right}
    .de{padding: 0 3px !important;
        width: 100px !important;}
    </style>
    <div class="page">
    <div class="inner">
        <?php
        if (!$model)
            $cols = array(
                'id',
                array(
                    'class' => 'CLinkColumn',
                    'labelExpression' => '$data->title',
                    'urlExpression' => '"/admin/pages/price/".$data->id',
                    'header' => 'Название'
                ),
                array(// display a column with "view", "update" and "delete" buttons
                    'class' => 'CButtonColumn',
                    'template' => '{update}{delete}',
                    'updateButtonUrl' => '"/admin/pages/price/update/".$data->id',
                    'deleteButtonUrl' => '"/admin/pages/price/delete/".$data->id'
                ),
            );
        else
            $cols = array(
                'id',
                'title',
                array(
                    'class' => 'DEditColumn',
                    'name' => 'price',
                    'link' => '/admin/pages/price/qsave',
                    'item_id' => '$data->id',
                    'htmlOptions' => array('style' => 'text-align: right')
                ),
                array(
                    'class' => 'DEditColumn',
                    'name' => 'metrix',
                    'link' => '/admin/pages/price/qsave',
                    'item_id' => '$data->id',
                    'htmlOptions' => array('style' => 'text-align: center')
                ),
                array(// display a column with "view", "update" and "delete" buttons
                    'class' => 'CButtonColumn',
                    'template' => '{update}{delete}',
                    'updateButtonUrl' => '"/admin/pages/price/update/".$data->id',
                    'deleteButtonUrl' => '"/admin/pages/price/delete/".$data->id'
                ),
            );

        $this->widget('zii.widgets.grid.CGridView', array(
            'enableHistory' => true,
            'id' => 'price',
            'cssFile' => false,
            'template' => '{items}{pager}',
            'dataProvider' => $cats,
            'columns' => $cols,
        ))
        ?>
    </div>