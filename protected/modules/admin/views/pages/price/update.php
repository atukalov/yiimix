<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = "Редактирование: ".$model->title;
$this->buttonBar = array(
    array(
        "class" => "link",
        "text" => "Добавить",
        "url" => array("pages/price/create"),
        "options" => array("class" => "but green"),
    ),
);


?>


        <?php $this->renderPartial('price/_form', array('model' => $model)); ?>
