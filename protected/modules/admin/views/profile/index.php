<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
?>
<div class="button-bar">
       <h1>Пользователи</h1>    
</div>


<div class="page">
    <div class="inner">


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users->search(),
    'template' => '{items}{pager}',
    'filter'=>$users,
    'columns' => array(
        'id',
        'username',
        'email',
        array(
            //'name'=>'lastname',
            'header'=>'Фамилия',
            'value'=>'$data->profile->last_name',
            ),
                array(
            //'name'=>'firstname',
            'header'=>'Имя',
            'value'=>'$data->profile->first_name',
            ),
        
       array(// display a column with "view", "update" and "delete" buttons
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonUrl' => '"/admin/shop/tovar/update?cat=".$data->id',
            'deleteButtonUrl' => '"/admin/shop/tovar/delete?cat=".$data->id'
        ),
    ),
))
?>
</div>
