<?php

/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
?>
<?=$mes?>

<form id="dfolder">
    <input type="hidden" name="Dfolder[dir]" value="<?=$dir?>">    
    <input type="hidden" name="Dfolder[files]" value="<?=$f?>">    
</form>

<? if($del){?>
<a onclick="delete_folder_process()">Удалить</a>
<? } ?>
<a onclick="$('#dialog').dialog('close')">Закрыть</a>

