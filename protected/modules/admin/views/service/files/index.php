<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */


$this->pageTitle = fileList::getPath();
$this->buttonBar = array(
    array(
        "class" => "popup",
        "text" => "Операции с файлами",
        "id" => "op",
        "items" => array(
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-scissors\"></span>&nbsp;&nbsp;Копировать",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-paste\"></span>&nbsp;&nbsp;Вставить",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "sep",
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-trash\"></span>&nbsp;&nbsp;Удалить",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "sep",
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-plus\"></span>&nbsp;&nbsp;Новая папка",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-upload\"></span>&nbsp;&nbsp;Закачать",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "sep",
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-save-file\"></span>&nbsp;&nbsp;В архив",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
            array(
                "class" => "link",
                "text" => "<span class=\"halflings halflings-open-file\"></span>&nbsp;&nbsp;Разархивировать",
                "url" => "javascript:void()",
                // "ajax" => "fileCopy()",
                "options" => array("class" => "red", "onclick" => "fileCopy()"),
            ),
        )
    ),
);
?>

<? Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/files.css'); ?>
<? Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<? Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>




<?
$this->widget('zii.widgets.grid.CGridView', array(
    'selectableRows' => 2,
    'dataProvider' => new CArrayDataProvider($catalog, array(
        //'id'=>'name',
        'keyField' => 'name',
        'sort' => array(
            'attributes' => array(
                'name',
            ),
        ),
        'pagination' => array(
            'pageSize' => 50,
        ))),
    'cssFile' => false,
    'template' => '{items}{pager}',
    'columns' => array(
        array('class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'id' => 'select-on-check',
            'checkBoxHtmlOptions' => array('class' => 'select-on-check'),
        // 'headerHtmlOptions' => array('class' => 'select-on-check-all'),
        ),
        array(
            'class' => 'CDataColumn',
            'name' => 'ico',
            'type' => 'raw',
            'header' => 'Файлы',
            'value' => '$data["ico"]."&nbsp;&nbsp;".$data["name"]',
        ),
)));
?> 


<div id="dialog"></div>
<script>

function fileCopy(){
         
        alert($(this).attr("disabled"));
    }
    function recalc() {
        $(".but.popup").each(function () {
            var p = $(this).position();
            var id = $(this).attr("id");
            var w1 = $(this).width() + 26;
            $("#i" + id).css("min-width", (w1 - 7) + "px");
            var w = $("#i" + id).width();

            $("#i" + id).css("left", (p.left + w1 - w - 2) + "px").css("top", (p.top + 29) + "px");

        });

    }

    $(document).ready(function () {

        recalc();

  //$(".popupinner a").prop("disabled","disabled");
  
        $(".but.popup").click(function () {
            recalc();
            var p = false;
            var id = $(this).attr("id");

            if ($(this).hasClass("sel"))
                p = true;

            if (p) {
                $(this).removeClass("sel");
                $("#i" + id).hide();
            } else {
                $(this).addClass("sel");
                $("#i" + id).show();

            }


        });

        $(".grid-view td").die("click");

        $(".selectrow").change(function () {
            $(".selected").removeClass("selected");

            $("input.selectrow:checked").each(function () {
                $(this).parent().parent().addClass("selected");


            });

        });

    });

    function unselect() {
        $('.file-items').removeClass('selected');
        updateButtons();
    }
    function selectAll() {
        $('.file-items').addClass('selected');
        updateButtons();
    }

    function create_folder() {
        var dir = '<?= fileList::getPath() ?>';

        $.ajax({
            url: '/admin/pages/files/cfolder?f=' + dir,
            success: function (a) {

                $('#dialog').html(a).dialog();
            }
        });
        return false;
    }

    function delete_folder() {
        if ($('.file-items.selected').length > 0) {
            var dir = '<?= fileList::getPath() ?>';
            var f = '';
            $('.file-items.selected').each(function () {
                f = f + ';' + $('p', this).html();
            });
            f = f.replace(';', '');
            //alert($f);

            $.ajax({
                url: '/admin/pages/files/dfolder?dir=' + dir + '&f=' + f,
                success: function (a) {

                    $('#dialog').html(a).dialog();
                }
            });
        }
        return false;
    }

    function create_folder_process() {

        var data = $('form#cfolder').serialize();
        $.ajax({
            type: "POST",
            url: '/admin/pages/files/cfolder',
            data: data,
            success: function (a) {
                $('#dialog').hide();
                window.location.reload();
            }
            //dataType: dataType
        });
        return false;
    }

    function updateButtons() {
        if ($('.file-items.selected').length > 0)
            $('.tbuttons').removeClass('gray');
        else
            $('.tbuttons').addClass('gray');

    }
    $(window).ready(function () {

        $('.file-items').click(function () {
            if ($(this).hasClass('selected'))
                $(this).removeClass('selected');
            else
                $(this).addClass('selected');
            updateButtons();
        });

        $('.file-items').dblclick(function () {
            if ($(this).attr('data-index') != '') {

                window.location.href = $(this).attr('data-index');

            }
        });
    });

</script>
