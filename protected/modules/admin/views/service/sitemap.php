<?php
/*
 * Yiimix sitemap create view
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            <i class="fa fa-sitemap text-info"></i>&nbsp;&nbsp;<strong><?= Yii::t('admin', 'Sitemap generator') ?></strong>
        </div>
    </div>

    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left col-md-12" style="position: relative">
            <a onclick="process()" class="btn btn-lg  btn-success"> <?= Yii::t('admin', 'Create') ?>&nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>         
        </div>


    </div>
    <div class="panel-body">

        <?php if (Yii::app()->user->hasFlash('sitemap_succcess')): ?>
            <h1><?= Yii::t('admin', 'Sitemap.xml created successfully.') ?> </h1>
            <h2><?= Yii::t('admin', 'Way to add to the search engines') ?> <?= Yii::app()->getBaseUrl(true) ?>/sitemap.xml</h2><br><br><br>
            <a class="btn btn-success btn-lg" href="<?= Yii::app()->getBaseUrl(true) ?>/sitemap.xml" target="_blank"><?= Yii::t('admin', 'View') ?></a><br><br><br><br>
        <?php else: ?>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'sitemap-form',
                'enableAjaxValidation' => false,
                "htmlOptions" => array("class" => "form-horizontal form-bordered", "enctype" => "multipart/form-data"),
            ));
            ?>

            <div class="form">
                <?php echo $form->errorSummary($model); ?>


                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'extend', array("class" => "col-md-4 control-label")); ?>
                        <div class="col-md-8">
                            <label class="switch switch-danger"><?php echo $form->checkbox($model, 'extend', array('disabled' => 'disabled')); ?>  <span></span></label>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'media', array("class" => "col-md-4 control-label")); ?>
                        <div class="col-md-8">
                            <label class="switch switch-danger"><?php echo $form->checkbox($model, 'media', array('disabled' => 'disabled')); ?>  <span></span></label>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'split', array("class" => "col-md-4 control-label")); ?>
                        <div class="col-md-8">
                            <label class="switch switch-danger" id="length"><?php echo $form->checkbox($model, 'split', array('disabled' => 'disabled')); ?>  <span></span></label>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'length', array("class" => "col-md-4 control-label")); ?>
                        <div class="col-md-8">
                            <?php echo $form->textField($model, 'length', array("class" => "form-control", 'disabled' => 'disabled')); ?>
                        </div>
                    </div>

                </div>


            </div><!-- form -->
        </div>
        <?php $this->endWidget(); ?>

    <?php endif; ?>
    <div class="panel-body" style="border-top: 1px solid #ddd; background: #fafafd">
        <div class="pull-right">
            <a href="" class="btn btn-sm  btn-danger" disabled><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;<?= Yii::t('admin', 'Set as default') ?></a>
        </div>
    </div>
</div>
<script>

    function process() {


        var progressBar = $('#prb');

            $.ajax({
                url: "/admin/service/sitemap?process=1",
                type: 'GET', // можно и POST
                dataType:'html',
                success: function (a) {
                 
                 
                }
   
   });
   }


    $("input[name='SitemapModel[split]']").on("change", function () {

        if ($("input[name='SitemapModel[split]']").is(":checked"))
            $("input[name='SitemapModel[length]']").attr('disabled', false).focus();
        else {
            $("input[name='SitemapModel[length]']").attr('disabled', true);
        }
    })
</script>