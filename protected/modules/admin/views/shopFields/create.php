<h1>Создать свойство</h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shop-items-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>    
        	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'tipe'); ?>
		<?php echo $form->dropdownlist($model,'tipe',array('string'=>'Строка','digit'=>'Число','file'=>'Файл','date'=>'Дата','blob'=>'Текст','list'=>'Список')); ?>
		<?php echo $form->error($model,'tipe'); ?>
	</div>
                	<div class="row">
		<?php echo $form->labelEx($model,'all'); ?>
		<?php echo $form->checkbox($model,'all'); ?>
		<?php echo $form->error($model,'all'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
