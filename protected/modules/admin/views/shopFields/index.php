<?php

$this->widget('zii.widgets.grid.CGridView',array(
    'dataProvider'=>$dp,
        'columns'=>array(
            'title',
            array(
                'header'=>'Тип',
                'type'=>'raw',
                'value'=>'ShopFields::model()->types[$data->tipe]'),            
            'name',            
            array(            // display a column with "view", "update" and "delete" buttons
            'class'=>'CButtonColumn',
        ),
),
     
));

?>
<a href="/admin/shopfields/create">Create</a>
