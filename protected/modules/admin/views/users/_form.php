<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.user
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'article-form',
    'enableAjaxValidation' => false,
    "htmlOptions" => array("class" => "form-horizontal form-bordered"),
        ));
?>
<?php echo $form->hiddenField($model, 'id'); ?>

<div class="form">
    <?php echo $form->errorSummary($model); ?>   
            <div class="form-group">
                <?php echo $form->labelEx($model, 'username', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'username', array("class" => "form-control")); ?>
                </div>
            </div>     

            <div class="form-group">
                <?php echo $form->labelEx($model, 'password', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'password', array("class" => "form-control")); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'email', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'email', array("class" => "form-control")); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'activkey', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'activkey', array("class" => "form-control")); ?>
                </div>
            </div>


            <div class="col-md-6">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'superuser', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <label class="switch switch-danger"><?php echo $form->checkbox($model, 'superuser'); ?>  <span></span></label>         

                    </div>
                </div>        
               
            </div>
             <div class="col-md-6">
             <div class="form-group">
                    <?php echo $form->labelEx($model, 'status', array("class" => "col-md-4 control-label")); ?>
                    <div class="col-md-8">
                        <label class="switch switch-danger"><?php echo $form->checkbox($model, 'status'); ?>  <span></span></label>         

                    </div>
                </div> 
             </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'create_at', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'create_at', array("class" => "form-control")); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'lastvisit_at', array("class" => "col-md-2 control-label")); ?>
                <div class="col-md-10">
                    <?php echo $form->textField($model, 'lastvisit_at', array("class" => "form-control")); ?>
                </div>
            </div>
        </div>
 
            <?php $this->widget("admin.widgets.fields.formFields", array("model" => $model->profiles, "form" => $form)) ?>

      
</div><!-- form -->
<?php $this->endWidget(); ?>



<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/ckeditor.js'); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/ckeditor/adapters/jquery.js'); ?>

<style>
    .page{width:100%}
    .page td:first-child{width:180px}
</style>
<script>
    $(document).ready(function () {

        $('.chosen-container').dropdown();

        // $(".editor").ckeditor();
        $("#Pages_action").change(function () {

            var val = $(this).val();

<?php if ($model->id): ?>
                var id =<?= $model->id ?>;
<?php else: ?>
                var id = -1;
<?php endif; ?>

            var $editors = $("textarea.editor");
            if ($editors.length) {

                $editors.each(function () {

                    var editorID = $(this).attr("id");
                    var instance = CKEDITOR.instances[editorID];

                    if (instance) {
                        instance.destroy(true);
                    }

                    //CKEDITOR.replace(editorID);
                });

            }

            $.ajax({
                type: "post",
                url: "/admin/pages/articles/ajax",
                data: {action: val, num: id},
                dataType: "html",
            }).done(function (data) {
                $("div#add").html(data);
                $(".select-chosen").chosen();
                $(".editor").ckeditor();

            });


        });

    });

</script>
