<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */
/* Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0;")->execute();
  $sql=" TRUNCATE TABLE `AuthItem`";
  Yii::app()->db->createCommand($sql)->execute();
  $sql="TRUNCATE TABLE `AuthItemChild`";
  Yii::app()->db->createCommand($sql)->execute();
  $sql="TRUNCATE TABLE `AuthAssignment`";
  Yii::app()->db->createCommand($sql)->execute();
  Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1;")->execute();

 */




/* $bizRule='return Yii::app()->user->id==$params["post"]->authID;';
  $task=$auth->createTask('updateOwnPost','редактирование своей записи',$bizRule);
  $task->addChild('updatePost');
 */


$this->breadcrumbs = array(
    Yii::t("admin", "Users")
);



$this->pageTitle = Yii::t("admin", "Users");
/* $this->buttonBar = array(
  array(
  "class" => "link",
  "text" => "Новый пользователь",
  "url" => array("users/create"),
  "options" => array("class" => "but green"),
  ),
  ); */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="fa fa-book text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Users") ?></b></h2>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <a href="<?php echo Yii::app()->createUrl("admin/users/create") ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= Yii::t("admin", "Create") ?></a>
        </div>        
        <div class="pull-right">
            <a href="<?php echo Yii::app()->createUrl("admin/users/rbac") ?>" class="btn btn-sm btn-alt btn-danger"><i class="fa fa-unlock"></i> <?= Yii::t("admin", "RBAC") ?></a>
        </div>

    </div>
    <div class="panel-heading panel-second-heading">
        <?php $this->widget("XAlphabet",array('url'=>'/admin/users'));?>        
    </div>
    <div class="panel-body" style="padding: 0;margin: 0;">
        <?php
        $this->widget('XGridView', array(
            'dataProvider' => $users->search(),
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable",
            'columns' => array(
                'id',
                'username',
                'email',
                'status',
                array(
                    'class' => 'XButtonColumn',
                    'template' => '{update} {delete}',
                    'updateButtonUrl' => '"/admin/users/update/".$data->id',
                    'deleteButtonUrl' => '"/admin/users/delete/".$data->id'
                ),
            ),
        ));
        ?>
    </div>
    <div class="panel-body asfooter">
        <div class="col-lg-6 pull-right">
            <?php $this->widget('WModelSearch', array('model' => 'User')); ?>

        </div>
    </div>
</div>