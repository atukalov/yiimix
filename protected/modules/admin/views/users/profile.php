<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   Users Admin Profile Updater
 * @package    yiimix.users
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */

$this->pageTitle = Yii::t('admin', 'User profile');

$this->breadcrumbs = array(
    Yii::t("admin", "Users") => '/admin/users/',
    $this->pageTitle
);
?>
<div class="col-md-12">
    <div class="block">
        <div class="block-title">
            <h3><i class="fa fa-user text-primary"></i> <?= $this->pageTitle ?>: <strong><?= $model->username ?></strong></h3>            
            <div class="block-options pull-right">  <a href="<?php echo Yii::app()->createUrl("admin/pages/logic/create") ?>" class="btn btn-sm btn-alt btn-primary"><i class="fa fa-save"></i></a></div>
        </div>
<?php //$this->renderPartial('logic/_form', array('model' => $model, 'fields' => $fields)) ?>

    </div>
</div>

