<div class="pull-right" style="white-space: nowrap">
<a class="btn btn-default btn-alt btn-xs"><i class="fa fa-edit"></i></a>
    <a class="btn btn-default btn-alt btn-xs"><i class="fa fa-trash"></i></a>
    <span class="divider divider-xs"></span>
    <?php if($data->type==2):?>
    <a class="btn btn-default btn-alt btn-xs"><i class="fa fa-user"></i></a>
    <?php endif;?>
    <a class="btn btn-white btn-alt btn-xs"><i class="fa fa-arrow-circle-right"></i></a>
</div>

<style>
    .divider{display: inline-block}
    .divider-xs{width: 9px}
</style>