<div style="white-space: nowrap">
    <?php
    switch($data->type)
    {
        case 0:
            echo '<i class="fa fa-share-alt text-warning"></i>&nbsp;&nbsp;&nbsp;<b>'.$data->name.'</b>';
            break;
        case 1:
            echo '<i class="fa fa-share-alt-square text-warning"></i>&nbsp;&nbsp;&nbsp;<b>'.$data->name.'</b>';
            break;
        default:
            echo '<i class="fa fa-users text-warning"></i>&nbsp;&nbsp;&nbsp;<b>'.$data->name.'</b>';
    }
    ?>
</div>