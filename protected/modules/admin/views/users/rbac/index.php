<?php
$this->breadcrumbs = array(
     Yii::t("admin", "Users") => array("users"),
     Yii::t("admin", "RBAC"),
);
?>
<div class="panel panel-default" id="rbac-panel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-users text-info"></i>&nbsp;&nbsp;<?= Yii::t("admin", "RBAC") ?> – <b>Groups</b></h3>
    </div>
    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <a href="#" class="btn btn-sm  btn-success"><i class="fa fa-plus"></i> Create</a>

        </div>
        <div class="pull-right">Filters: 
            <div class="btn-group btn-group-sm" id="selector">
                <button class="btn btn-default btn-alt active" data-index="2"><i class="fa fa-users text-info"></i> Groups</button>
                <button class="btn btn-default btn-alt" data-index="0"><i class="fa fa-share-alt text-info"></i> Rules</button>
                <button class="btn btn-default btn-alt" data-index="1"><i class="fa fa-share-alt-square text-info"></i> Biz rules</button>
            </div>


        </div>

    </div>
     
    <div class="panel-body" style="padding: 0;margin: 0;">
        <?php
        $model = new AuthItem("search");
        if(isset($_GET["type"]) && $_GET["type"] != "")
            $model->type = intval($_GET["type"]);
        else
            $model->type = 2;

        $this->widget('XGridView', array(
            'dataProvider' => $model->search(),
            'id' => 'rbac-grid',
            'cssFile' => false,
            'template' => '{items}{pager}',
            'enableHistory' => true,
            'ajaxUpdate'=>true,
            'itemsCssClass' => "table table-striped table-hover",
            'htmlOptions' => array("class" => "col-md-6", "style" => "padding:0; border-right: 1px solid #ddd"),
            'columns' => array(
                array(
                    "class" => "DCustomColumn",
                    "data" => '$data',
                    "datas" => array(),
                    "rf" => "rbac/_name",
                    //  "header" => "Заголовок",
                    "name" => "name",
                ),
                'description',
                /*                array(
                  'class' => 'CButtonColumn',
                  'template' => '{update}{delete}',
                  ), */
                array(
                    "class" => "DCustomColumn",
                    "data" => '$data',
                    "datas" => array(),
                    "rf" => "rbac/_but",
                    "header" => "",
                    "name" => "",
                ),
            ),
        ));
        ?>

    </div>
    <div class="panel-body" style="border-top: 1px solid #ddd; background: #fafafd">
        <div class="pull-left">
            <button id="assign" href="#" class="btn btn-sm  btn-primary btn-alt"><i class="fa fa-magnet"></i>&nbsp;&nbsp;&nbsp;Assign rule</button>
            <button id="make" desibled class="disabled btn btn-sm  btn-default btn-alt"><i class="fa fa-automobile"></i>&nbsp;&nbsp;&nbsp;Make rule by controller</button>


        </div>
    </div>
     </div>
    <style>
        .page.selected a{background: rgb(70, 183, 191) !important;color:#fff}

    </style>
    <script>

        function toggleButton(id, on) {

            var but = $("button#" + id);
            if (on)
                but.removeAttr("disabled")
                        .removeClass("disabled")
                        .removeClass("btn-default")
                        .addClass("btn-primary");
            else
                but.attr("disabled", "disabled")
                        .addClass("disabled")
                        .addClass("btn-default")
                        .removeClass("btn-primary");
        }

        $("#selector button").on("click", function () {

            $("#selector button").removeClass("active");
            $(this).addClass("active");
            var i = $(this).data("index");
            $("#rbac-panel .panel-title").html('<i class="fa fa-users text-info"></i>&nbsp;&nbsp;RBAC – <b>' + $(this).text() + '</b>');
            $("#rbac-panel .panel-title i").attr("class", $("i", this).attr("class"));
            if (i == 0) {
                toggleButton("make", true);
                toggleButton("assign", false);
            } else
            if (i == 1) {
                toggleButton("make", false);
                toggleButton("assign", true);

            } else
            if (i == 2) {
                toggleButton("make", false);
                toggleButton("assign", true);
            }
            $.fn.yiiGridView.update('rbac-grid', {data: {type: i}});
        });

    </script>