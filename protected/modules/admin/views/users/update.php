<?php
/*
 * Yiimix configurating and installer class
 *
 * @category   YimMix
 * @package    yiimix.pages
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group
 * @license    http://www.yiimix.ru/license/
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

/* @var $this ShopItemsController */
/* @var $model ShopItems */
/* @var $form CActiveForm */
/* @var $fields array */



$this->breadcrumbs = array(
    Yii::t('yiimix','Users') => Yii::app()->createUrl("admin/pages/articles"),
    Yii::t('yiimix','Update ').$model->username
);

$this->pageTitle =  Yii::t('yiimix','Update user: ').$model->username;
?>

<div class="block">

    <div class="block-title">
        <div class="block-options pull-right">
            <a onclick="ajaxFormSend('article-form', '/admin/users/update/<?= $model->id ?>')" class="btn btn-sm btn-alt btn-primary enable-tooltip" title="Сохранить"><i class="fa fa-save"></i></a>            
        </div>
        <h2 ><i class="fa fa-pencil text-info"></i>&nbsp;&nbsp;<?= Yii::t('yiimix','Update user: ')?><strong><?= $model->username ?></strong></h2>
    </div>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>

</div>
<style>
    .btn-group{white-space: nowrap}
    .popover{width: 140px !Important;}
    .popover-title{font-weight:500; text-align: center; font-size: 14px}

</style>

<script>

    $(document).ready(function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            onConfirm: function () {

                fileDelete($(this).attr("data-index"));
            },
        });
    });

    var gets = "?e=pages&id=<?= $model->id ?>";




</script>