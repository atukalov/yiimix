<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class mainMenu extends CWidget {

    public $items;

    public function init() {
        $item = Menus::model()->findByAttributes(array("name" => "admin"));
        if ($item) {
            $items = Menus::model()->findAllByAttributes(array("parent" => $item->id), array("order" => "pos ASC"));

            if ($items) {
                $temp = array();
                foreach ($items as $k => $v) {

                    $ru = explode("/", trim(Yii::app()->request->requestUri, "/"));
                    $p = explode("/", trim($v->url, "/"));
                    $a = array_shift($ru);
                    $a = array_shift($p);
                    $ii = false;

                    if (!empty($ru) && !empty($p) && $ru[0] == $p[0])
                        $ii = true;
                    if (empty($ru) && empty($p))
                        $ii = true;

                    $temp[$k] = array("label" => $v->name, "url" => (substr($v->url, 0, 1) != "/" ? "/" . $v->url : $v->url), "active" => $ii, "items" => array(), "itemOptions" => array("class" => "has-sub"));
                    $si = Menus::model()->findAllByAttributes(array("parent" => $v->id));
                    if ($si) {
                        foreach ($si as $key => $value) {
                            if ($value->name == "-")
                                $temp[$k]["items"][] = array("template" => "<li class='sep'></li>");
                            else
                                $temp[$k]["items"][] = array("label" => $value->name, "url" => (substr($value->url, 0, 1) != "/" ? "/" . $value->url : $value->url));
                        }
                    }
                }
                $this->items = $temp;
            }
        }
    }

    public function run() {
        ?><div id="cssmenu">
            <?php $this->widget('zii.widgets.CMenu', array("items" => $this->items)) ?>
        </div><?php
    }

}
