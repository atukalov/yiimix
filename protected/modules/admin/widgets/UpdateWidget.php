<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class UpdateWidget extends CWidget {

    public $items = array();

    public function init() {
        ini_set("soap.wsdl_cache_enabled", 0);
        $cl = new SoapClient('http://loc.yiimix.ru/updates/quote');
        $this->items = $cl->getAllVer();
        // var_dump($this->items);
    }

    public function run() {
        ?>
        <style>
            .modul{
                border-radius: 10px;
                background: #efc;
                overflow: hidden;
                font-size: .8em;
                margin: 10px;
                border:1px solid #080
            }
            .modul .header{
                padding: 5px 10px;
                color:#fff;
                background: #0a0;
            }
            .modul .item{padding: 5px 20px 20px 10px;
                         border-bottom:1px solid #080; float:left}
            .modul .item:last-child{border: none}
            .modul .desc{color:#666}
            .but{
                border-radius: 5px;
                background: #0c0;
                padding:3px 10px;
                color:#fff;
                text-decoration: none;
                cursor:pointer;
                float:right
            }
            .modul h3{font-size: 1.4em;color:#080}
            .but:hover{
                background: #080
            }
            .modul p span{font-weight: 800}
            .layout td{width:33%}
        </style>

        <?
        ?><div class="modul"><?
        ?><div class="header">Последние версии</div><?
            if(!empty($this->items))
                foreach($this->items as $k => $v) {
                    $this->drawItem($k);
                }
            ?></div><?
            //var_dump( $this->items);die;
        }

        public function drawItem($name) {
            ?><div class="item"><?
            ?><h3><?= $name ?></h3><?
            ?><p><span>Версия: </span><?= $this->items[$name]['date'] ?></p><?
            ?><p><span>Дата выхода: </span><?= Yii::app()->dateFormatter->formatDateTime($this->items[$name]['version'], 'long', ''); ?></p><?
            ?><p class="desc"><?= $this->items[$name]['description'] ?></p><?
            ?><a class="but">Установить</a><?
            ?></div><?
        }

    }
    