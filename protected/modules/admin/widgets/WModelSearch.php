<?php

class WModelSearch extends CWidget {

    public $model;
    public $field = array();
    public $attr = array();
    private $dval;
    private $dvar;
    public $l;

    public function init() {


        if ($this->model)
            $m = $this->model;

        $mod = new $m();


        if (empty($this->field)) {
            $r = $mod->rules();

            $temp = array();
            $attr = $mod->attributeLabels();
            foreach ($r as $v)
                if (isset($v['on']) && $v['on'] = 'search') {
                    $temp = explode(',', $v[0]);
                    foreach ($temp as $k => $e) {
                        $temp[$k] = trim($e);
                        $this->attr[$k] = $attr[$temp[$k]];
                    }
                }

            $this->field = $temp;
        }


        if (isset($_GET[$this->model])) {

            $k = array_keys($_GET[$this->model]);
            $f = array_shift($k);
            $this->dvar = $f;
            $this->dval = $_GET[$this->model][$f];
        } else {
            $k = $this->field;
            $f = array_shift($k);
            $this->dvar = $f;
            $this->dval = "";
        }
        // 
        $this->l = $mod->attributeLabels();

        Yii::app()->clientScript->registerScript('WModelSearch', '
            $("#WMS_' . $this->model . ' .dropdown-menu a").on("click",function(){
                var v=$(this).attr("data-index");
                $("#WMS_' . $this->model . '_I").val(v);
                $("#WMS_' . $this->model . ' button.sel").html($(this).text()+"<span class=\"caret\"></span>");
                $("#WMS_' . $this->model . ' #search_string").attr("name","' . $this->model . '["+v+"]");
            });
             $("#WMS_' . $this->model . ' button.sub").on("click",function(){                  
                    window.location.href=window.location.href.split("?")[0];
                });
            ');
    }

    public function run() {
        
        ?>
        <div class="input-group"  id="WMS_<?= $this->model ?>">
            <div class="input-group-btn dropup">
                <input type="hidden" id="WMS_<?= $this->model ?>_I" value="<?= $this->dvar ?>">
                <button type="button" class="btn btn-default dropdown-toggle sel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->l[$this->dvar] ?> <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <?php foreach ($this->attr as $k => $v): ?>
                        <li><a data-index="<?= $this->field[$k] ?>" ><?= $v ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <form id="WModelForm" method="get">
                <input type="text" class="form-control" aria-label="..." id="search_string" value="<?= $this->dval ?>" name="<?= $this->model ?>[<?= $this->dvar ?>]">
            </form>
            <span class="input-group-btn">
                <button class="btn btn-default sub" type="button"><i class="fa fa-remove"></i></button>
            </span>
        </div>
        <?php
    }

}
