<?php

class WModules extends CWidget {

    public $items;

    public function init() {
        $res = Yii::app()->cache->get('admin.config.modules.' . Language::$current);
        $this->items = $res;

        if (!$res) {
            $urls = array();
            // var_dump(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'));die;
            if ($handle = opendir(Yii::getPathOfAlias('application.modules.admin.controllers.config.modules'))) {
                Yii::import('application.modules.admin.controllers.config.modules.*');
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && $entry != '.DS_Store') {
                        $c = explode('.', $entry);
                        $c = $c[0];
                        $l = strtolower(str_replace('Action', '', $c));
                        $r = new $c($this->controller, $l);
                        $urls[] = array('id' => count($urls), 'title' => Yii::t('admin', $r->title), 'url' => 'admin/config/' . $l);
                    }
                }
                closedir($handle);
            }
              $this->items = $urls;
            Yii::app()->cache->set('admin.config.modules.' . Language::$current, $urls);
        } 
    }

    public function run() {
        ?>
        <div class="block">
            <div class="block-title">
                <h2><strong>Модули</strong></h2>
            </div>
            <ul class="nav nav-pills nav-stacked" style="padding-bottom: 20px" id="top-menu">

                <?php
                foreach ($this->items as $v) :
                    $cl = Yii::app()->request->requestUri == '/' . $v['url'] ? " class=\"active\"" : "";
                    ?>            
                    <li<?= $cl ?>>
                        <a href="/<?= $v['url'] ?>"><?= $v['title'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>


        <?php
    }

}
