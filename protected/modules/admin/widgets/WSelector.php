<?php

class WSelector extends CWidget
{

    public $varName = '';
    public $id = 'selector';
    public $value;
    public $items = array();
    public $ajax = array();
    public $options = "";

    public function init()
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerScript($this->id.'Click', '$("#'.$this->id.' button").on("click", function(){ $("#'.$this->id.' button").removeClass("active");$(this).addClass("active");'
            . '$.ajax({url: "'.(isset($this->ajax['url'])?$this->ajax['url']:Yii::app()->request->url).'", '
            . 'type: "'.(isset($this->ajax['type'])?$this->ajax['type']:'get').'", '
            . 'data: {'.$this->varName.': $(this).attr("data-index")}, '
            . (isset($this->ajax['dataType'])?$this->ajax['dataType']:'')
            . (isset($this->ajax['success'])?'success: function(data){'.$this->ajax['success'].'}':'')
            .'});'
            . '});', CClientScript::POS_READY);        
        
    }

    public function run()
    {
        if(!empty($this->items)):
            ?><div class="btn-group btn-group-sm" id="<?= $this->id ?>" <?= $this->options?>><?php
            foreach($this->items as $item):
                $class = '';
                if($this->value == $item['value'])
                    $class = " active";
                ?><button  <?= $this->options?> class="btn btn-default btn-alt<?= $class ?>" data-index="<?=$item['value']?>"><?php
                        if(isset($item['icon'])):
                            ?><i class="<?= $item['icon'] ?>"></i> <?php
                        endif;
                        ?><?= $item['title'] ?></button><?php
                endforeach;
                ?></div><?php
        endif;
    }

}
