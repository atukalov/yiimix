<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class XAlphabet extends CWidget {

    public $url;
    public $var = 'ch';
    private $df = '';

    public function init() {
        if (isset($_GET[$this->var]) && $_GET[$this->var] != '') {
            $this->df = mb_strtoupper($_GET[$this->var]);
        } else {
            if (!empty($_GET))
                $this->df = 'filter';
        }
    }

    public
            function run() {
        ?>

        <nav class="nav navbar-default" id="alphabeth">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alphabeth-inner" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h4 class="navbar-text "><?= Yii::t('admin', 'Username`s Filter') ?></h4>
            <div class="collapse navbar-collapse" id="alphabeth-inner">
                <ul class="nav nav-pills nav-justified">
                    <li role="presentation" <?= ($this->df == '' ? 'class="active"' : '') ?>><a href="<?= $this->url ?>"><?= Yii::t('admin', 'All') ?></a></li>
                    <?php
                    foreach (range('A', 'Z') as $letter) {
                        ?><li role="presentation" <?= ($letter == $this->df ? 'class="active"' : '') ?>><a href="<?= $this->url ?>/?<?= $this->var ?>=<?= mb_strtolower($letter) ?>"><?= $letter ?></a></li><?php
                        }
                        ?></ul>
            </div>
        </nav><?php
    }

}
