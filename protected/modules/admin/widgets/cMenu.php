<?php
/*
 * Yiimix configurating and installer class
 * 
 * @category   YimMix 
 * @package    yiimix.base
 * @author     Tukalov Anatoly <anatoly.tukalov@gmail.com>
 * @copyright  2014 YiiMix Group 
 * @license    http://www.yiimix.ru/license/ 
 * @version    SVN: $Id$
 * @link       http://www.yiimix.ru/package/base
 * @see        http://www.yiimix.ru/
 * @since      File available since Release 0.0.1
 * @deprecated File deprecated in Release 0.0.1
 */

class cMenu extends CWidget {

    public $items = array();

    public function run() {
        if (!empty($this->items)) {
            ?><ul class="top-menu"><?
            foreach ($this->items as $k => $v) {
                if (is_numeric($k)) {
                    ?><li class="sep"><hr></li><?
                    } else {
                        // var_dump($v === '/'.Yii::app()->controller->action->url());
                        if ($v['url'] == '/' . Yii::app()->controller->action->url())
                            $cl = ' class="active"';
                        else
                            $cl = '';
                        ?><li<?= $cl ?>><a href="<?= $v['url'] ?>" id="<?= $v['class'] ?>"><span><?= $k ?></span></a></li><?
                        }
                    }
                    ?></ul><?
                }
            }

        }
        ?>
