<?php

class fieldsForItem extends CWidget {

    public $model;
    public $entity;
    public $fields;

    public function init() {

        $this->fields = Fields::model()->findAllByAttributes(array('target' => 'lists'));
        $this->_registerScripts();
    }

    public function run() {
        if (isset($this->fields)):
            ?>
            <div class="col-lg-12">


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title"><i class="fa fa-list text-info"></i>&nbsp;&nbsp;<b><?= Yii::t("admin", "Подключение дополнительных полей") ?></b></h2>
                    </div>
                    <div class="panel-body" style="border-bottom: 1px solid #ddd; background: #fafafd">
                        <div class="pull-right">
                            <a href="<?php echo Yii::app()->createUrl("admin/pages/fields") ?>" class="btn btn-sm btn-primary"><i class="fa fa-list"></i> Дополнительные поля</a>

                        </div>
                    </div>

                    <div class="panel-body" style="padding: 0;margin: 0;">

                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'dataProvider' => new CArrayDataProvider($this->fields, array('pagination' => array(
                                    'pageSize' => 1000,
                                ),)),
                            'itemsCssClass' => "table table-vcenter table-striped table-condensed table-hover dataTable table-responsive",
                            'template' => '{items}{pager}',
                            'cssFile' => '',
                            'columns' => array(
                                'title',
                                'name',
                                'type',
                                array('class' => 'boolColumn',
                                    'name' => 'multi',
                                ),
                                array('class' => 'boolColumn',
                                    'name' => 'all',
                                ),
                                array('class' => 'boolColumn',
                                    'name' => 'req',
                                ),
                                array(
                                    'type' => 'raw',
                                    'header' => '',
                                    'value' => '"<label class=\"switch switch-danger\" style=\"float:right;margin-right:10px\"><input class=\"uses\" name=\"checked[]\" type=\"checkbox\" data-index=\"".$data->id."\" data-entity=\"' . $this->entity . '\" data-item=\"' . $this->model->id . '\"".(Fields::isChecked(' . $this->model->id . ',"' . $this->entity . '",$data->id)==1?" checked":"").">  <span></span></label>"',
                                ),
                            ),
                        ))
                        ?>
                    </div>
                </div>
            </div>
            <?php
        endif;
    }

    private function _registerScripts() {
        $script = <<<EOT
$(".uses").on("change",function(){
   $.ajax({
        url: '/admin/pages/fields/ajax',
        method: "POST",
        data: "F[target]="+$(this).data("entity")+"&F[action]="+$(this).data("item")+"&F[field]="+$(this).data("index"),
        dataType:"html",
        error: function(a){
        console.log(a);
            }
   }); 
   });        
EOT;

        Yii::app()->clientScript->registerScript('usesField_' . $this->entity, $script, CClientScript::POS_READY);
    }

}
