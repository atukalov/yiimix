<?php

class formFields extends CWidget {

    public $model;
    public $form;

    public function run() {      
      

        if ($this->model->id !== null) {
            ?><div id="add"><?php
                $f = $this->model->FieldsBehavior->getFields();
                if (!empty($f)) {
                    foreach ($f as $field) {
                        echo $field->input($this->form, $this->model);
                    }
                }
                ?></div><?php
        }
    }

}
