<?php

class filterWidget extends adminWidget
{

    public $target;
    private $columns = array();
    private $model;

    public function init()
    {

        $this->title = "Фильтр";
        $this->icon = "fa fa-filter";

        $this->minimize = false;
        $this->fullscreen = false;

        $s = $this->target;
        $model = $s::model();
        $columns = $model->rules();

        $res = array();
        foreach($columns as $key => $value)
            if(in_array("search", $value))
                foreach($value as $k => $v)
                    if($k == "on" && $v = "search")
                        $res = explode(",", $value[0]);



        if(!empty($res))
            $this->columns = $res;
        
        Yii::app()->clientScript->registerScript('filterSubmit','function applyFilters(){ $.fn.yiiGridView.update("pages-grid",{data: $("#filter-form").serialize()});return false;}', CClientScript::POS_BEGIN);
        Yii::app()->clientScript->registerScript('filterClear','function clearFilters(){document.getElementById("filter-form").reset();applyFilters();}', CClientScript::POS_BEGIN);
        
        
    }

    public function buttons()
    {
        ?><a onclick="applyFilters()" class="btn btn-sm btn-alt btn-success"  title="Фильтровать" id="fileupload-clear"><i class="fa fa-refresh"></i></a>
         <a onclick="clearFilters()" class="btn btn-sm btn-alt btn-danger"  title="Обнулить" id="fileupload-clear"><i class="fa fa-ban"></i></a><?php
    }

    public function content()
    {

        if(!empty($this->columns))
        {
            $model = new $this->target;
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'filter-form',
                'enableAjaxValidation' => false,
                "htmlOptions" => array("class" => "form-horizontal form-bordered"),
            ));

            foreach($this->columns as $v)
            {
                ?><div class="form-group"><?php
                    echo $form->label($model, trim($v), array("class" => "col-md-3"));
                    ?><div class="col-md-9"><?php
                        echo $form->textField($model, trim($v), array("class" => "form-control"));
                        ?></div><?php
                        ?></div><?php
            }

            $this->endWidget();
        }
    }

}
