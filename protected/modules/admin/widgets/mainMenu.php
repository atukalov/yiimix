<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class mainMenu extends CWidget
{

    public $items;

    public function init()
    {
        $this->items = array_keys(Config::getByValue('admin', 'modules', 1));
    }

    public function run()
    {
        ?>
        <ul>
            <? $cc = strtolower(Yii::app()->controller->id); ?>


            <?php foreach($this->items as $v): ?>
                <li id="<?= $v ?>" <?= $cc == $v ? ' class="active"' : '' ?>><a href="/admin/<?= $v ?>"><?= $v ?></a></li>

        <?php endforeach; ?>
</ul>
<?
}

}
