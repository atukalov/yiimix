<?php

class newMainMenu extends CWidget {

    private $menu;
    private $start;
    private $cu;
    private $u;

    public function init() {

        $cacheId = "mainMenu";
        $cache = Yii::app()->cache->get($cacheId);

        if ($cache === false) {

            $adminMenu = Menus::model()->findByAttributes(array("name" => "admin"));
            $this->start = $adminMenu->id;

            if ($adminMenu) {

                $items = Menus::model()->findAll(array("order" => "pos ASC"));
                $temp = array();
                $tempd = array();

                if ($items) {
                    // Первая обработка
                    foreach ($items as $v) {

                        $temp[$v->id] = $v->attributes;
                        $temp[$v->id]["sub"] = array();

                        if ($v->query !== "") {

                            eval('$temp1=' . $v->query . ';');

                            foreach ($temp1 as $k1 => $v1)
                                $temp1[$k1]["parent"] = $v->id;

                            $tempd = array_merge($tempd, $temp1);
                        }
                    }

                    foreach ($tempd as $k => $v)
                        $temp[] = $v;

                    unset($tempd);

                    // Вторая обработка
                    foreach ($temp as $k => $v) {
                        foreach ($temp as $k1 => $v1) {
                            if (is_array($v1)) {

                                if ($v1["parent"] . "" === $v["id"]) {
                                    $temp[$k]["sub"][] = $k1;
                                }
                            }
                        }
                    }

                    $this->menu = $temp;
                    unset($temp);
                    unset($adminMenu);
                    Yii::app()->cache->set($cacheId, array("menu" => $this->menu, "start" => $this->start), 60 * 60 * 60 * 24);
                }
            }
        } else {
            $this->menu = $cache["menu"];
            $this->start = $cache["start"];
        }
        $ss = explode("?", ltrim(Yii::app()->request->requestUri, "/"));
        $r = explode("/", $ss[0]);
        $this->u = array();
        $aa = isset($ss[1]) ? "?" . $ss[1] : "";

        foreach ($r as $k => $v) {
            $this->u[$k] = isset($r[$k - 1]) ? ltrim($this->u[$k - 1] . "/" . $v, "/") : $v;
        }
        $this->cu = end($this->u) . $aa;
    }

    private function renderItem($i) {

        if ($i == $this->start) {
            foreach ($this->menu[$i]["sub"] as $v)
                $this->renderItem($v);
        } else {

            $item = $this->menu[$i];

            if ($item["op"] == 1) {
                ?>
                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix">
                        <a href="javascript:void(0)" data-toggle="tooltip" title="" 
                           data-original-title="<?= Yii::t('admin', $item["name"]) ?>">
                            <i class="<?= $item["icon"] ?>"></i></a></span>
                    <span class="sidebar-header-title"><?= Yii::t('admin', $item["name"]) ?></span>
                </li>

                <?php
                if (!empty($item["sub"]))
                    foreach ($item["sub"] as $v)
                        $this->renderItem($v);
            } elseif ($item["op"] == 0) {

                $c = "";
                if ($this->cu == ltrim($item["url"], "/"))
                    $c = " class=\"active\"";


                if (empty($item["sub"])) {
                    ?>
                    <li <?= $c ?>>
                        <a href="<?= Yii::app()->createUrl($item["url"]) ?>">
                            <i class="<?= $item["icon"] ?>"></i> 
                            <span class="sidebar-nav-mini-hide">
                                <?= Yii::t('admin', $item["name"]) ?>
                            </span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li>
                        <a  class="sidebar-nav-submenu">
                            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                            <i class="<?= $item["icon"] ?>"></i> 
                            <span class="sidebar-nav-mini-hide"> <?= Yii::t('admin', $item["name"]) ?></span></a>
                        <ul>
                            <?php
                            foreach ($item["sub"] as $v)
                                $this->renderItem($v);
                            ?></ul></li>
                    <?php
                }
            }
        }
    }

    public function run() {
        ?>

        <div id="sidebar">           

            <div id="sidebar-scroll">                
                <div class="sidebar-content">
                    <a href="/admin" class="sidebar-brand">
                        <img src="/images/admin/logosm.png"  style="height:42px;">
                        <span class="sidebar-nav-mini-hide" style="font-size:1.2em;letter-spacing: .1em"><strong>yiimix</strong></span>
                    </a>
                    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                        <div class="sidebar-user-avatar">
                            <a href="/admin/user/profile">
                                <img src="<?= Yii::app()->user->getAvatar() ?>" alt="avatar">
                            </a>
                        </div>
                        <div class="sidebar-user-name"><?= Yii::app()->user->getName() ?></div>
                        <div class="sidebar-user-links">

                            <?php
                            $c = Config::get('user.panel.widgets');

                            //var_dump($c);die;
                            if (!empty($c))
                                foreach ($c as $k => $v) {
                                    if (is_array($v)) {
                                        Yii::import($v[0]);
                                        $this->widget($v[0]);
                                    } else {
                                        Yii::import($v);
                                        $this->widget($v);
                                    }
                                }
                            ?>
                            <a href="/user/logout" data-toggle="tooltip" data-placement="bottom" title="<?= Yii::t('admin', 'Logout') ?>" data-original-title="<?= Yii::t('admin', 'Logout') ?>"><i class="gi gi-exit"></i></a>
                        </div>
                    </div>
                </div>

                <ul class="sidebar-nav"  style="width:200px">
                    <?php $this->renderItem($this->start) ?>
                </ul>
            </div>
        </div>
        <?php
    }

}
