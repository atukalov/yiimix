<?php

class ymCatsTree extends CWidget {

    public $items=array();

    public function init() {
  
        $sql = "SELECT `id`, `parent`, `title` FROM shop_cats";
        $res = Yii::app()->db->createCommand($sql)->query();

        if ($res) {
            

            while ($i = $res->read())
                $this->items[$i['id']] = array($i['id'], $i['parent'], $i['title'], array());

            foreach ($this->items as $k => $v)
                foreach ($this->items as $k1 => $v1)
                    if ($v1[1] == $k)
                        $this->items[$k][3][] = $k1;
               
        }
        
        $this->renderScript();
    }

    private function normalizeItems() {
        
    }

    private function renderItem($item) {        
       
        $el='<li><span class="file"><a href="/admin/shop/tovar?id='.$item.'">Элементы</a></span></li>';
        $str = '<li><a href="/admin/shop/catalog?cat='.$item.'">' . $this->items[$item][2] . '</a>';

        $sub = '';
        if (!empty($this->items[$item][3]))
            foreach ($this->items[$item][3] as $v)
                $sub .= $this->renderItem($v);

        if ($sub != '')
            $str.='<ul>' . $sub . '</ul></li>';
        else
            $str.='<ul>' . $el . '</ul></li>';
        return $str;
    }

    public function run() {
        
        ?><div><ul id="navigation" class="treeview-red"><?
            $s = '';
            if(!empty($this->items))
            foreach ($this->items as $k => $v)
                if($v[1]==0)
                $s.=$this->renderItem($k);
            
            echo $s;
        ?></ul></div><?
    }

    private function renderScript(){
        
        $cs=Yii::app()->getClientScript();
        $cs->registerCSSFile('/js/jquery.treeview/jquery.treeview.css');
        $cs->registerScriptFile('/js/jquery.treeview/jquery.treeview.js');
         $cs->registerScriptFile('/js/jquery.cookie.js');

        $cs->registerScript('tv','
            $("#navigation").treeview({
		persist: "location",
		collapsed: true,
		unique: true,
                animated: "fast",
	});');
    }
    
}
?>
