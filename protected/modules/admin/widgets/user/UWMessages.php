<?php

class UWMessages extends CWidget
{
    public function run()
    {
        ?> <a href="/admin/users/messages" data-toggle="tooltip" data-placement="bottom" title="<?= Yii::t('admin', 'Messages') ?>" data-original-title="<?= Yii::t('admin', 'Messages') ?>"><i class="gi gi-envelope"></i></a><?php        
    }

}
