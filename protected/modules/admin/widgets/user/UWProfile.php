<?php

class UWProfile extends CWidget
{
    public function run()
    {
        ?> <a href="/admin/users/profile" data-toggle="tooltip" data-placement="bottom" title="<?= Yii::t('admin', 'Profile') ?>" data-original-title="<?= Yii::t('admin', 'Profile') ?>"><i class="gi gi-vcard"></i></a><?php
    }

}
