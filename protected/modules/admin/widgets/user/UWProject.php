<?php

class UWProject extends CWidget
{
    public function run()
    {
        ?> <a href="/admin/users/profile" data-toggle="tooltip" data-placement="bottom" title="<?= Yii::t('admin', 'Project') ?>" data-original-title="<?= Yii::t('admin', 'Project') ?>"><i class="gi gi-cogwheel"></i></a><?php
    }

}
