<?php

class ymPointer extends CWidget {

    public $model;
    public $field;
    private $fieldt;
    private $emptyCountRow = 3;
    private $val = null;
    private $dval;

    public function init() {

        $this->fieldt = ShopFields::getField($this->field);
        $f = $this->field;
        if ($this->model->$f != '') {
            $this->val = explode(',', $this->model->$f);
        }
        
        $res = ShopItems::model()->findAllByAttributes(array('id' => $this->val));
        $temp = array();
        foreach ($res as $v)
            $temp[$v->id] = $v->title;

        unset($res);
        $this->dval = $temp;
        unset($temp);
        $this->registerScript();
    }

    public function run() {
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'dialog',
            'options' => array(
                'title' => 'Выбирете товар',
                'autoOpen' => false,
                'modal' => true,
                'width' => 'auto',
                'height' => 'auto',
                'resizable' => 'false',
                'buttons' => array('close' => 'js:function() { $( this ).dialog( "close" );}'),
            ),
        ));

        $model = new ShopItems('search');
        $model->unsetAttributes();
        if (isset($_GET['ShopItems']))
            $model->attributes = $_GET['ShopItems'];

        $this->controller->renderPartial('application.modules.admin.views.shop.catalog.select', array('model' => $model, 'var' => $this->field));

        $this->endWidget();

        $f = $this->field;

        if ($this->val!==null) {            
            foreach ($this->val as $k => $v) {
                ?><input name='Pointer[<?= $f ?>][<?= $k ?>]' placeholder='ID' value="<?= $v ?>" id="pointer-<?= $f ?>-<?= $k ?>">
                <label id="pointertitle-<?= $f ?>-<?= $k ?>"><?= $this->dval[$v] ?></label>
                <a class="pointer" id="b-<?= $f ?>-<?= $k ?>">...</a><?
                ?><br><?
            }
        } else {
            for ($i = 0; $i < $this->emptyCountRow; $i++) {
                ?><input name='Pointer[<?= $f ?>][<?= $i ?>]' placeholder="Товар" id="pointer-<?= $f ?>-<?= $i ?>">
                <label id="pointertitle-<?= $f ?>-<?= $i ?>"></label>
                <a class="pointer" id="b-<?= $f ?>-<?= $i ?>">...</a><?
                ?><br><?
            }
        }
    }

    public function registerScript() {

        $cs = Yii::app()->getClientScript();
        $cs->registerScript('global', ' var sel=1;  ', CClientScript::POS_BEGIN);
        $cs->registerScript('Pointer_' . $this->field, '
         
          
$(".butonone").die("click");
          $("a.pointer").die("click").live("click",function(){
          
sel=$(this).attr("id").split("-")[2];
            $("#dialog").dialog("open");
            return false;
            });
        



');
    }

}
?>
