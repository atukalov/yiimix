<?php

class ymVariant extends CWidget {

    public $model;
    public $field;
    private $fieldt;
    private $emptyCountRow = 3;
    private $val;

    public function init() {

        $this->fieldt = ShopFields::getField($this->field);
        $f = $this->field;
        $this->val = json_decode($this->model->$f);
    }

    public function run() {
        $f = $this->field;
        $title = array('Вариант', 'Цена', 'На складе');
        if (!empty($this->val)) {
            foreach ($this->val as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    ?><input class="var_<?= $f ?>" name='Variant[<?= $f ?>][<?= $k ?>][<?= $k1 ?>]' value="<?= $v1 ?>" placeholder='<?= $title[$k1] ?>'><?
                }
                ?><br><?
            }
        } else {
           for ($i=0;$i<$this->emptyCountRow;$i++) {
               for ($j=0;$j<3;$j++) {
                    ?><input name='Variant[<?= $f ?>][<?= $i ?>][<?= $j ?>]'  placeholder='<?= $title[$j] ?>'><?
                }
                ?><br><?
            }
        }
    }

}

