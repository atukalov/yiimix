<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle="Error ".$code;

?>
<style>
    body{background: #ccc}
    .mlogo{background:url(/images/ico/all.svg) 0 -1px no-repeat;display:block;width:150px;height:70px}
</style>
 <a href="/" class="mlogo" id="logo"></a>
<h1>Error <?php echo $code; ?></h1>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>
<br><br><a href="/">Вернуться на главную страницу</a>