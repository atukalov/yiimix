<?php

class Calculator extends CWidget {

    public $prices = array();
    public $calc;

    public function init() {
        $c = Price::model()->findByAttributes(array('title' => 'Калькулятор'));
        $calc = Price::model()->findAllByAttributes(array('parent' => $c->id));
        $this->calc = $calc;
        $ch = array();


        foreach($calc as $v) {
            $l = substr($v['title'], 0, 1);
            $r = explode(':', $v['title']);

            if(substr($v['title'], 1, 2) != '00') {

                switch ($l) {
                    case 'U':
                        $ch['pot'][$r[0]] = $r[1];
                        break;
                    case 'D':
                        $ch['pol'][$r[0]] = $r[1];
                        break;
                    case 'S':
                        $ch['st'][$r[0]] = $r[1];
                        break;
                    case 'E':
                        $ch['el'][$r[0]] = $r[1];
                        break;
                    case 'V':
                        $ch['san'][$r[0]] = $r[1];
                        break;
                }
            }
            else {
                $ch['null'][$r[0]] = $r[1];
            }
        }
        $this->prices = $ch;
    }

    public function run() {
        ?>

        <style>
            .calc{
                background: #eee;
                width:400px;
                padding:20px;
                border-radius: 10px;
                margin: 0 auto

            }
            .calc table{width:100%}
            .calc .title{font-weight: 800;text-align: center; border: 1px solid #fff; border-width: 1px 0}
        </style>

        <div class="calc"><?php
            $m = new calc();
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'calc-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <table>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'width'); ?>
                    </td><td>
                        <?php echo $form->textField($m, 'width'); ?>м.
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'height'); ?>
                    </td><td>
                        <?php echo $form->textField($m, 'height'); ?>м.
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'length'); ?>
                    </td><td>
                        <?php echo $form->textField($m, 'length'); ?>м.
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="title">Потолок</td></tr>
                <tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'dpotolok'); ?>
                    </td><td>
                        <?php echo $form->checkbox($m, 'dpotolok'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'potolok'); ?>
                    </td><td>
                        <?php echo $form->dropDownList($m, 'potolok', array_merge(array(-1 => 'Нет'), $this->prices['pot'])); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="title">Пол</td></tr>
                <tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'dpol'); ?>
                    </td><td>
                        <?php echo $form->checkbox($m, 'dpol'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'pol'); ?>
                    </td><td>
                        <?php echo $form->dropDownList($m, 'pol', array_merge(array(-1 => 'Нет'), $this->prices['pol'])); ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="title">Стены</td></tr>
                <tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'dstena'); ?>
                    </td><td>
                        <?php echo $form->checkbox($m, 'dstena'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->label($m, 'stena'); ?>
                    </td><td>
                        <?php echo $form->dropDownList($m, 'stena', array_merge(array(-1 => 'Нет'), $this->prices['st'])); ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <a onclick="cacl()" class="but" style="font-size:.9em; float:left;margin-top:10px">Рассичтать</a>
                    </td><td class="cena">

                    </td>
                </tr>
            </table>
            <script>
                function cacl() {

                    if ($("#Calc_width").val() == "" || $("#Calc_height").val() == "" || $("#Calc_length").val() == "") {
                        alert("Введите пожалуйста размеры комнаты");
                    } else {

                        $.ajax({
                            type: "post",
                            url: "/site/calc",
                            data: $("#calc-form").serialize(),
                            dataType: "html"
                        }).done(function(data) {                            
                            $("td.cena").html(data);

                        });
                    }

                }
            </script>
            <?php Yii::app()->clientScript->registerCoreScript("jquery") ?>

            <?php $this->endWidget(); ?>
        </div>
        <?php
    }

    public function calc($vars) {
        $this->init();
        $pr = array();
        foreach($this->calc as $v) {
            $a = explode(":", $v["title"]);
            $pr[$a[0]] = $v;
        }

        $w = (isset($vars['width']) && $vars['width'] > 0) ? floatval($vars['width']) : false;
        $h = (isset($vars['height']) && $vars['height'] > 0) ? floatval($vars['height']) : false;
        $l = (isset($vars['length']) && $vars['length'] > 0) ? floatval($vars['length']) : false;

        if(!$w || !$h || !$l) {

            return 'Error: Нужно задать размеры';
        }
        else {
            $summa = 0;
            $pu = $w * $h;
            $pl = $w * $l * 2 + $h * $l * 2;
            //Вычисляем потолок
            if(isset($vars["dpotolok"]) && $vars["dpotolok"] == "1") {
                $summa+=$pr["U00"]->price * $pu;
            }
            if(isset($vars["potolok"]) && $vars["potolok"] != "0") {
                $summa+=$pr[$vars["potolok"]]->price * $pu;
            }

            //Вычисляем пол
            if(isset($vars["dpol"]) && $vars["dpol"] == "1") {
                $summa+=$pr["D00"]->price * $pu;
            }
            if(isset($vars["pol"]) && $vars["pol"] != "0") {
                $summa+=$pr[$vars["pol"]]->price * $pu;
            }

            //Вычисляем cтены
            if(isset($vars["dstena"]) && $vars["dstena"] == "1") {
                $summa+=$pr["S00"]->price * $pl;
            }
            if(isset($vars["stena"]) && $vars["stena"] != "0") {
                $summa+=$pr[$vars["stena"]]->price * $pl;
            }
            return $summa;
        }
    }

}


