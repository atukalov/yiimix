<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author wolf
 */
class bxSlider extends CWidget {

    public $items = array();
    public $id;
    public $options = "auto: true,autoControls: true,captions: true";
    public $vars = array();

    public function init() {
        $this->registerScript();
    }

    public function run() {
        $image = $this->vars["image"];
        $tit = $this->vars["title"];
        ?><ul class="bxslider" ><?php
            if ($this->items) {
                foreach ($this->items as $v) {
                    ?><li><img src = "<?= $v->$image ?>" title = "<?= $v->$tit ?>"/></li><?php
                }
            }
            ?></ul><?php
    }

    private function registerScript() {

        Yii::app()->getClientScript()->registerScriptFile("/js/jquery.bxslider/jquery.bxslider.min.js");//, CClientScript::POS_END     
        Yii::app()->getClientScript()->registerScript("bxSlider", "$('.bxslider').bxSlider({
                    auto: true,
                    autoControls: true,
                    captions: true
                });");
    }

}
