<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author wolf
 */
class catalogMenu extends CWidget {

    public $current;
    public $items;

    public function init() {

        $this->items = $this->getParentList($this->current->id);
        //var_dump($this->items);die;
        $this->registerScript();
    }

    private function getParentList($id) {
        $temp = array();

        $parent = $this->current->parent;

        $res = Pages::model()->findAllByAttributes(array("parent" => $this->current->id, "action" => array("catalog","cdat","widecat")));
        $temp[] = CHtml::listData($res, 'url', 'title');

        while ($parent > 1) {
            $res = Pages::model()->findAllByAttributes(array("parent" => $parent, "action" => array("catalog","cdat","widecat")));
            $temp[] = CHtml::listData($res, 'url', 'title');

            if(!$res)
                break;
            $res = Pages::model()->findByPk($res[0]->parent);

            $parent = $res->parent;
            unset($res);
        }

        return $temp;
    }

    public function run() {
        ?><div class="cat-menu"><table class="catalog-menu"><tr><?php
        $j = 0;
        for ($i = count($this->items) - 1; $i >= 0; $i--) {
            if (!empty($this->items[$i])) {
                $c = round((256 / (count($this->items) + 2)) * $j);
                ?><td class="list" style="background: rgba(<?= $c ?>,<?= $c ?>,<?= $c ?>,.95)"><ul><?php
                        $j++;
                        foreach ($this->items[$i] as $k => $v) {
                            ?><li><a href="/<?= $k ?>"><?= $v ?></a></li><?php
                                }
                                ?></ul></td><?php
                        }
                      
        }
          ?><td style="width: 25%;background: rgba(0,0,0,.1)"></td></tr></table></div><?php
    }

    private function registerScript() {
        
        $cs=Yii::app()->getClientScript();
        $cs->registerCoreScript("jquery");
         $cs->registerScript("inarray","
        Array.prototype.in_array = function(p_val) {
    for(var i = 0, l = this.length; i < l; i++)  {
        if(this[i] == p_val) {
            return true;
        }
    }
    return false;
}",  CClientScript::POS_BEGIN);
        
        $cs->registerScript("catmenus","
            
	


              var url =  window.location.pathname.split('/');
              var urls=new Array();
              var str='';
              for(var a in url){
              if(a>0){
                urls.push(str+'/'+url[a]);
                str=str+'/'+url[a];
              }
              }

                $('.catalog-menu li').each(function(){
                    var t=$(this);
                    var href=$('a',this).attr('href');
                    console.log(href,urls);
                    if(urls.in_array(href)){
                    t.addClass('active');
                    }
                })
                ",CClientScript::POS_READY);
        
    }

}
