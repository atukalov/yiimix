<?php

class MainMenu extends CWidget {

    private $menu;
    private $start = 0;
    private $cu;
    private $u;

    public function init() {

        $cacheId = "main_menu";
        $cache = Yii::app()->cache->get($cacheId);

        if ($cache === false) {

            $menu = Menus::model()->findByAttributes(array("name" => "main"));
            $this->start = $menu->id;

            if ($menu) {

                $items = Menus::model()->findAll(array("order" => "pos ASC"));

                $temp = array();
                $tempd = array();

                if ($items) {
                    // Первая обработка
                    foreach ($items as $v) {

                        $temp[$v->id] = $v->attributes;
                        $temp[$v->id]["sub"] = array();

                        if ($v->query !== "") {
                            eval('$temp1=' . $v->query . ';');

                            foreach ($temp1 as $k1 => $v1)
                                $temp1[$k1]["parent"] = $v->id;

                            $tempd = array_merge($tempd, $temp1);
                        }
                    }


                    foreach ($tempd as $k => $v)
                        $temp[] = $v;
                    unset($tempd);

                    // Вторая обработка
                    foreach ($temp as $k => $v) {
                        foreach ($temp as $k1 => $v1) {
                            if (is_array($v1)) {
                                if ($v1["parent"] === $v["id"]) {
                                    $temp[$k]["sub"][] = $k1;
                                }
                            }
                        }
                    }

                    $this->menu = $temp;
                    unset($temp);
                    unset($menu);
                    Yii::app()->cache->set($cacheId, array("menu" => $this->menu, "start" => $this->start), 60 * 60 * 60 * 24);
                }
            }
        } else {
            $this->menu = $cache["menu"];
            $this->start = $cache["start"];
        }
        $ss = explode("?", ltrim(Yii::app()->request->requestUri, "/"));
        $r = explode("/", $ss[0]);
        $this->u = array();
        $aa = isset($ss[1]) ? "?" . $ss[1] : "";

        foreach ($r as $k => $v) {
            $this->u[$k] = isset($r[$k - 1]) ? ltrim($this->u[$k - 1] . "/" . $v, "/") : $v;
        }
        $this->cu = end($this->u) . $aa;
    }

    private function renderItem($i) {

        if ($i == $this->start) {
            foreach ($this->menu[$i]["sub"] as $v)
                $this->renderItem($v);
        } else {
            $item = $this->menu[$i];
            $c = "";
            if ($this->cu == ltrim($item["url"], "/"))
                $c = " class=\"active\"";

            if ($item["name"] == 'separator') {
                ?>
                <li role="separator" class="divider"></li>
                <?php
            } else if (empty($item["sub"])) {
                ?>
                <li <?= $c ?>>
                    <?php if ($item["url"] != '' || $item["name"] == 'Главная') { ?>
                        <a href="<?= self::url($item["url"]) ?>">
                        <?php } else { ?><span> <?php } ?>
                            <?= $item["name"] ?>
                            <?php if ($item["url"] != '' || $item["name"] == 'Главная') { ?>
                        </a>
                    <?php } else { ?></span> <?php } ?>
                </li>
                <?php
            } else {
                ?>
                <li class="dropdown" onclick="window.location.href='/<?= $item["url"] ?>'">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <?= $item["name"] ?></a>
                    <ul class="dropdown-menu  multi-level" style="display: none">
                        <?php
                        foreach ($item["sub"] as $v)
                            $this->renderSubItem($v);
                        ?></ul></li>
                <?php
            }
        }
    }

    private function renderSubItem($i) {

        if ($i == $this->start) {
            foreach ($this->menu[$i]["sub"] as $v)
                $this->renderItem($v);
        } else {

            $item = $this->menu[$i];
            $c = "";
            if ($this->cu == ltrim($item["url"], "/"))
                $c = " class=\"active\"";

            if ($item["name"] == 'separator') {
                ?>
                <li role="separator" class="divider"></li>
                <?php
            } else if (empty($item["sub"])) {
                ?>
                <li <?= $c ?>>
                    <?php if ($item["url"] != '' || $item["name"] == 'Главная') { ?>
                        <a href="<?= self::url($item["url"]) ?>">
                        <?php } else { ?><span> <?php } ?>
                            <?= $item["name"] ?>
                            <?php if ($item["url"] != '' || $item["name"] == 'Главная') { ?>
                        </a>
                    <?php } else { ?></span> <?php } ?>
                </li>
                <?php
            } else {
                ?>
                <li class="dropdown-submenu" onclick="window.location.href='/<?= $item["url"] ?>'">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <?= $item["name"] ?></a>
                    <ul class="dropdown-menu" style="display: none">                        
                        <?php
                        foreach ($item["sub"] as $v)
                            $this->renderSubItem($v);
                        ?></ul></li>
                    <?php
                }
            }
        }

        protected static function url($str) {

            if (mb_substr($str, 0, 1) != '/')
                return '/' . $str;
            else
                return $str;
        }

        public function run() {
            ?>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav light">
                <?php if (Yii::app()->request->requestUri != '/') { ?>
                    <li><img src="/img/logo2.png" class="logo2" alt=""></li>
                <?php } ?>

                <?php $this->renderItem($this->start) ?>
                <style>
                    .dropdown-submenu {
                        position: relative;
                        display: block
                    }

                    .dropdown-submenu > .dropdown-menu {
                        display: none !important;
                        top: 0;
                        left: 100%;
                        margin-top: -6px;
                        margin-left: -1px;
                        -webkit-border-radius: 0 6px 6px 6px;
                        -moz-border-radius: 0 6px 6px;
                        border-radius: 0 6px 6px 6px;
                    }
                    .dropdown-menu>li>span{margin-left: 40px; font-size: 18px;font-weight: 800;font-family: light;color:yellow}

                    .dropdown-submenu:hover>.dropdown-menu {
                        display: block !important;
                    }

                    .dropdown-submenu>a:after {
                        display: block;
                        content: " ";
                        float: right;
                        width: 0;
                        height: 0;
                        border-color: transparent;
                        border-style: solid;
                        border-width: 5px 0 5px 5px;
                        border-left-color: #ccc;
                        margin-top: 5px;
                        margin-right: -10px;
                    }

                    .dropdown-submenu:hover>a:after {
                        border-left-color: #fff;
                    }

                    .dropdown-submenu.pull-left {
                        float: none;
                    }

                    .dropdown-submenu.pull-left>.dropdown-menu {
                        left: -100%;
                        margin-left: 10px;
                        -webkit-border-radius: 6px 0 6px 6px;
                        -moz-border-radius: 6px 0 6px 6px;
                        border-radius: 6px 0 6px 6px;
                    }

                </style>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><div class="search"><form action="/search" method="get">
                            <input type="search" name="q" placeholder="Поиск"  value="<?= isset($_GET['q']) ? CHtml::encode($_GET['q']) : '' ?>"></form></div></li>
                <li><a href="#" class="city">Москва </a></li>
                <li><a data-toggle="modal" data-target="#userlogin" class="enter">Войти </a></li>
            </ul>
        </div>




        <?php
    }

}
