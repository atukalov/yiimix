<?php

class navVert extends CWidget {

    //put your code here
    public $id = 0;
    public $items;

    public function init() {

        $item = Menus::model()->findByPk($this->id);

        if (!empty($item->query)) {

            $res = preg_match("/(?:([a-zA-z_0-9:]+)(::))?([a-zA-z_0-9]+)\s*\(\s*(.*?)\s*\)\s*$/", $item->query, $m);

            if ($res) {
                $cl = empty($m[1]) ? false : $m[1];
                $func = empty($m[3]) ? false : $m[3];
                $params = empty($m[4]) ? false : $m[4];
            }


            if ($cl) {
                $this->items = forward_static_call(array($cl, $func), $params);
            } else {
                $this->items = call_user_func($func, $params);
            }
        } else {
            $this->items = Menus::model()->findAllByAttributes(array("parent" => $this->id));
        }
    }

    public function run() {
        ?>

        <div class="sidebar vert">
            <ul class="nav">

                <?php
                foreach ($this->items as $k => $v):

                    $q = trim(Yii::app()->request->requestUri,"/");
                    
                    $cl = "";
                    if (preg_match("/^" . $k . "/", $q))
                        $cl = " class=\"active\"";
                    ?>
                    <li<?= $cl ?>>
                        <a href="/<?= $k ?>">
                            <?php if (isset($v->icon) && !empty($v->icon)): ?>
                                <i class="fa fa-group"></i>
                            <?php endif; ?>
                            <span class="menu-title"><?= $v ?></span>

                        </a>
                    </li>

                <?php endforeach; ?>


        </div>
        <?php
    }

}
