<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author wolf
 */
class rightMenu extends CWidget {

    public $id;
    private $items = array();
    private $ids = array();

    public function init() {

        $it = Pages::model()->findByAttributes(array("id" => $this->id));
        $id = $it->id;
        $p = array(intval($it->id));
        if ($it):
            while ($it->parent > 1):
                $it = Pages::model()->findByAttributes(array("id" => $id));
                if ($it->parent == 1) {
                    $id = intval($it->id);
                    break;
                } else
                    $id = intval($it->parent);
                $p[] = $id;
            endwhile;
        endif;
        $this->ids = $p = array_reverse($p);
        $this->items = Pages::model()->findAllByAttributes(array("parent" => $p[0]), array("order" => "position ASC"));
        //var_dump($p);
    }

    private function renderItem($item) {

        $active = in_array($item->id, $this->ids) ? TRUE : FALSE;
        $res = FALSE;

        if ($active)
            $res = Pages::model()->findAllByAttributes(array("parent" => $item->id), array("order" => "position ASC"));
        
        ?> <li<?= ($active) ? ' class="active"' : '' ?>><a href="/<?= $item->url ?>"><?= $item->title ?></a><?php
            if ($res):
                ?><ul class="sub"><?php
                foreach ($res as $v) {
                    $this->renderItem($v);
                }
                ?></ul><?php
            endif;
            ?></li><?php
        }

        public function run() {
            if (!empty($this->items)):
                ?><ul class="right-menu"><?php
                foreach ($this->items as $v) {
                    $this->renderItem($v);
                }
                ?></ul><?php
        endif;
    }

}
